package bmg.emef.log;

import bmg.emef.environment.Environment;

public interface Log
{
	public default String getLoggerName()
	{
		return "Default Logger";
	}

	public default void debug(String message)
	{
		if (Environment.Analytics.development)
		{
			System.out.println("DEBUG :: " + message);
		}
	}

	public default void info(String message)
	{
		if ((Environment.Analytics.verbose || Environment.Analytics.development) && !Environment.Analytics.noLog)
		{
			System.out.println("INFO  :: " + message);
		}
	}

	public default void warn(String message)
	{
		if (!Environment.Analytics.noLog)
		{
			System.out.println("WARN  :: " + message);
		}
	}

	public default void error(String message)
	{
		System.err.println("ERROR :: " + message);
	}
}
