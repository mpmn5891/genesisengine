/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Sri Harsha Chilakapati
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package bmg.emef.util;

import bmg.emef.math.Vector2;
import bmg.emef.math.Vector3;
import bmg.emef.math.Vector4;

public final class MathUtils
{
	private MathUtils()
	{
		// nothing to see here, prevents instantiation
	}

	public static double toDegrees(double radians)
	{
		return Math.toDegrees(radians);
	}

	public static float toDegrees(float radians)
	{
		return (float) Math.toDegrees(radians);
	}

	public static double toRadians(double degrees)
	{
		return Math.toRadians(degrees);
	}

	public static float toRadians(float degrees)
	{
		return (float) Math.toRadians(degrees);
	}

	public static int abs(int value)
	{
		return Math.abs(value);
	}

	public static float abs(float value)
	{
		return Math.abs(value);
	}

	public static short abs(short value)
	{
		return (short) Math.abs(value);
	}

	public static double abs(double value)
	{
		return Math.abs(value);
	}

	public static long abs(long value)
	{
		return Math.abs(value);
	}

	public static float cot(float angle)
	{
		return 1f / tan(angle);
	}

	public static double cot(double angle)
	{
		return 1d / tan(angle);
	}

	public static float tan(float angle)
	{
		return sin(angle) / cos(angle);
	}

	public static double tan(double angle)
	{
		return sin(angle) / cos(angle);
	}

	public static float sin(float angle)
	{
		return (float) Math.sin(Math.toRadians(angle));
	}

	public static double sin(double angle)
	{
		return Math.sin(Math.toRadians(angle));
	}

	public static float cos(float angle)
	{
		return (float) Math.cos(Math.toRadians(angle));
	}

	public static double cos(double angle)
	{
		return Math.cos(Math.toRadians(angle));
	}

	public static float sec(float angle)
	{
		return 1f / cos(angle);
	}

	public static double sec(double angle)
	{
		return 1d / cos(angle);
	}

	public static float csc(float angle)
	{
		return 1f / sin(angle);
	}

	public static double csc(double angle)
	{
		return 1d / sin(angle);
	}

	public static float acos(float angle)
	{
		return (float) Math.toDegrees(Math.acos(angle));
	}

	public static double acos(double angle)
	{
		return Math.toDegrees(Math.acos(angle));
	}

	public static float asin(float angle)
	{
		return (float) Math.toDegrees(Math.asin(angle));
	}

	public static double asin(double angle)
	{
		return Math.toDegrees(Math.asin(angle));
	}

	public static float atan2(float y, float x)
	{
		return (float) Math.toDegrees(Math.atan2(y, x));
	}

	public static double atan2(double x, double y)
	{
		return Math.toDegrees(Math.atan2(x, y));
	}

	public static int sqrt(int value)
	{
		return (int) Math.sqrt(value);
	}

	public static float sqrt(float value)
	{
		return (float) Math.sqrt(value);
	}

	public static short sqrt(short value)
	{
		return (short) Math.sqrt(value);
	}

	public static long sqrt(long value)
	{
		return (long) Math.sqrt(value);
	}

	public static double sqrt(double value)
	{
		return Math.sqrt(value);
	}

	public static int min(int a, int b)
	{
		return Math.min(a, b);
	}

	public static int max(int a, int b)
	{
		return Math.max(a, b);
	}

	public static float min(float a, float b)
	{
		return Math.min(a, b);
	}

	public static float max(float a, float b)
	{
		return Math.max(a, b);
	}

	public static short min(short a, short b)
	{
		return (short) Math.min(a, b);
	}

	public static short max(short a, short b)
	{
		return (short) Math.max(a, b);
	}

	public static long min(long a, long b)
	{
		return Math.min(a, b);
	}

	public static long max(long a, long b)
	{
		return Math.max(a, b);
	}

	public static double min(double a, double b)
	{
		return Math.min(a, b);
	}

	public static double max(double a, double b)
	{
		return Math.max(a, b);
	}

	public static Vector2 min(Vector2 v1, Vector2 v2)
	{
		return new Vector2(v1.x < v2.x ? v1.x : v2.x, v1.y < v2.y ? v1.y : v2.y);
	}

	public static Vector2 min(Vector2 v1, Vector2 v2, Vector2 dest)
	{
		return dest.set(v1.x < v2.x ? v1.x : v2.x, v1.y < v2.y ? v1.y : v2.y);
	}

	public static Vector2 max(Vector2 v1, Vector2 v2)
	{
		return new Vector2(v1.x > v2.x ? v1.x : v2.x, v1.y > v2.y ? v1.y : v2.y);
	}

	public static Vector2 max(Vector2 v1, Vector2 v2, Vector2 dest)
	{
		return dest.set(v1.x > v2.x ? v1.x : v2.x, v1.y > v2.y ? v1.y : v2.y);
	}

	public static Vector3 min(Vector3 v1, Vector3 v2)
	{
		return new Vector3(v1.x < v2.x ? v1.x : v2.x, v1.y < v2.y ? v1.y : v2.y, v1.z < v2.z ? v1.z : v2.z);
	}

	public static Vector3 min(Vector3 v1, Vector3 v2, Vector3 dest)
	{
		return dest.set(v1.x < v2.x ? v1.x : v2.x, v1.y < v2.y ? v1.y : v2.y, v1.z < v2.z ? v1.z : v2.z);
	}

	public static Vector3 max(Vector3 v1, Vector3 v2)
	{
		return new Vector3(v1.x > v2.x ? v1.x : v2.x, v1.y > v2.y ? v1.y : v2.y, v1.z > v2.z ? v1.z : v2.z);
	}

	public static Vector3 max(Vector3 v1, Vector3 v2, Vector3 dest)
	{
		return dest.set(v1.x > v2.x ? v1.x : v2.x, v1.y > v2.y ? v1.y : v2.y, v1.z > v2.z ? v1.z : v2.z);
	}

	public static Vector4 min(Vector4 v1, Vector4 v2)
	{
		return new Vector4(v1.x < v2.x ? v1.x : v2.x, v1.y < v2.y ? v1.y : v2.y, v1.z < v2.z ? v1.z : v2.z, v1.w < v2.w ? v1.w : v2.w);
	}

	public static Vector4 min(Vector4 v1, Vector4 v2, Vector4 dest)
	{
		return dest.set(v1.x < v2.x ? v1.x : v2.x, v1.y < v2.y ? v1.y : v2.y, v1.z < v2.z ? v1.z : v2.z, v1.w < v2.w ? v1.w : v2.w);
	}

	public static Vector4 max(Vector4 v1, Vector4 v2)
	{
		return new Vector4(v1.x > v2.x ? v1.x : v2.x, v1.y > v2.y ? v1.y : v2.y, v1.z > v2.z ? v1.z : v2.z, v1.w > v2.w ? v1.w : v2.w);
	}

	public static Vector4 max(Vector4 v1, Vector4 v2, Vector4 dest)
	{
		return dest.set(v1.x > v2.x ? v1.x : v2.x, v1.y > v2.y ? v1.y : v2.y, v1.z > v2.z ? v1.z : v2.z, v1.w > v2.w ? v1.w : v2.w);
	}

	public static int convertRange(int value, int oldMin, int oldMax, int newMin, int newMax)
	{
		return (((value - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
	}

	public static float convertRange(float value, float oldMin, float oldMax, float newMin, float newMax)
	{
		return (((value - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
	}

	public static short convertRange(short value, short oldMin, short oldMax, short newMin, short newMax)
	{
		return (short) ((((value - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin);
	}

	public static long convertRange(long value, long oldMin, long oldMax, long newMin, long newMax)
	{
		return (((value - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
	}

	public static double convertRange(double value, double oldMin, double oldMax, double newMin, double newMax)
	{
		return (((value - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
	}

	public static int floor(int value)
	{
		return (int) Math.floor(value);
	}

	public static float floor(float value)
	{
		return (float) Math.floor(value);
	}

	public static short floor(short value)
	{
		return (short) Math.floor(value);
	}

	public static long floor(long value)
	{
		return (long) Math.floor(value);
	}

	public static double floor(double value)
	{
		return (double) Math.floor(value);
	}

	public static int ceil(int value)
	{
		return (int) Math.ceil(value);
	}

	public static float ceil(float value)
	{
		return (float) Math.ceil(value);
	}

	public static short ceil(short value)
	{
		return (short) Math.ceil(value);
	}

	public static long ceil(long value)
	{
		return (long) Math.ceil(value);
	}

	public static double ceil(double value)
	{
		return (double) Math.ceil(value);
	}

	public static int clamp(int val, int min, int max)
	{
		val = Math.min(val, max);
		val = Math.max(val, min);

		return val;
	}

	public static long clamp(long val, long min, long max)
	{
		val = Math.min(val, max);
		val = Math.max(val, min);

		return val;
	}

	public static double clamp(double val, double min, double max)
	{
		val = Math.min(val, max);
		val = Math.max(val, min);

		return val;
	}

	public static short clamp(short val, short min, short max)
	{
		val = (short) Math.min(val, max);
		val = (short) Math.max(val, min);

		return val;
	}

	public static Vector2 clamp(Vector2 vector, Vector2 min, Vector2 max)
	{
		Vector2 result = new Vector2();

		result.x = clamp(vector.x, min.x, max.x);
		result.y = clamp(vector.y, min.y, max.y);

		return result;
	}

	public static float clamp(float val, float min, float max)
	{
		val = Math.min(val, max);
		val = Math.max(val, min);

		return val;
	}

	public static Vector3 clamp(Vector3 vector, Vector3 min, Vector3 max)
	{
		Vector3 result = new Vector3();

		result.x = clamp(vector.x, min.x, max.x);
		result.y = clamp(vector.y, min.y, max.y);
		result.z = clamp(vector.z, min.z, max.z);

		return result;
	}

	public static Vector4 clamp(Vector4 vector, Vector4 min, Vector4 max)
	{
		Vector4 result = new Vector4();

		result.x = clamp(vector.x, min.x, max.x);
		result.y = clamp(vector.y, min.y, max.y);
		result.z = clamp(vector.z, min.z, max.z);
		result.w = clamp(vector.w, min.w, max.w);

		return result;
	}

	public static float round(float value, float precision)
	{
		return Math.round(value * precision) / precision;
	}

	public static double rount(double value, double precision)
	{
		return Math.round(value * precision) / precision;
	}

	/**
	 * Returns a random real number between x1 (inclusive) and x2 (exclusive).
	 *
	 * @param x1
	 *            The inclusive
	 * @param x2
	 *            The exclusive
	 *
	 * @return A random real number between x1 and x2
	 */
	public static int randomRange(int x1, int x2)
	{
		return (int) (Math.floor(x1 + (Math.random() * (x2 - x1))));
	}

	/**
	 * Returns one of the arguments chosen randomly.
	 *
	 * @param values
	 *            The array containing values
	 *
	 * @return A random value present in the array
	 */
	public static int choose(int[] values)
	{
		return (values[random(values.length + 1)]);
	}

	/**
	 * Returns a random real number between 0 and x. The number is always smaller than x.
	 *
	 * @param x
	 *            The maximum range
	 *
	 * @return A random real number
	 */
	public static int random(int x)
	{
		return (int) (Math.floor(Math.random() * x));
	}

	/**
	 * Returns the fractional part of x, that is, the part behind the decimal dot.
	 *
	 * @param x
	 *            The real number
	 *
	 * @return The fractional part of x
	 */
	public static double frac(double x)
	{
		return x % 1.0;
	}

	/**
	 * Returns x to the power n.
	 *
	 * @param x
	 *            The base
	 * @param n
	 *            The exponent
	 *
	 * @return x to the power n.
	 */
	public static int power(int x, int n)
	{
		return (int) (Math.pow(x, n));
	}

	/**
	 * Returns the average of the values.
	 *
	 * @param values
	 *            The array of integers
	 *
	 * @return The mean of the values
	 */
	public static int mean(int[] values)
	{
		int result = 0;

		for (int value : values)
		{
			result += value;
		}

		result /= values.length;
		return result;
	}

	/**
	 * Returns the distance between point (x1,y1) and point (x2,y2).
	 *
	 * @param x1
	 *            The abscissa of first point
	 * @param y1
	 *            The ordinate of first point
	 * @param x2
	 *            The abscissa of second point
	 * @param y2
	 *            The ordinate of second point
	 *
	 * @return The distance between two points
	 */
	public static double pointDistance(double x1, double y1, double x2, double y2)
	{
		return Math.sqrt(((x2 - x1) * (x2 - x1))
				+ ((y2 - y1) * (y2 - y1)));
	}

	/**
	 * Returns the distance between point (x1,y1,z1) and point (x2,y2,z2).
	 *
	 * @param x1
	 *            The abscissa of first point
	 * @param y1
	 *            The ordinate of first point
	 * @param z1
	 *            The depth of first point
	 * @param x2
	 *            The abscissa of second point
	 * @param y2
	 *            The ordinate of second point
	 * @param z2
	 *            The depth of second point
	 *
	 * @return The distance between two points
	 */
	public static double pointDistance(double x1, double y1, double z1, double x2, double y2, double z2)
	{
		return Math.sqrt(((x2 - x1) * (x2 - x1))
				+ ((y2 - y1) * (y2 - y1))
				+ ((y2 - y1) * (y2 - y1)));
	}

	public static boolean chance(int percentage)
	{
		return random(100) <= percentage;
	}

	/**
	 * Returns the inclination of the line-segment drawn from (x1, y1) to (x2, y2) in degrees
	 *
	 * @param x1
	 *            The abscissa of first point
	 * @param y1
	 *            The ordinate of first point
	 * @param x2
	 *            The abscissa of second point
	 * @param y2
	 *            The ordinate of second point
	 *
	 * @return The direction in degrees
	 */
	public static float getDirection(float x1, float y1, float x2, float y2)
	{
		return atan((y2 - y1) / (x2 - x1));
	}

	public static float atan(float value)
	{
		return (float) Math.toDegrees(Math.atan(value));
	}

	public static boolean isBoolean(Object object)
	{
		boolean bool = object instanceof Boolean;

		if (bool) return true;

		try
		{
			Boolean.parseBoolean(object.toString());
			bool = true;
		}
		catch (Exception e)
		{
			bool = false;
		}

		return bool;
	}

	public static boolean isInteger(Object object)
	{
		boolean bool = object instanceof Integer;

		if (bool) return true;

		try
		{
			Integer.parseInt(object.toString());
			bool = true;
		}
		catch (Exception e)
		{
			bool = false;
		}

		return bool;
	}

	public static boolean isShort(Object object)
	{
		boolean bool = object instanceof Short;

		if (bool) return true;

		try
		{
			Short.parseShort(object.toString());
			bool = true;
		}
		catch (Exception e)
		{
			bool = false;
		}

		return bool;
	}

	public static boolean isFloat(Object object)
	{
		boolean bool = object instanceof Float;

		if (bool) return true;

		try
		{
			Float.parseFloat(object.toString());
			bool = true;
		}
		catch (Exception e)
		{
			bool = false;
		}

		return bool;
	}

	public static boolean isLong(Object object)
	{
		boolean bool = object instanceof Long;

		if (bool) return true;

		try
		{
			Long.parseLong(object.toString());
			bool = true;
		}
		catch (Exception e)
		{
			bool = false;
		}

		return bool;
	}

	public static boolean isDouble(Object object)
	{
		boolean bool = object instanceof Double;

		if (bool) return true;

		try
		{
			Double.parseDouble(object.toString());
			bool = true;
		}
		catch (Exception e)
		{
			bool = false;
		}

		return bool;
	}
}
