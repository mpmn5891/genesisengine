package bmg.emef.util;

@FunctionalInterface
public interface ObjectProvider<T>
{
	T provide();
}
