package bmg.emef.util;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

import bmg.emef.environment.Exceptions;

public class ObjectStack<T>
{
	private Deque<T> stack;
	private ObjectProvider<T> provider;

	public ObjectStack(ObjectProvider<T> provider)
	{
		stack = new LinkedList<>();
		this.provider = provider;
	}

	public T pop()
	{
		if (stack.size() == 0)
		{
			try
			{
				T object = provider.provide();
				stack.push(object);
			}
			catch (Exception e)
			{
				Exceptions.Exception.reThrow(e);
			}
		}
		return stack.pop();
	}

	public void push(T object)
	{
		stack.push(object);
	}

	public void push(@SuppressWarnings("unchecked") T... objects)
	{
		for (T object : objects)
		{
			stack.push(object);
		}
	}

	public List<T> getAsList()
	{
		List<T> list = new ArrayList<>();
		T object;
		while ((object = stack.poll()) != null)
		{
			list.add(object);
		}
		return list;
	}
}
