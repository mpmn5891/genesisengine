package bmg.emef.util;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.lwjgl.stb.STBVorbis;
import org.lwjgl.stb.STBVorbisInfo;
import org.lwjgl.system.MemoryUtil;

import bmg.emef.audio.Audio.ALFormat;
import bmg.emef.buffer.DirectBuffer;
import bmg.emef.environment.Exceptions;
import bmg.emef.io.DirectBufferInputStream;

public class SoundUtils
{
	static ByteBuffer convertAudioBytes(byte[] samples, boolean twoByteSamples)
	{
		ByteBuffer src = ByteBuffer.wrap(samples);
		src.order(ByteOrder.LITTLE_ENDIAN);
		return convertAudioBytes(src, twoByteSamples);
	}

	static ByteBuffer convertAudioBytes(ByteBuffer samples, boolean twoByteSamples)
	{
		ByteBuffer dest = ByteBuffer.allocate(samples.capacity());
		dest.order(ByteOrder.nativeOrder());
		if (twoByteSamples)
		{
			ShortBuffer dest_short = dest.asShortBuffer();
			ShortBuffer src_short = samples.asShortBuffer();
			while (src_short.hasRemaining())
			{
				dest_short.put(src_short.get());
			}
		}
		else
		{
			while (samples.hasRemaining())
			{
				dest.put(samples.get());
			}
		}
		dest.rewind();
		return dest;
	}

	public static class SoundReaders
	{
		public static class OggReader
		{
			public ByteBuffer data;
			public int sampleRate;
			public ALFormat format;

			public static OggReader decode(DirectBuffer input)
			{
				OggReader reader = new OggReader();
				ByteBuffer memoryBuffer = input.nativeBuffer();
				IntBuffer error = BufferUtils.createIntBuffer(1);
				long handle = STBVorbis.stb_vorbis_open_memory(memoryBuffer, error, null);
				if (handle == MemoryUtil.NULL)
				{
					throw new Exceptions.ALException("Incountered Error decoding OGG data:\n" + error.get(0));
				}
				STBVorbisInfo info = STBVorbisInfo.malloc();
				STBVorbis.stb_vorbis_get_info(handle, info);
				int channels = info.channels();
				reader.sampleRate = info.sample_rate();
				reader.format = channels == 1 ? ALFormat.MONO_16 : ALFormat.STEREO_16;
				int numSamples = STBVorbis.nstb_vorbis_stream_length_in_samples(handle);
				ByteBuffer pcm = BufferUtils.createByteBuffer(numSamples * Short.BYTES);
				STBVorbis.stb_vorbis_get_samples_short_interleaved(handle, channels, pcm.asShortBuffer());
				reader.data = pcm;
				STBVorbis.stb_vorbis_close(handle);
				info.free();
				return reader;
			}
		}

		public static class WavReader
		{
			public ByteBuffer data;
			public int sampleRate;
			public ALFormat format;

			public static WavReader decode(DirectBuffer input)
			{
				WavReader reader = new WavReader();
				try
				{
					DirectBufferInputStream is = new DirectBufferInputStream(input);
					BufferedInputStream bis = new BufferedInputStream(is);
					AudioInputStream ais = AudioSystem.getAudioInputStream(bis);
					AudioFormat fmt = ais.getFormat();
					reader.sampleRate = (int) fmt.getSampleRate();
					int format = fmt.getChannels();
					if (format == 1)
					{
						if (fmt.getSampleSizeInBits() == 8)
						{
							reader.format = ALFormat.MONO_8;
						}
						else if (fmt.getSampleSizeInBits() == 16)
						{
							reader.format = ALFormat.MONO_16;
						}
					}
					else if (format == 2)
					{
						if (fmt.getSampleSizeInBits() == 8)
						{
							reader.format = ALFormat.STEREO_8;
						}
						else if (fmt.getSampleSizeInBits() == 16)
						{
							reader.format = ALFormat.STEREO_16;
						}
					}
					else
					{
						throw new Exceptions.ALException("Incorrect Wav file format");
					}
					int length = ais.available();
					if (length <= 0)
					{
						length = ais.getFormat().getChannels() * (int) ais.getFrameLength() * ais.getFormat().getSampleSizeInBits() / 8;
					}
					byte[] samples = new byte[length];
					DataInputStream dis = new DataInputStream(ais);
					dis.readFully(samples);
					reader.data = SoundUtils.convertAudioBytes(samples, fmt.getSampleSizeInBits() == 16);
					ais.close();
					dis.close();
					is.close();
				}
				catch (UnsupportedAudioFileException | IOException e)
				{
					Exceptions.ALException.reThrow(e);
				}
				return reader;
			}
		}
	}
}
