package bmg.emef.util;

import bmg.emef.environment.Environment;

public class TimeUtils
{
	public static final double ENGINE_START_TIME = convert(System.currentTimeMillis(), Unit.MILLIS, getDefaultTimeUnit());

	private TimeUtils()
	{
		// nothing to see here, prevents instantiation
	}

	public static double currentNanos()
	{
		try
		{
			return Environment.display.nanoTime();
		}
		catch (Exception e)
		{
			return convert(System.currentTimeMillis() - ENGINE_START_TIME, getDefaultTimeUnit(), Unit.NANOS);
		}
	}

	public static double currentMicros()
	{
		return currentNanos() / 1000.0;
	}

	public static double currentMillis()
	{
		return currentMicros() / 1000.0;
	}

	public static double currentSeconds()
	{
		return currentMillis() / 1000.0;
	}

	public static double currentTime(Unit unit)
	{
		switch (unit)
		{
			case NANOS:
			{
				return currentNanos();
			}
			case MICROS:
			{
				return currentMicros();
			}
			case MILLIS:
			{
				return currentMillis();
			}
			default:
			{
				return currentSeconds();
			}
		}
	}

	public static double currentTime()
	{
		return currentTime(getDefaultTimeUnit());
	}

	public static double convert(double time, Unit source)
	{
		return convert(time, source, getDefaultTimeUnit());
	}

	public static double convert(double time, Unit source, Unit target)
	{
		if (source == target)
		{
			return time;
		}

		double factor = 1;

		if (source == Unit.SECONDS)
		{
			if (target == Unit.MILLIS)
				factor = 1000.0;
			else if (target == Unit.MICROS)
				factor = 1000000.0;
			else
				factor = 1000000000.0;
		}
		else if (source == Unit.MILLIS)
		{
			if (target == Unit.SECONDS)
				factor = 1 / 1000.0;
			else if (target == Unit.MICROS)
				factor = 1000.0;
			else
				factor = 1000000.0;
		}
		else if (source == Unit.MICROS)
		{
			if (target == Unit.SECONDS)
				factor = 1 / 1000000.0;
			else if (target == Unit.MILLIS)
				factor = 1 / 1000.0;
			else
				factor = 1000.0;
		}
		else
		{
			if (target == Unit.SECONDS)
				factor = 1 / 1000000000.0;
			else if (target == Unit.MILLIS)
				factor = 1 / 1000000.0;
			else if (target == Unit.MICROS)
				factor = 1 / 1000.0;
		}

		return time * factor;
	}

	public static Unit getDefaultTimeUnit()
	{
		return Unit.SECONDS;
	}

	public enum Unit
	{
		NANOS,
		MICROS,
		MILLIS,
		SECONDS
	}
}
