package bmg.emef.util;

import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import bmg.emef.buffer.DirectFloatBuffer;
import bmg.emef.math.Matrix3;
import bmg.emef.math.Matrix4;

public final class BufferUtils
{
	public static DirectFloatBuffer storeInto(Matrix3 mat, DirectFloatBuffer dest)
	{
		int index = 0;
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				dest.write(index++, mat.get(i, j));
			}
		}
		return dest;
	}

	public static DirectFloatBuffer storeInto(Matrix4 mat, DirectFloatBuffer dest)
	{
		int index = 0;
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				dest.write(index++, mat.get(i, j));
			}
		}
		return dest;
	}

	public static IntBuffer createIntBuffer(int capacity)
	{
		return org.lwjgl.BufferUtils.createIntBuffer(capacity);
	}

	public static FloatBuffer createFloatBuffer(int capacity)
	{
		return org.lwjgl.BufferUtils.createFloatBuffer(capacity);
	}

	public static DoubleBuffer createDoubleBuffer(int capacity)
	{
		return org.lwjgl.BufferUtils.createDoubleBuffer(capacity);
	}

	public static ShortBuffer createShortBuffer(int capacity)
	{
		return org.lwjgl.BufferUtils.createShortBuffer(capacity);
	}

	public static ByteBuffer createByteBuffer(int capacity)
	{
		return org.lwjgl.BufferUtils.createByteBuffer(capacity);
	}
}
