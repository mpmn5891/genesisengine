package bmg.emef.audio;

import static bmg.emef.audio.Audio.Constants.AL_DIRECTION;
import static bmg.emef.audio.Audio.Constants.AL_LOOPING;
import static bmg.emef.audio.Audio.Constants.AL_POSITION;
import static bmg.emef.audio.Audio.Constants.AL_VELOCITY;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import bmg.emef.audio.al.ALSource;
import bmg.emef.environment.Environment;
import bmg.emef.util.ObjectStack;

public final class AudioScene
{
	private ObjectStack<ALSource> sourcesPool;
	private ObjectStack<PlayingSource> playingSourcesPool;
	private Map<PlayingSource, AudioSource> playingSources;
	private AudioSource defaultAudioSource;

	AudioScene()
	{
		sourcesPool = new ObjectStack<>(ALSource::new);
		playingSourcesPool = new ObjectStack<>(PlayingSource::new);
		playingSources = new HashMap<>();
		defaultAudioSource = new AudioSource();
		Environment.eventManager.registerDisposeListener(this::cleanUp);
		Environment.eventManager.registerUpdateListener(this::updateSources);
	}

	private void updateSources(float deltaTime)
	{
		Iterator<PlayingSource> iterator = playingSources.keySet().iterator();
		while (iterator.hasNext())
		{
			PlayingSource playingSource = iterator.next();
			ALSource source = playingSource.alSource;
			AudioSource audioSource = playingSources.get(playingSource);
			ALSource.State state = source.getState();
			if (state != ALSource.State.PLAYING && state != ALSource.State.LOOPING)
			{
				source.attachBuffer(null);
				iterator.remove();
				sourcesPool.push(source);
				playingSourcesPool.push(playingSource);
				continue;
			}
			if (audioSource.updated)
			{
				source.pause();
				source.setParameter(AL_POSITION, audioSource.position);
				source.setParameter(AL_VELOCITY, audioSource.velocity);
				source.setParameter(AL_DIRECTION, audioSource.direction);
				audioSource.updated = false;
				source.play();
			}
		}
	}

	public void playStatic(Sound sound)
	{
		play(sound, defaultAudioSource, false);
	}

	public void playStatic(Sound sound, boolean loop)
	{
		play(sound, defaultAudioSource, loop);
	}

	public void play(Sound sound, AudioSource source)
	{
		play(sound, source, false);
	}

	public void play(Sound sound, AudioSource source, boolean loop)
	{
		ALSource alSource = sourcesPool.pop();
		source.update();
		alSource.attachBuffer(sound.buffer);
		alSource.setParameter(AL_POSITION, source.position);
		alSource.setParameter(AL_VELOCITY, source.velocity);
		alSource.setParameter(AL_DIRECTION, source.direction);
		alSource.setParameter(AL_LOOPING, loop);
		source.updated = false;
		alSource.play();
		PlayingSource playingSource = playingSourcesPool.pop();
		playingSource.sound = sound;
		playingSource.alSource = alSource;
		playingSources.put(playingSource, source);
	}

	public void stopAllSources()
	{
		for (PlayingSource source : playingSources.keySet())
		{
			source.alSource.stop();
			sourcesPool.push(source.alSource);
		}
		playingSources.clear();
	}

	public void stopFromAllSources(Sound sound)
	{
		for (PlayingSource source : playingSources.keySet())
		{
			if (source.sound.buffer.getID() == sound.buffer.getID())
			{
				source.alSource.stop();
			}
		}
	}

	public void stopAllFromSource(AudioSource source)
	{
		for (PlayingSource playingSource : playingSources.keySet())
		{
			if (playingSources.get(playingSource) == source)
			{
				playingSource.alSource.stop();
			}
		}
	}

	public void stopStatic(Sound sound)
	{
		stop(sound, defaultAudioSource);
	}

	public void stop(Sound sound, AudioSource source)
	{
		for (PlayingSource playingSource : playingSources.keySet())
		{
			if (playingSource.sound.buffer.getID() == sound.buffer.getID())
			{
				if (playingSources.get(playingSource) == source)
				{
					playingSource.alSource.stop();
				}
			}
		}
	}

	private void cleanUp()
	{
		for (ALSource source : sourcesPool.getAsList())
		{
			source.dispose();
		}
	}

	private static class PlayingSource
	{
		private ALSource alSource;
		private Sound sound;
	}
}
