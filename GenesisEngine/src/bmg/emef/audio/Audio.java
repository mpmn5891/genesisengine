package bmg.emef.audio;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import org.lwjgl.openal.AL;
import org.lwjgl.openal.AL10;
import org.lwjgl.openal.ALC;
import org.lwjgl.openal.ALC10;
import org.lwjgl.openal.ALCCapabilities;

import bmg.emef.audio.al.ALBuffer;
import bmg.emef.buffer.DirectBuffer;
import bmg.emef.environment.Environment;
import bmg.emef.environment.Exceptions;
import bmg.emef.util.SoundUtils;
import bmg.emef.util.SoundUtils.SoundReaders.OggReader;
import bmg.emef.util.SoundUtils.SoundReaders.WavReader;

public class Audio
{
	private long device;
	private long context;
	public final AudioScene scene = new AudioScene();

	public Audio()
	{
		device = ALC10.alcOpenDevice((ByteBuffer) null);
		ALCCapabilities deviceCaps = ALC.createCapabilities(device);
		context = ALC10.alcCreateContext(device, (IntBuffer) null);
		ALC10.alcMakeContextCurrent(context);
		AL.createCapabilities(deviceCaps);
		Environment.eventManager.registerDisposeListener(this::cleanUp);
	}

	private void cleanUp()
	{
		ALC10.alcCloseDevice(device);
		ALC10.alcDestroyContext(context);
	}

	public int alGenBuffers()
	{
		return AL10.alGenBuffers();
	}

	public void alBufferData(int id, int format, DirectBuffer data, int frequency)
	{
		AL10.alBufferData(id, format, (ByteBuffer) data.nativeBuffer(), frequency);
	}

	public void alDeleteBuffers(int... buffers)
	{
		for (int buffer : buffers)
			AL10.alDeleteBuffers(buffer);
	}

	public int alGetError()
	{
		return AL10.alGetError();
	}

	public int alGenSources()
	{
		return AL10.alGenSources();
	}

	public void alSourcei(int id, int param, int value)
	{
		AL10.alSourcei(id, param, value);
	}

	public void alSourcef(int id, int param, float value)
	{
		AL10.alSourcef(id, param, value);
	}

	public void alSource3f(int id, int param, float v1, float v2, float v3)
	{
		AL10.alSource3f(id, param, v1, v2, v3);
	}

	public void alSourcePlay(int id)
	{
		AL10.alSourcePlay(id);
	}

	public void alSourcePause(int id)
	{
		AL10.alSourcePause(id);
	}

	public void alSourceRewind(int id)
	{
		AL10.alSourceRewind(id);
	}

	public void alSourceStop(int id)
	{
		AL10.alSourceStop(id);
	}

	public int alGetSourcei(int id, int parameter)
	{
		return AL10.alGetSourcei(id, parameter);
	}

	public void alDeleteSources(int... sources)
	{
		for (int source : sources)
			AL10.alDeleteSources(source);
	}

	public boolean isSupported(AudioFormat format)
	{
		switch (format)
		{
			case OGG:
			{
				return true;
			}
			case MP3:
			{
				return false;
			}
			case WAV:
			{
				return true;
			}
			case WEBM:
			{
				return false;
			}
			default:
			{
				return false;
			}
		}
	}

	public Sound readToALBuffer(DirectBuffer data, AudioFormat format)
	{
		Sound sound = null;
		try
		{
			if (!isSupported(format))
			{
				throw new Exceptions.ALException("Can not decode an unsupported format: " + format.toString());
			}
			else
			{
				switch (format)
				{
					case OGG:
					{
						OggReader ogg = SoundUtils.SoundReaders.OggReader.decode(data);
						ALBuffer buffer = new ALBuffer();
						buffer.uploadData(new DirectBuffer(ogg.data), ogg.format, ogg.sampleRate);
						sound = new Sound(buffer);
						break;
					}
					case WAV:
					{
						WavReader wav = SoundUtils.SoundReaders.WavReader.decode(data);
						ALBuffer buffer = new ALBuffer();
						buffer.uploadData(new DirectBuffer(wav.data), wav.format, wav.sampleRate);
						sound = new Sound(buffer);
						break;
					}
					default:
					{
						throw new Exceptions.ALException("Incountered Error trying to decode sound data");
					}
				}
			}
		}
		catch (Throwable thrown)
		{
			Exceptions.ALException.reThrow(thrown);
		}
		return sound;
	}

	public static final class Constants
	{
		public static final int AL_INVALID = 0xffffffff;
		public static final int AL_NONE = 0x0;
		public static final int AL_FALSE = 0x0;
		public static final int AL_TRUE = 0x1;
		public static final int AL_NO_ERROR = 0x0;
		public static final int AL_INVALID_NAME = 0xa001;
		public static final int AL_INVALID_ENUM = 0xA002;
		public static final int AL_INVALID_VALUE = 0xA003;
		public static final int AL_INVALID_OPERATION = 0xA004;
		public static final int AL_OUT_OF_MEMORY = 0xA005;
		public static final int AL_DOPPLER_FACTOR = 0xC000;
		public static final int AL_DISTANCE_MODEL = 0xD000;
		public static final int AL_VENDOR = 0xB001;
		public static final int AL_VERSION = 0xB002;
		public static final int AL_RENDERER = 0xB003;
		public static final int AL_EXTENSIONS = 0xB004;
		public static final int AL_INVERSE_DISTANCE = 0xD001;
		public static final int AL_INVERSE_DISTANCE_CLAMPED = 0xD002;
		public static final int AL_SOURCE_ABSOLUTE = 0x201;
		public static final int AL_SOURCE_RELATIVE = 0x202;
		public static final int AL_POSITION = 0x1004;
		public static final int AL_VELOCITY = 0x1006;
		public static final int AL_GAIN = 0x100A;
		public static final int AL_CONE_INNER_ANGLE = 0x1001;
		public static final int AL_CONE_OUTER_ANGLE = 0x1002;
		public static final int AL_PITCH = 0x1003;
		public static final int AL_DIRECTION = 0x1005;
		public static final int AL_LOOPING = 0x1007;
		public static final int AL_BUFFER = 0x1009;
		public static final int AL_SOURCE_STATE = 0x1010;
		public static final int AL_CONE_OUTER_GAIN = 0x1022;
		public static final int AL_SOURCE_TYPE = 0x1027;
		public static final int AL_INITIAL = 0x1011;
		public static final int AL_PLAYING = 0x1012;
		public static final int AL_PAUSED = 0x1013;
		public static final int AL_STOPPED = 0x1014;
		public static final int AL_ORIENTATION = 0x100F;
		public static final int AL_BUFFERS_QUEUED = 0x1015;
		public static final int AL_BUFFERS_PROCESSED = 0x1016;
		public static final int AL_MIN_GAIN = 0x100D;
		public static final int AL_MAX_GAIN = 0x100E;
		public static final int AL_REFERENCE_DISTANCE = 0x1020;
		public static final int AL_ROLLOFF_FACTOR = 0x1021;
		public static final int AL_MAX_DISTANCE = 0x1023;
		public static final int AL_FREQUENCY = 0x2001;
		public static final int AL_BITS = 0x2002;
		public static final int AL_CHANNELS = 0x2003;
		public static final int AL_SIZE = 0x2004;
		public static final int AL_FORMAT_MONO8 = 0x1100;
		public static final int AL_FORMAT_MONO16 = 0x1101;
		public static final int AL_FORMAT_STEREO8 = 0x1102;
		public static final int AL_FORMAT_STEREO16 = 0x1103;
		public static final int AL_UNUSED = 0x2010;
		public static final int AL_PENDING = 0x2011;
		public static final int AL_PROCESSED = 0x2012;

		private Constants()
		{
			// nothing to see here, prevents instantiation
		}
	}

	public enum AudioFormat
	{
		OGG,
		WAV,
		MP3,
		WEBM;
	}

	public enum ALFormat
	{
		MONO_8(Constants.AL_FORMAT_MONO8),
		MONO_16(Constants.AL_FORMAT_MONO16),
		STEREO_8(Constants.AL_FORMAT_STEREO8),
		STEREO_16(Constants.AL_FORMAT_STEREO16);

		private int alFormat;

		ALFormat(int alFormat)
		{
			this.alFormat = alFormat;
		}

		public static ALFormat getEnum(int value)
		{
			switch (value)
			{
				case Constants.AL_FORMAT_MONO8:
				{
					return MONO_8;
				}
				case Constants.AL_FORMAT_MONO16:
				{
					return MONO_16;
				}
				case Constants.AL_FORMAT_STEREO8:
				{
					return STEREO_8;
				}
				case Constants.AL_FORMAT_STEREO16:
				{
					return STEREO_16;
				}
				default:
				{
					throw new Exceptions.ALException("Unknown format value: " + value);
				}
			}
		}

		public int getALFormat()
		{
			return alFormat;
		}
	}
}
