package bmg.emef.audio;

import bmg.emef.audio.al.ALBuffer;
import bmg.emef.environment.Environment;

public class Sound
{
	public ALBuffer buffer;

	public Sound(ALBuffer buffer)
	{
		this.buffer = buffer;
	}

	public void play()
	{
		Environment.audio.scene.playStatic(this);
	}

	public void play(boolean loop)
	{
		Environment.audio.scene.playStatic(this, loop);
	}

	public void stop()
	{
		Environment.audio.scene.stopStatic(this);
	}

	public void dispose()
	{
		buffer.dispose();
	}
}
