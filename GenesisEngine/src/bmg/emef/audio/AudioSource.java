package bmg.emef.audio;

import bmg.emef.math.Vector3;

public class AudioSource
{
	public final Vector3 position = new Vector3();
	public final Vector3 velocity = new Vector3();
	public final Vector3 direction = new Vector3();

	boolean updated = true;

	public void update()
	{
		updated = true;
	}
}
