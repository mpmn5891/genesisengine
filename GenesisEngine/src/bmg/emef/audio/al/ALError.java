package bmg.emef.audio.al;

import static bmg.emef.audio.Audio.Constants.AL_INVALID_ENUM;
import static bmg.emef.audio.Audio.Constants.AL_INVALID_NAME;
import static bmg.emef.audio.Audio.Constants.AL_INVALID_OPERATION;
import static bmg.emef.audio.Audio.Constants.AL_INVALID_VALUE;
import static bmg.emef.audio.Audio.Constants.AL_NO_ERROR;
import static bmg.emef.audio.Audio.Constants.AL_OUT_OF_MEMORY;

import bmg.emef.environment.Environment;
import bmg.emef.environment.Exceptions;

public final class ALError
{
	private ALError()
	{
		// nothing to see here, prevents instantiation
	}

	public static void check()
	{
		check(false);
	}

	public static void check(boolean force)
	{
		if (force || Environment.Analytics.development)
		{
			switch (Environment.audio.alGetError())
			{
				case AL_NO_ERROR:
				{
					break;
				}
				case AL_INVALID_NAME:
				{
					throw new Exceptions.ALException.InvalidName();
				}
				case AL_INVALID_ENUM:
				{
					throw new Exceptions.ALException.InvalidEnum();
				}
				case AL_INVALID_VALUE:
				{
					throw new Exceptions.ALException.InvalidValue();
				}
				case AL_INVALID_OPERATION:
				{
					throw new Exceptions.ALException.InvalidOperation();
				}
				case AL_OUT_OF_MEMORY:
				{
					throw new Exceptions.ALException.OutOfMemory();
				}
				default:
				{
					throw new Exceptions.ALException("Unknown OpenAL Error");
				}
			}
		}
	}

	public static Value get()
	{
		switch (Environment.audio.alGetError())
		{
			case AL_NO_ERROR:
			{
				return Value.NO_ERROR;
			}
			case AL_INVALID_NAME:
			{
				return Value.INVALID_NAME;
			}
			case AL_INVALID_ENUM:
			{
				return Value.INVALID_ENUM;
			}
			case AL_INVALID_VALUE:
			{
				return Value.INVALID_VALUE;
			}
			case AL_INVALID_OPERATION:
			{
				return Value.INVALID_OPERATION;
			}
			case AL_OUT_OF_MEMORY:
			{
				return Value.OUT_OF_MEMORY;
			}
			default:
			{
				return Value.NO_ERROR;
			}
		}
	}

	public enum Value
	{
		NO_ERROR,
		INVALID_NAME,
		INVALID_ENUM,
		INVALID_VALUE,
		INVALID_OPERATION,
		OUT_OF_MEMORY;
	}
}
