package bmg.emef.audio.al;

import static bmg.emef.audio.Audio.Constants.AL_BUFFER;
import static bmg.emef.audio.Audio.Constants.AL_FALSE;
import static bmg.emef.audio.Audio.Constants.AL_LOOPING;
import static bmg.emef.audio.Audio.Constants.AL_PAUSED;
import static bmg.emef.audio.Audio.Constants.AL_PLAYING;
import static bmg.emef.audio.Audio.Constants.AL_SOURCE_STATE;
import static bmg.emef.audio.Audio.Constants.AL_TRUE;

import bmg.emef.environment.Environment;
import bmg.emef.environment.Exceptions;
import bmg.emef.math.Vector3;

public class ALSource
{
	private int id;
	private boolean disposed;

	public ALSource()
	{
		this(Environment.audio.alGenSources());
	}

	public ALSource(int id)
	{
		this.id = id;
		ALError.check();
		disposed = false;
	}

	public void setParameter(int parameter, int value)
	{
		if (disposed)
		{
			throw new Exceptions.ALException("Can not set a Parameter for a disposed openAL Source");
		}
		else
		{
			Environment.audio.alSourcei(id, parameter, value);
			ALError.check();
		}
	}

	public void setParameter(int parameter, float value)
	{
		if (disposed)
		{
			throw new Exceptions.ALException("Can not set a Parameter for a disposed openAL Source");
		}
		else
		{
			Environment.audio.alSourcef(id, parameter, value);
			ALError.check();
		}
	}

	public void setParameter(int parameter, float value1, float value2, float value3)
	{
		if (disposed)
		{
			throw new Exceptions.ALException("Can not set a Parameter for a disposed openAL Source");
		}
		else
		{
			Environment.audio.alSource3f(id, parameter, value1, value2, value3);
			ALError.check();
		}
	}

	public void attachBuffer(ALBuffer buffer)
	{
		setParameter(AL_BUFFER, buffer == null ? 0 : buffer.getID());
	}

	public void setParameter(int parameter, boolean value)
	{
		setParameter(parameter, value ? AL_TRUE : AL_FALSE);
	}

	public void setParameter(int parameter, Vector3 value)
	{
		setParameter(parameter, value.x, value.y, value.z);
	}

	public int getParameter(int parameter)
	{
		int result = Environment.audio.alGetSourcei(id, parameter);
		ALError.check();
		return result;
	}

	public void play()
	{
		if (disposed)
		{
			throw new Exceptions.ALException("Can not paly a disposed openAL Source");
		}
		else
		{
			Environment.audio.alSourcePlay(id);
			ALError.check();
		}
	}

	public void pause()
	{
		if (disposed)
		{
			throw new Exceptions.ALException("Can not pause a disposed openAL Source");
		}
		else
		{
			Environment.audio.alSourcePause(id);
			ALError.check();
		}
	}

	public void stop()
	{
		if (disposed)
		{
			throw new Exceptions.ALException("Can not stop a disposed openAL Source");
		}
		else
		{
			Environment.audio.alSourceStop(id);
			ALError.check();
		}
	}

	public void rewind()
	{
		if (disposed)
		{
			throw new Exceptions.ALException("Can not rewind a disposed openAL Source");
		}
		else
		{
			Environment.audio.alSourceRewind(id);
			ALError.check();
		}
	}

	public int getID()
	{
		return id;
	}

	public boolean isDisposed()
	{
		return disposed;
	}

	public State getState()
	{
		int state = getParameter(AL_SOURCE_STATE);
		switch (state)
		{
			case AL_PLAYING:
			{
				if (getParameter(AL_LOOPING) == AL_TRUE)
				{
					return State.LOOPING;
				}
				else
				{
					return State.PLAYING;
				}
			}
			case AL_PAUSED:
			{
				return State.PAUSED;
			}
			default:
			{
				return State.STOPPED;
			}
		}
	}

	public void dispose()
	{
		if (disposed)
		{
			Environment.log.warn("Tried to dispose an already disposed OpenAL Source");
		}
		else
		{
			Environment.audio.alDeleteSources(id);
			ALError.check();
			disposed = true;
		}
	}

	public enum State
	{
		PLAYING,
		STOPPED,
		PAUSED,
		LOOPING;
	}
}
