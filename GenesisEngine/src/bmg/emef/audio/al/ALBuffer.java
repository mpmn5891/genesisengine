package bmg.emef.audio.al;

import bmg.emef.audio.Audio.ALFormat;
import bmg.emef.buffer.DirectBuffer;
import bmg.emef.environment.Environment;
import bmg.emef.environment.Exceptions;

public class ALBuffer
{
	private int id;
	private boolean disposed;

	public ALBuffer()
	{
		this(Environment.audio.alGenBuffers());
	}

	public ALBuffer(int id)
	{
		this.id = id;
		ALError.check();
		disposed = false;
		Environment.eventManager.registerDisposeListener(this::dispose);
	}

	public void uploadData(DirectBuffer data, ALFormat format, int frequency)
	{
		if (disposed)
		{
			throw new Exceptions.ALException("Unable to upload data to a disposed OpenAL buffer");
		}
		else
		{
			Environment.audio.alBufferData(id, format.getALFormat(), data, frequency);
			ALError.check();
		}
	}

	public int getID()
	{
		return id;
	}

	public boolean isDisposed()
	{
		return disposed;
	}

	public void dispose()
	{
		if (disposed)
		{
			Environment.log.warn("Tried to dispose and already disposed OpenAL Buffer");
		}
		else
		{
			Environment.audio.alDeleteBuffers(id);
			ALError.check();
			disposed = true;
		}
	}
}
