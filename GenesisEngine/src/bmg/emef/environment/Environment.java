package bmg.emef.environment;

import org.lwjgl.system.Configuration;

import bmg.emef.Input.Input;
import bmg.emef.audio.Audio;
import bmg.emef.events.EventManager;
import bmg.emef.events.loops.UpdateLoop;
import bmg.emef.events.loops.VariableStepLoop;
import bmg.emef.graphics.Color;
import bmg.emef.graphics.Graphics;
import bmg.emef.graphics.gl.GLContext;
import bmg.emef.graphics.gl.Texture;
import bmg.emef.io.IO;
import bmg.emef.log.Log;
import bmg.emef.math.Vector3;

public class Environment
{
	public static Platform platform = Environment.getPlatform();
	public static EventManager eventManager = EventManager.getInstance();
	public static UpdateLoop loop = new VariableStepLoop();
	public static Graphics graphics = new Graphics();
	public static Display display = new Display();
	public static Audio audio = new Audio();
	public static IO io = new IO();
	public static Input input = new Input();
	public static Log log = new Log()
	{};

	public static void start()
	{
		Environment.Analytics.engineStarted = true;
		Configuration.DEBUG.set(Environment.Analytics.development);
		if (Environment.getPlatform() == Platform.MACOSX)
		{
			System.setProperty("java.awt.headless", "true");
		}
		display.window.makeCurrent();
		GLContext.enable(Graphics.Constants.GL_BLEND);
		GLContext.blendFunc(Graphics.Constants.GL_SRC_ALPHA, Graphics.Constants.GL_ONE_MINUS_SRC_ALPHA);
		Texture.EMPTY = Texture.fromColor(Color.TRANSPARENT, 32, 32);
		Texture.EMPTY.bind();

		Environment.display.setIcon(Environment.io.getImageReader().readImage(Environment.io.createInternalFileHandle("textures/icon.png")));

		Environment.Analytics.glInitialized = true;
		eventManager.propagateInitializeEvent();
		Environment.Analytics.engineInitialized = true;
		eventManager.propagateResizeEvent();
		nativeLoop();
	}

	public static void exit()
	{
		display.window.setShouldClose(true);
	}

	private static void nativeLoop()
	{
		try
		{
			while (!display.window.shouldClose())
			{
				loop.preformLoop();
				display.window.swapBuffers();
			}
		}
		catch (Throwable thrown)
		{
			Environment.eventManager.propagateError(thrown);
			Exceptions.Exception.reThrow(thrown);
		}
		finally
		{
			Environment.eventManager.propagateDisposeEvent();
		}
	}

	public static String getVersionString()
	{
		return Environment.getVersion().toString();
	}

	public static Vector3 getVersion()
	{
		return new Vector3(0, 1, 1);
	}

	public static Platform getPlatform()
	{
		if (platform == null)
		{
			final String os = System.getProperty("os.name").toLowerCase();
			final String arch = System.getProperty("os.arch").toLowerCase();
			boolean isWindows = os.contains("windows");
			boolean isLinux = os.contains("linux");
			boolean isMac = os.contains("mac");
			boolean is64bit = arch.equals("amd64") || arch.equals("x86_64");
			platform = Platform.UNKNOWN;
			if (isWindows) platform = is64bit ? Platform.WINDOWS_64 : Platform.WINDOWS_32;
			if (isLinux) platform = is64bit ? Platform.LINUX_64 : Platform.UNKNOWN;
			if (isMac) platform = Platform.MACOSX;
		}
		return platform;
	}

	public static class Override
	{
		public static void loop(UpdateLoop loop)
		{
			Environment.loop = loop;
		}

		public static void graphics(Graphics graphics)
		{
			Environment.graphics = graphics;
		}

		public static void display(Display display)
		{
			Environment.display = display;
		}

		public static void audio(Audio audio)
		{
			Environment.audio = audio;
		}

		public static void io(IO io)
		{
			Environment.io = io;
		}

		public static void input(Input input)
		{
			Environment.input = input;
		}

		public static void log(Log log)
		{
			Environment.log = log;
		}
	}

	public static class Analytics
	{
		// flags
		public static boolean development = false;
		public static boolean verbose = false;
		public static boolean noLog = false;
		public static boolean engineStarted = false;
		public static boolean engineInitialized = false;
		public static boolean glInitialized = false;

		// statistics
		public static int totalRenderCalls = 0;
		public static int renderCallsThisFrame = 0;
		public static double lastRenderTime = 0;
		public static double averageRenderTime = 0;
		public static double lastUpdateTime = 0;
		public static double averageUpdateTime = 0;

		public static int getActiveTreadCount()
		{
			return Thread.activeCount();
		}

		public static int getAvailableProcessors()
		{
			return Runtime.getRuntime().availableProcessors();
		}

		public static long getFreeMemory()
		{
			return Runtime.getRuntime().freeMemory();
		}

		public static long getAvailableMemory()
		{
			return Runtime.getRuntime().totalMemory();
		}

		public static long getMaximumMemory()
		{
			return Runtime.getRuntime().maxMemory();
		}
	}

	public enum Platform
	{
		WINDOWS_32,
		WINDOWS_64,
		MACOSX,
		LINUX_64,
		UNKNOWN;
	}
}
