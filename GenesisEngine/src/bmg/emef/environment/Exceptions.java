package bmg.emef.environment;

public class Exceptions
{
	public static class Exception extends RuntimeException
	{
		private static final long serialVersionUID = 386421511537132832L;

		public Exception()
		{
			super();
			for (StackTraceElement e : this.getStackTrace())
			{
				Environment.log.error(e.toString());
			}
		}

		public Exception(String message)
		{
			super(message);
			for (StackTraceElement e : this.getStackTrace())
			{
				Environment.log.error(e.toString());
			}
		}

		public Exception(Throwable thrown)
		{
			super(thrown);
			for (StackTraceElement e : this.getStackTrace())
			{
				Environment.log.error(e.toString());
			}
		}

		public static void reThrow(Throwable thrown)
		{
			if (thrown instanceof Exceptions.Exception)
			{
				throw (Exceptions.Exception) thrown;
			}
			else
			{
				Environment.log.error(thrown.toString());
				throw new Exceptions.Exception(thrown);
			}
		}
	}

	public static class FileIOException extends Exception
	{
		private static final long serialVersionUID = -3502263771010410669L;

		public FileIOException(String message)
		{
			super(message);
		}
	}

	public static class GLException extends Exception
	{
		private static final long serialVersionUID = -3862801004981002783L;

		public GLException(String message)
		{
			super(message);
		}

		public static class InvalidEnum extends GLException
		{
			private static final long serialVersionUID = 824447367494587721L;

			public InvalidEnum()
			{
				super("An unacceptable value is specified for an enumerated argument");
			}
		}

		public static class InvalidValue extends GLException
		{
			private static final long serialVersionUID = 8869318426783094646L;

			public InvalidValue()
			{
				super("A numeric argument is out of range");
			}
		}

		public static class InvalidOperation extends GLException
		{
			private static final long serialVersionUID = 4568496375719538359L;

			public InvalidOperation()
			{
				super("The specified operation is not allowed in the current state");
			}
		}

		public static class InvalidFramebufferOperation extends GLException
		{
			private static final long serialVersionUID = 7740644240964935965L;

			public InvalidFramebufferOperation()
			{
				super("The FrameBuffer object is incomplete");
			}
		}

		public static class OutOfMemory extends GLException
		{
			private static final long serialVersionUID = -3869204036157277718L;

			public OutOfMemory()
			{
				super("There is not enough memory left to execute the command");
			}
		}
	}

	public static class ALException extends Exception
	{
		private static final long serialVersionUID = -1653823352934745344L;

		public ALException(String message)
		{
			super(message);
		}

		public static class InvalidDevice extends ALException
		{
			private static final long serialVersionUID = -7327788121048507546L;

			public InvalidDevice()
			{
				super("Invalid or no Device selected");
			}
		}

		public static class InvalidContext extends ALException
		{
			private static final long serialVersionUID = 4584866324853496930L;

			public InvalidContext()
			{
				super("Invalid or no Context selected");
			}
		}

		public static class InvalidName extends ALException
		{
			private static final long serialVersionUID = -4633990176372182025L;

			public InvalidName()
			{
				super("Invalid name parameter passed");
			}
		}

		public static class InvalidEnum extends ALException
		{
			private static final long serialVersionUID = 7002400681375929660L;

			public InvalidEnum()
			{
				super("Invalid enum value");
			}
		}

		public static class InvalidValue extends ALException
		{
			private static final long serialVersionUID = -3016404681526602287L;

			public InvalidValue()
			{
				super("Invalid parameter value");
			}
		}

		public static class InvalidOperation extends ALException
		{
			private static final long serialVersionUID = 5889796028090524413L;

			public InvalidOperation()
			{
				super("Invalid OpenAL Operation");
			}
		}

		public static class OutOfMemory extends ALException
		{
			private static final long serialVersionUID = 6088809051138069423L;

			public OutOfMemory()
			{
				super("OpenAL ran out of memory");
			}
		}
	}
}
