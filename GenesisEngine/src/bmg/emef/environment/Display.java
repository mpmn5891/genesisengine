package bmg.emef.environment;

import org.lwjgl.glfw.GLFW;

import bmg.emef.glfw.GLFW3;
import bmg.emef.glfw.Monitor;
import bmg.emef.glfw.VideoMode;
import bmg.emef.glfw.Window;
import bmg.emef.graphics.Image;
import bmg.emef.io.FileHandle;
import bmg.emef.math.Vector2;

public class Display
{
	public Window window;
	private int windowWidth;
	private int windowHeight;
	private int windowPositionX;
	private int windowPositionY;
	private boolean fullscreen;
	private boolean focus = true;
	private boolean cursorGrabbed = false;

	public Display()
	{
		GLFW3.init();
		Window.setHint(GLFW.GLFW_OPENGL_DEBUG_CONTEXT, Environment.Analytics.development);
		Window.setHint(GLFW.GLFW_OPENGL_PROFILE, GLFW.GLFW_OPENGL_CORE_PROFILE);
		Window.setHint(GLFW.GLFW_OPENGL_FORWARD_COMPAT, GLFW.GLFW_TRUE);
		Window.setHint(GLFW.GLFW_CONTEXT_VERSION_MAJOR, 3);
		Window.setHint(GLFW.GLFW_CONTEXT_VERSION_MINOR, 2);
		windowWidth = 800;
		windowHeight = 600;
		window = new Window(windowWidth, windowHeight, "EngineMcEngineFace");
		window.setPositionCallback((window1, xPos, yPos) ->
		{
			if (!fullscreen)
			{
				windowPositionX = xPos;
				windowPositionY = yPos;
			}
			Environment.eventManager.propagateResizeEvent();
		});
		window.setFocusCallback((w, f) ->
		{
			focus = f;
			if (focus)
			{
				Environment.eventManager.propagateFocusEvent(true);
			}
			else
			{
				Environment.eventManager.propagateFocusEvent(false);
			}
		});
		Environment.eventManager.registerDisposeListener(this::cleanUp);
		GLFW3.setSwapInterval(1);
	}

	public void setSize(int width, int height)
	{
		window.setSize(width, height);
		if (!fullscreen)
		{
			windowWidth = width;
			windowHeight = height;
		}
		Environment.eventManager.propagateResizeEvent();
	}

	public boolean isFullscreen()
	{
		return fullscreen;
	}

	public void setFullscreen(boolean fullscreen)
	{
		if (fullscreen)
		{
			this.fullscreen = true;
			window.setMonitor(Monitor.getPrimaryMonitor());
		}
		else
		{
			window.setMonitor(null);
			window.setSize(windowWidth, windowHeight);
			window.setPosition(windowPositionX, windowPositionY);
			this.fullscreen = false;
		}
		Environment.eventManager.propagateResizeEvent();
	}

	public void centerOnScreen()
	{
		VideoMode mode = Monitor.getPrimaryMonitor().getVideoMode();
		Vector2 windowPosition = window.getSize();
		windowPosition.x = (mode.getWidth() - windowPosition.x) / 2;
		windowPosition.y = (mode.getHeight() - windowPosition.y) / 2;
		window.setPosition(windowPosition);
		windowPositionX = (int) windowPosition.x;
		windowPositionY = (int) windowPosition.y;
	}

	public void setPosition(int x, int y)
	{
		window.setPosition(x, y);

		windowPositionX = x;
		windowPositionY = y;
	}

	public int getWidth()
	{
		return (int) window.getSize().x;
	}

	public int getHeight()
	{
		return (int) window.getSize().y;
	}

	public float getAspectRatio()
	{
		return (float) getWidth() / (float) getHeight();
	}

	public String getTitle()
	{
		return window.getTitle();
	}

	public void setTitle(String title)
	{
		window.setTitle(title);
	}

	public void setIcon(FileHandle path)
	{
		window.setIcon(path);
	}

	public void setIcon(Image image)
	{
		window.setIcon(image);
	}

	public void close()
	{
		window.setShouldClose(true);
	}

	public double nanoTime()
	{
		return System.nanoTime();
	}

	public void setVSync(boolean vSync)
	{
		GLFW3.setSwapInterval(vSync ? 1 : 0);
	}

	public boolean hasFocus()
	{
		return focus;
	}

	public void setGrabMouse(boolean grabMouse)
	{
		if (grabMouse)
		{
			cursorGrabbed = true;
			window.setInputMode(GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_DISABLED);
		}
		else
		{
			cursorGrabbed = false;
			window.setInputMode(GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_NORMAL);
		}
	}

	public boolean isCursorGrabbed()
	{
		return cursorGrabbed;
	}

	private void cleanUp()
	{
		window.destroy();
		GLFW3.terminate();
	}
}
