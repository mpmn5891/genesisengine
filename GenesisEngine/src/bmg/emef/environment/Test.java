package bmg.emef.environment;

import bmg.emef.Input.Keyboard;
import bmg.emef.buffer.DirectFloatBuffer;
import bmg.emef.graphics.Color;
import bmg.emef.graphics.Graphics;
import bmg.emef.graphics.Image;
import bmg.emef.graphics.gl.ArrayObject;
import bmg.emef.graphics.gl.BufferObject;
import bmg.emef.graphics.gl.GLContext;
import bmg.emef.graphics.gl.Program;
import bmg.emef.graphics.gl.Shader;
import bmg.emef.graphics.gl.Texture;
import bmg.emef.io.FileHandle;
import bmg.emef.math.Transform;
import bmg.emef.math.Vector3;

public class Test
{
	public static void main(String[] args)
	{
		new Test();
	}

	public Test()
	{
		Environment.Analytics.verbose = true;
		Environment.eventManager.registerInitializeListener(this::init);
		Environment.eventManager.registerUpdateListener(this::update);
		Environment.eventManager.registerRenderListener(this::render);
		Environment.eventManager.registerResizeListener(this::resized);
		Environment.eventManager.registerFocusListener(this::focus);
		Environment.eventManager.registerDisposeListener(this::dispose);
		Environment.start();
	}

	private Texture texture;
	private BufferObject vertexBuffer, texCoordBuffer;
	private ArrayObject vao;
	private Transform transform;
	private Program program;

	public void init()
	{
		Environment.log.debug("initialized");
		Environment.display.setTitle("OPEN_GL_TEST");
		Environment.display.centerOnScreen();
		transform = new Transform();
		vao = new ArrayObject();
		vao.bind();

		GLContext.clearColor(Color.CYAN);
		GLContext.enable(Graphics.Constants.GL_BLEND);
		GLContext.blendFunc(Graphics.Constants.GL_SRC_ALPHA, Graphics.Constants.GL_ONE_MINUS_SRC_ALPHA);

		Image image = Environment.io.getImageReader().readImage(FileHandle.getInternalFile("textures/icon.png"));
		texture = Texture.fromImage(image);
		Environment.display.setIcon(image);
		image.dispose();

		// The vertex shader source
		String vsSource = "#version 400 core                             \n" +
				"uniform mat4 transform;                                 \n" +
				"in vec2 position;                                       \n" +
				"in vec2 texCoords;                                      \n" +
				"                                                        \n" +
				"out vec2 vTexCoords;                                    \n" +
				"                                                        \n" +
				"void main()                                             \n" +
				"{                                                       \n" +
				"    vTexCoords = texCoords;                             \n" +
				"    gl_Position = transform * vec4(position, 0.0, 1.0); \n" +
				"}";
		Shader vShader = new Shader(Shader.Type.VERTEX);
		vShader.source(vsSource);
		vShader.compile();

		// The fragment shader source
		String fsSource = "#version 400 core                       \n" +
				"uniform sampler2D texUnit;                        \n" +
				"in vec2 vTexCoords;                               \n" +
				"                                                  \n" +
				"void main()                                       \n" +
				"{                                                 \n" +
				"    gl_FragColor = texture2D(texUnit, vTexCoords); \n" +
				"}";
		Shader fShader = new Shader(Shader.Type.FRAGMENT);
		fShader.source(fsSource);
		fShader.compile();

		program = new Program();
		program.attach(vShader);
		program.attach(fShader);
		program.link();
		program.setUniform("texUnit", 0);

		float[] vertices =
		{
				-0.8f, +0.8f,
				+0.8f, +0.8f,
				-0.8f, -0.8f,

				+0.8f, +0.8f,
				+0.8f, -0.8f,
				-0.8f, -0.8f
		};
		DirectFloatBuffer verticesBuffer = new DirectFloatBuffer(vertices.length);
		for (int i = 0; i < vertices.length; i++)
		{
			verticesBuffer.write(i, vertices[i]);
		}
		vertexBuffer = new BufferObject(BufferObject.Target.ARRAY_BUFFER);
		vertexBuffer.uploadData(verticesBuffer.getDirectBuffer(), BufferObject.Usage.STATIC_DRAW);
		verticesBuffer.getDirectBuffer().free();

		int positionloc = program.getAttribute("position");
		vao.pointAttribute(positionloc, 2, Graphics.Constants.GL_FLOAT, vertexBuffer);
		vao.enableAttributeArray(positionloc);

		float[] texCoords =
		{
				0, 0,
				1, 0,
				0, 1,

				1, 0,
				1, 1,
				0, 1
		};
		DirectFloatBuffer texCoordsBuffer = new DirectFloatBuffer(texCoords.length);
		for (int i = 0; i < texCoords.length; i++)
		{
			texCoordsBuffer.write(i, texCoords[i]);
		}
		texCoordBuffer = new BufferObject(BufferObject.Target.ARRAY_BUFFER);
		texCoordBuffer.uploadData(texCoordsBuffer.getDirectBuffer(), BufferObject.Usage.STATIC_DRAW);
		texCoordsBuffer.getDirectBuffer().free();

		int texCoordloc = program.getAttribute("texCoords");
		vao.pointAttribute(texCoordloc, 2, Graphics.Constants.GL_FLOAT, texCoordBuffer);
		vao.enableAttributeArray(texCoordloc);
	}

	public void update(float delta)
	{
		if (Keyboard.isKeyDown(Keyboard.QWERTY.KEY_ESCAPE))
		{
			Environment.display.close();
		}
		if (Keyboard.isKeyDown(Keyboard.QWERTY.KEY_F))
		{
			Environment.display.setFullscreen(!Environment.display.isFullscreen());
		}
		Environment.display.setTitle("OPEN_GL_TEST" + Environment.getVersionString() + " - FPS :: " + Environment.loop.getFPS());
		transform.rotate(Vector3.AXIS_Y, 45 * delta);
		program.setUniform("transform", transform.matrix);
	}

	public void render(float delta)
	{
		texture.bind(0);
		program.use();
		GLContext.drawArrays(vao, Graphics.Primitive.TRIANGLES, 0, 6);
	}

	public void resized()
	{
		Environment.graphics.glViewport(0, 0, Environment.display.getWidth(), Environment.display.getHeight());
		Environment.log.debug("Resized");
	}

	public void focus(boolean hasFocus)
	{
		Environment.log.debug("focus changed :: " + hasFocus);
	}

	public void dispose()
	{
		Environment.log.debug("disposed");
		GLContext.bindVertexArrayObject(null);
		GLContext.bindVertexBufferObject(null);
		program.dispose();
		vertexBuffer.dispose();
		texCoordBuffer.dispose();
		vao.dispose();
		if (texture != null)
		{
			texture.dispose();
		}
	}
}
