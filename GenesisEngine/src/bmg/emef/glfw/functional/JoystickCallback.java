package bmg.emef.glfw.functional;

@FunctionalInterface
public interface JoystickCallback
{
	void invoke(int joystick, boolean connected);
}
