package bmg.emef.glfw.functional;

import bmg.emef.glfw.Window;

@FunctionalInterface
public interface WindowIconifyCallback
{
	void invoke(Window window, boolean iconify);
}
