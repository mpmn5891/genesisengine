package bmg.emef.glfw.functional;

import bmg.emef.glfw.Window;

@FunctionalInterface
public interface ScrollCallback
{
	void invoke(Window window, double xOffset, double yOffset);
}
