package bmg.emef.glfw.functional;

import bmg.emef.glfw.Window;

@FunctionalInterface
public interface FramebufferSizeCallback
{
	void invoke(Window window, int width, int height);
}
