package bmg.emef.glfw.functional;

import bmg.emef.glfw.Window;

@FunctionalInterface
public interface WindowSizeCallback
{
	void invoke(Window window, int width, int height);
}
