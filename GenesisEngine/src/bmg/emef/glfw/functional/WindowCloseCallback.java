package bmg.emef.glfw.functional;

import bmg.emef.glfw.Window;

@FunctionalInterface
public interface WindowCloseCallback
{
	void invoke(Window window);
}
