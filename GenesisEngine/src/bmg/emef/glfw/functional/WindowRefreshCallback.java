package bmg.emef.glfw.functional;

import bmg.emef.glfw.Window;

@FunctionalInterface
public interface WindowRefreshCallback
{
	void invoke(Window window);
}
