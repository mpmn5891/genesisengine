package bmg.emef.glfw.functional;

import bmg.emef.glfw.Window;

@FunctionalInterface
public interface CharacterModsCallback
{
	void invoke(Window window, int codePoint, int mods);
}
