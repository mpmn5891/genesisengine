package bmg.emef.glfw.functional;

import bmg.emef.glfw.Window;

@FunctionalInterface
public interface CursorEnterCallback
{
	void invoke(Window window, boolean entered);
}
