package bmg.emef.glfw.functional;

@FunctionalInterface
public interface ErrorCallback
{
	void invoke(int error, String description);
}
