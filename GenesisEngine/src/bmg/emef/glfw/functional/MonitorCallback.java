package bmg.emef.glfw.functional;

import bmg.emef.glfw.Monitor;

@FunctionalInterface
public interface MonitorCallback
{
	void invoke(Monitor monitor, int event);
}
