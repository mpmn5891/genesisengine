package bmg.emef.glfw.functional;

import bmg.emef.glfw.Window;

@FunctionalInterface
public interface DropCallback
{
	void invoke(Window window, String[] paths);
}
