package bmg.emef.glfw.functional;

import bmg.emef.glfw.Window;

@FunctionalInterface
public interface CharacterCallback
{
	void invoke(Window window, int codePoint);
}
