package bmg.emef.glfw.functional;

import bmg.emef.glfw.Window;

@FunctionalInterface
public interface WindowPositionCallback
{
	void invoke(Window window, int xPosition, int yPosition);
}
