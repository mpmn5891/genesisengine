package bmg.emef.glfw.functional;

import bmg.emef.glfw.Window;

@FunctionalInterface
public interface MouseButtonCallback
{
	void invoke(Window window, int button, int action, int mods);
}
