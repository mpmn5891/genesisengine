package bmg.emef.glfw.functional;

import bmg.emef.glfw.Window;

@FunctionalInterface
public interface CursorPositionCallback
{
	void invoke(Window window, double xPosition, double yPosition);
}
