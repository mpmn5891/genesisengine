package bmg.emef.glfw.functional;

import bmg.emef.glfw.Window;

@FunctionalInterface
public interface WindowFocusCallback
{
	void invoke(Window window, boolean focus);
}
