package bmg.emef.glfw.functional;

import bmg.emef.glfw.Window;

@FunctionalInterface
public interface KeyCallback
{
	void invoke(Window window, int key, int scanCode, int action, int mods);
}
