package bmg.emef.glfw;

import static org.lwjgl.glfw.GLFW.GLFW_REFRESH_RATE;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDefaultWindowHints;
import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
import static org.lwjgl.glfw.GLFW.glfwGetClipboardString;
import static org.lwjgl.glfw.GLFW.glfwGetCurrentContext;
import static org.lwjgl.glfw.GLFW.glfwGetKey;
import static org.lwjgl.glfw.GLFW.glfwGetMouseButton;
import static org.lwjgl.glfw.GLFW.glfwGetWindowAttrib;
import static org.lwjgl.glfw.GLFW.glfwHideWindow;
import static org.lwjgl.glfw.GLFW.glfwIconifyWindow;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwMaximizeWindow;
import static org.lwjgl.glfw.GLFW.glfwRestoreWindow;
import static org.lwjgl.glfw.GLFW.glfwSetCharCallback;
import static org.lwjgl.glfw.GLFW.glfwSetCharModsCallback;
import static org.lwjgl.glfw.GLFW.glfwSetClipboardString;
import static org.lwjgl.glfw.GLFW.glfwSetCursor;
import static org.lwjgl.glfw.GLFW.glfwSetCursorEnterCallback;
import static org.lwjgl.glfw.GLFW.glfwSetCursorPos;
import static org.lwjgl.glfw.GLFW.glfwSetCursorPosCallback;
import static org.lwjgl.glfw.GLFW.glfwSetDropCallback;
import static org.lwjgl.glfw.GLFW.glfwSetFramebufferSizeCallback;
import static org.lwjgl.glfw.GLFW.glfwSetInputMode;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetMouseButtonCallback;
import static org.lwjgl.glfw.GLFW.glfwSetScrollCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowCloseCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowFocusCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowIcon;
import static org.lwjgl.glfw.GLFW.glfwSetWindowIconifyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowMonitor;
import static org.lwjgl.glfw.GLFW.glfwSetWindowPos;
import static org.lwjgl.glfw.GLFW.glfwSetWindowPosCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowRefreshCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose;
import static org.lwjgl.glfw.GLFW.glfwSetWindowSize;
import static org.lwjgl.glfw.GLFW.glfwSetWindowSizeCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowTitle;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
import static org.lwjgl.glfw.GLFW.nglfwGetCursorPos;
import static org.lwjgl.glfw.GLFW.nglfwGetFramebufferSize;
import static org.lwjgl.glfw.GLFW.nglfwGetWindowFrameSize;
import static org.lwjgl.glfw.GLFW.nglfwGetWindowPos;
import static org.lwjgl.glfw.GLFW.nglfwGetWindowSize;
import static org.lwjgl.system.MemoryUtil.memAddress;

import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFWCharCallback;
import org.lwjgl.glfw.GLFWCharModsCallback;
import org.lwjgl.glfw.GLFWCursorEnterCallback;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWDropCallback;
import org.lwjgl.glfw.GLFWFramebufferSizeCallback;
import org.lwjgl.glfw.GLFWImage;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWScrollCallback;
import org.lwjgl.glfw.GLFWWindowCloseCallback;
import org.lwjgl.glfw.GLFWWindowFocusCallback;
import org.lwjgl.glfw.GLFWWindowIconifyCallback;
import org.lwjgl.glfw.GLFWWindowPosCallback;
import org.lwjgl.glfw.GLFWWindowRefreshCallback;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GLCapabilities;
import org.lwjgl.system.MemoryUtil;

import bmg.emef.environment.Environment;
import bmg.emef.glfw.functional.CharacterCallback;
import bmg.emef.glfw.functional.CharacterModsCallback;
import bmg.emef.glfw.functional.CursorEnterCallback;
import bmg.emef.glfw.functional.CursorPositionCallback;
import bmg.emef.glfw.functional.DropCallback;
import bmg.emef.glfw.functional.FramebufferSizeCallback;
import bmg.emef.glfw.functional.KeyCallback;
import bmg.emef.glfw.functional.MouseButtonCallback;
import bmg.emef.glfw.functional.ScrollCallback;
import bmg.emef.glfw.functional.WindowCloseCallback;
import bmg.emef.glfw.functional.WindowFocusCallback;
import bmg.emef.glfw.functional.WindowIconifyCallback;
import bmg.emef.glfw.functional.WindowPositionCallback;
import bmg.emef.glfw.functional.WindowRefreshCallback;
import bmg.emef.glfw.functional.WindowSizeCallback;
import bmg.emef.graphics.Color;
import bmg.emef.graphics.Image;
import bmg.emef.io.FileHandle;
import bmg.emef.math.Vector2;
import bmg.emef.math.Vector4;
import bmg.emef.util.MathUtils;

public class Window
{
	private static Map<Long, Window> registeredWindows = new HashMap<>();

	public static void setHint(int hint, boolean value)
	{
		setHint(hint, value ? 1 : 0);
	}

	public static void setHint(int hint, int value)
	{
		glfwWindowHint(hint, value);
	}

	public static void setDefaultHints()
	{
		glfwDefaultWindowHints();
	}

	public static Window getCurrentContext()
	{
		return Window.registeredWindows.get(glfwGetCurrentContext());
	}

	private long handle;
	private Vector2 position;
	private Vector2 size;
	private Vector2 framebufferSize;
	private String title;
	private Monitor monitor;

	private GLFWCharCallback glfwCharCallback;
	private GLFWCharModsCallback glfwCharModsCallback;
	private GLFWCursorEnterCallback glfwCursorEnterCallback;
	private GLFWCursorPosCallback glfwCursorPosCallback;
	private GLFWDropCallback glfwDropCallback;
	private GLFWFramebufferSizeCallback glfwFramebufferSizeCallback;
	private GLFWKeyCallback glfwKeyCallback;
	private GLFWMouseButtonCallback glfwMouseButtonCallback;
	private GLFWScrollCallback glfwScrollCallback;
	private GLFWWindowCloseCallback glfwWindowCloseCallback;
	private GLFWWindowFocusCallback glfwWindowFocusCallback;
	private GLFWWindowIconifyCallback glfwWindowIconifyCallback;
	private GLFWWindowPosCallback glfwWindowPosCallback;
	private GLFWWindowRefreshCallback glfwWindowRefreshCallback;
	private GLFWWindowSizeCallback glfwWindowSizeCallback;

	private CharacterCallback characterCallback;
	private CharacterModsCallback characterModsCallback;
	private CursorEnterCallback cursorEnterCallback;
	private CursorPositionCallback cursorPositionCallback;
	private DropCallback dropCallback;
	private FramebufferSizeCallback framebufferSizeCallback;
	private KeyCallback keyCallback;
	private MouseButtonCallback mouseButtonCallback;
	private ScrollCallback scrollCallback;
	private WindowCloseCallback windowCloseCallback;
	private WindowFocusCallback windowFocusCallback;
	private WindowIconifyCallback windowIconifyCallback;
	private WindowPositionCallback windowPositionCallback;
	private WindowRefreshCallback windowRefreshCallback;
	private WindowSizeCallback windowSizeCallback;

	private GLCapabilities windowCapabilities;

	public Window()
	{
		this(800, 600);
	}

	public Window(int width, int height)
	{
		this(width, height, (Monitor) null, null);
	}

	public Window(int width, int height, Monitor monitor, Window share)
	{
		this(width, height, "EngineMcEngineFace", monitor, share);
	}

	public Window(int width, int height, String title, Monitor monitor, Window share)
	{
		this(width, height, title, monitor, (monitor == null ? Monitor.getPrimaryMonitor() : monitor).getVideoMode().getRefreshRate(), share);
	}

	public Window(Monitor monitor)
	{
		this(monitor != null ? monitor.getVideoMode().getWidth() : 800, monitor != null ? monitor.getVideoMode().getHeight() : 600, monitor);
	}

	public Window(int width, int height, Monitor monitor)
	{
		this(width, height, "EngineMcEngineFace", monitor);
	}

	public Window(int width, int height, String title, Monitor monitor)
	{
		this(width, height, title, monitor, null);
	}

	public Window(Window share)
	{
		this(800, 600, share);
	}

	public Window(int width, int height, Window share)
	{
		this(width, height, "EngineMcEngineFace", share);
	}

	public Window(int width, int height, String title, Window share)
	{
		this(width, height, title, null, share);
	}

	public Window(String title)
	{
		this(800, 600, title);
	}

	public Window(int width, int height, String title)
	{
		this(width, height, title, (Monitor) null);
	}

	public Window(VideoMode videoMode, Monitor monitor)
	{
		this(videoMode, "EngineMcEngineFace", monitor);
	}

	public Window(VideoMode videoMode, String title, Monitor monitor)
	{
		this(videoMode, title, monitor, null);
	}

	public Window(VideoMode videoMode, String title, Monitor monitor, Window share)
	{
		this(videoMode.getWidth(), videoMode.getHeight(), title, monitor, videoMode.getRefreshRate(), share);
	}

	public Window(int width, int height, String title, Monitor monitor, int refreshRate, Window share)
	{
		size = new Vector2(width, height);
		framebufferSize = new Vector2();
		position = new Vector2();
		this.title = title;
		this.monitor = monitor;
		Window.setHint(GLFW_REFRESH_RATE, refreshRate);
		handle = glfwCreateWindow(width, height, title, monitor == null ? MemoryUtil.NULL : monitor.getHandle(), share == null ? MemoryUtil.NULL : share.getHandle());
		if (handle == MemoryUtil.NULL) throw new RuntimeException("Can not create window");
		Window.registeredWindows.put(handle, this);
		initNativeCallbacks();
	}

	public long getHandle()
	{
		return handle;
	}

	private void initNativeCallbacks()
	{
		initCustomCallbacks();
		glfwCharCallback = GLFWCharCallback.create((window, codePoint) -> characterCallback.invoke(Window.registeredWindows.get(window), codePoint));
		glfwCharModsCallback = GLFWCharModsCallback.create((window, codePoint, mods) -> characterModsCallback.invoke(Window.registeredWindows.get(window), codePoint, mods));
		glfwCursorEnterCallback = GLFWCursorEnterCallback.create((window, entered) -> cursorEnterCallback.invoke(Window.registeredWindows.get(window), entered));
		glfwCursorPosCallback = GLFWCursorPosCallback.create((window, xPos, yPos) ->
		{
			Window registeredWindow = Window.registeredWindows.get(window);
			cursorPositionCallback.invoke(registeredWindow, xPos, yPos);
		});
		glfwDropCallback = GLFWDropCallback.create((window, count, names) ->
		{
			String[] strings = new String[count];
			for (int i = 0; i < count; i++)
			{
				strings[i] = GLFWDropCallback.getName(names, i);
			}
			dropCallback.invoke(Window.registeredWindows.get(window), strings);
		});
		glfwFramebufferSizeCallback = GLFWFramebufferSizeCallback
				.create((window, width, height) -> framebufferSizeCallback.invoke(Window.registeredWindows.get(window), width, height));
		glfwKeyCallback = GLFWKeyCallback.create((window, key, scanCode, action, mods) ->
		{
			Window registeredWindow = Window.registeredWindows.get(window);
			keyCallback.invoke(registeredWindow, key, scanCode, action, mods);
		});
		glfwMouseButtonCallback = GLFWMouseButtonCallback.create((win, button, action, mods) ->
		{
			Window window = registeredWindows.get(win);
			mouseButtonCallback.invoke(window, button, action, mods);
		});
		glfwScrollCallback = GLFWScrollCallback.create((window, xOffset, yOffset) ->
		{
			Window registeredWindow = Window.registeredWindows.get(window);
			scrollCallback.invoke(registeredWindow, xOffset, yOffset);
		});
		glfwWindowCloseCallback = GLFWWindowCloseCallback.create((window) -> windowCloseCallback.invoke(Window.registeredWindows.get(window)));
		glfwWindowFocusCallback = GLFWWindowFocusCallback.create((window, focus) -> windowFocusCallback.invoke(Window.registeredWindows.get(window), focus));
		glfwWindowIconifyCallback = GLFWWindowIconifyCallback.create((window, iconify) -> windowIconifyCallback.invoke(Window.registeredWindows.get(window), iconify));
		glfwWindowPosCallback = GLFWWindowPosCallback
				.create((window, xPosition, yPosition) -> windowPositionCallback.invoke(Window.registeredWindows.get(window), xPosition, yPosition));
		glfwWindowRefreshCallback = GLFWWindowRefreshCallback.create((window) -> windowRefreshCallback.invoke(Window.registeredWindows.get(window)));
		glfwWindowSizeCallback = GLFWWindowSizeCallback.create((window, width, height) -> windowSizeCallback.invoke(Window.registeredWindows.get(window), width, height));
		glfwSetCharCallback(handle, glfwCharCallback);
		glfwSetCharModsCallback(handle, glfwCharModsCallback);
		glfwSetCursorEnterCallback(handle, glfwCursorEnterCallback);
		glfwSetCursorPosCallback(handle, glfwCursorPosCallback);
		glfwSetDropCallback(handle, glfwDropCallback);
		glfwSetFramebufferSizeCallback(handle, glfwFramebufferSizeCallback);
		glfwSetKeyCallback(handle, glfwKeyCallback);
		glfwSetMouseButtonCallback(handle, glfwMouseButtonCallback);
		glfwSetScrollCallback(handle, glfwScrollCallback);
		glfwSetWindowCloseCallback(handle, glfwWindowCloseCallback);
		glfwSetWindowFocusCallback(handle, glfwWindowFocusCallback);
		glfwSetWindowIconifyCallback(handle, glfwWindowIconifyCallback);
		glfwSetWindowPosCallback(handle, glfwWindowPosCallback);
		glfwSetWindowRefreshCallback(handle, glfwWindowRefreshCallback);
		glfwSetWindowSizeCallback(handle, glfwWindowSizeCallback);
	}

	private void initCustomCallbacks()
	{
		setCharacterCallback(null);
		setCharacterModsCallback(null);
		setCursorEnterCallback(null);
		setCursorPositionCallback(null);
		setDropCallback(null);
		setFramebufferSizeCallback(null);
		setKeyCallback(null);
		setMouseButtonCallback(null);
		setScrollCallback(null);
		setCloseCallback(null);
		setFocusCallback(null);
		setIconifyCallback(null);
		setPositionCallback(null);
		setRefreshCallback(null);
		setSizeCallback(null);
	}

	public int getKey(int key)
	{
		return glfwGetKey(handle, key);
	}

	public int getMouseButton(int button)
	{
		return glfwGetMouseButton(handle, button);
	}

	public Vector2 getCursorPosition()
	{
		DoubleBuffer pos = BufferUtils.createDoubleBuffer(2);
		nglfwGetCursorPos(handle, memAddress(pos), memAddress(pos) + Double.BYTES);
		return new Vector2((float) pos.get(0), (float) pos.get(1));
	}

	public void setCursorPosition(Vector2 position)
	{
		setCursorPosition(position.x, position.y);
	}

	public void setCursorPosition(double x, double y)
	{
		glfwSetCursorPos(handle, x, y);
	}

	public void setInputMode(int mode, int value)
	{
		glfwSetInputMode(handle, mode, value);
	}

	public void setCursor(Cursor cursor)
	{
		glfwSetCursor(handle, cursor == null ? MemoryUtil.NULL : cursor.getHandle());
	}

	public void makeCurrent()
	{
		glfwMakeContextCurrent(handle);
		if (windowCapabilities == null)
		{
			windowCapabilities = GL.createCapabilities();
		}
		else
		{
			GL.setCapabilities(windowCapabilities);
		}
	}

	public void setMonitor(Monitor monitor, VideoMode videoMode)
	{
		this.monitor = monitor;
		if (videoMode == null)
		{
			Vector2 size = getSize();
			videoMode = new VideoMode((int) size.x, (int) size.y, 32, 32, 32, 32);
		}
		glfwSetWindowMonitor(handle, monitor == null ? MemoryUtil.NULL : monitor.getHandle(), 0, 0, videoMode.getWidth(), videoMode.getHeight(), videoMode.getRefreshRate());
	}

	public void swapBuffers()
	{
		glfwSwapBuffers(handle);
	}

	public boolean shouldClose()
	{
		return glfwWindowShouldClose(handle);
	}

	public void maximize()
	{
		glfwMaximizeWindow(handle);
	}

	public void setIcon(Image image)
	{
		int width = image.getWidth();
		int height = image.getHeight();
		GLFWImage.Buffer glfwImages = GLFWImage.calloc(1);
		GLFWImage glfwImage = glfwImages.get(0);
		glfwImage.width(width).height(height);
		ByteBuffer data = BufferUtils.createByteBuffer(width * height * 4);
		Color color = Color.REUSABLE_STACK.pop();
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				image.getPixel(x, y, color);
				data.put((byte) (color.r * 255f)).put((byte) (color.g * 255f)).put((byte) (color.b * 255f)).put((byte) (color.a * 255f));
			}
		}
		Color.REUSABLE_STACK.push(color);
		data.flip();
		glfwImage.pixels(data);
		glfwSetWindowIcon(handle, glfwImages);
		glfwImages.free();
	}

	public void setIcon(FileHandle path)
	{
		Image image = Environment.io.getImageReader().readImage(path);
		setIcon(image);
	}

	public void setShouldClose(boolean value)
	{
		glfwSetWindowShouldClose(handle, value);
	}

	public void iconify()
	{
		glfwIconifyWindow(handle);
	}

	public void restore()
	{
		glfwRestoreWindow(handle);
	}

	public void hide()
	{
		glfwHideWindow(handle);
	}

	public void show()
	{
		glfwShowWindow(handle);
	}

	public void destroy()
	{
		releaseNativeCallbacks();
		glfwDestroyWindow(handle);
	}

	private void releaseNativeCallbacks()
	{
		glfwCharCallback.free();
		glfwCharModsCallback.free();
		glfwCursorEnterCallback.free();
		glfwCursorPosCallback.free();
		glfwDropCallback.free();
		glfwFramebufferSizeCallback.free();
		glfwKeyCallback.free();
		glfwMouseButtonCallback.free();
		glfwScrollCallback.free();
		glfwWindowCloseCallback.free();
		glfwWindowFocusCallback.free();
		glfwWindowIconifyCallback.free();
		glfwWindowPosCallback.free();
		glfwWindowRefreshCallback.free();
		glfwWindowSizeCallback.free();
	}

	public Vector2 getPosition()
	{
		IntBuffer pos = BufferUtils.createIntBuffer(2);
		nglfwGetWindowPos(handle, memAddress(pos), memAddress(pos) + Integer.BYTES);
		return position.set(pos.get(0), pos.get(1));
	}

	public void setPosition(Vector2 position)
	{
		this.position.set(position);
		glfwSetWindowPos(handle, (int) position.x, (int) position.y);
	}

	public void setPosition(float x, float y)
	{
		setPosition(position.set(x, y));
	}

	public Vector2 getSize()
	{
		IntBuffer size = BufferUtils.createIntBuffer(2);
		nglfwGetWindowSize(handle, memAddress(size), memAddress(size) + Integer.BYTES);
		return this.size.set(size.get(0), size.get(1));
	}

	public void setSize(Vector2 size)
	{
		this.size.set(size);
		glfwSetWindowSize(handle, (int) size.x, (int) size.y);
	}

	public void setSize(float width, float height)
	{
		setSize(size.set(width, height));
	}

	public Vector2 getFramebufferSize()
	{
		IntBuffer size = BufferUtils.createIntBuffer(2);
		nglfwGetFramebufferSize(handle, memAddress(size), memAddress(size) + Integer.BYTES);
		return framebufferSize.set(size.get(0), size.get(1));
	}

	public void setFramebufferSize(Vector2 fbSize)
	{
		Vector2 temp = Vector2.REUSABLE_STACK.pop();
		Vector2 framebufferSize = getFramebufferSize();
		Vector2 size = getSize();
		temp.set(size).scale(1 / framebufferSize.x, 1 / framebufferSize.y);
		this.framebufferSize.set(fbSize);
		size.set(fbSize).scale(temp.x, temp.y);
		setSize(size);
		Vector2.REUSABLE_STACK.push(temp);
	}

	public void setFramebufferSize(float width, float height)
	{
		Vector2 temp = Vector2.REUSABLE_STACK.pop();
		setFramebufferSize(temp.set(width, height));
		Vector2.REUSABLE_STACK.push(temp);
	}

	public String getClipboardString()
	{
		return glfwGetClipboardString(handle);
	}

	public void setClipBoardString(String string)
	{
		glfwSetClipboardString(handle, string);
	}

	public Vector4 getFrameSize()
	{
		IntBuffer size = BufferUtils.createIntBuffer(4);
		long left = memAddress(size);
		long top = left + Integer.BYTES;
		long right = top + Integer.BYTES;
		long bottom = right + Integer.BYTES;
		nglfwGetWindowFrameSize(handle, left, top, right, bottom);
		return new Vector4(size.get(0), size.get(1), size.get(2), size.get(3));
	}

	public Monitor getMonitor()
	{
		return monitor;
	}

	public void setMonitor(Monitor monitor)
	{
		setMonitor(monitor, monitor == null ? null : monitor.getVideoMode());
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;

		glfwSetWindowTitle(handle, title);
	}

	public int getAttribute(int attribute)
	{
		return glfwGetWindowAttrib(handle, attribute);
	}

	public CharacterCallback getCharacterCallback()
	{
		return characterCallback;
	}

	public CharacterCallback setCharacterCallback(CharacterCallback callback)
	{
		CharacterCallback previousCallback = characterCallback;
		if (callback == null)
		{
			callback = (window, codePoint) ->
			{
			};
		}
		this.characterCallback = callback;
		return previousCallback;
	}

	public CharacterModsCallback getCharacterModsCallback()
	{
		return characterModsCallback;
	}

	public CharacterModsCallback setCharacterModsCallback(CharacterModsCallback callback)
	{
		CharacterModsCallback previousCallback = characterModsCallback;
		if (callback == null)
		{
			callback = (window, codePoint, mods) ->
			{
			};
		}
		this.characterModsCallback = callback;
		return previousCallback;
	}

	public CursorEnterCallback getCursorEnterCallback()
	{
		return cursorEnterCallback;
	}

	public CursorEnterCallback setCursorEnterCallback(CursorEnterCallback callback)
	{
		CursorEnterCallback previousCallback = cursorEnterCallback;
		if (callback == null)
		{
			callback = (window, entered) ->
			{
			};
		}
		this.cursorEnterCallback = callback;
		return previousCallback;
	}

	public CursorPositionCallback getCursorPositionCallback()
	{
		return cursorPositionCallback;
	}

	public CursorPositionCallback setCursorPositionCallback(CursorPositionCallback callback)
	{
		CursorPositionCallback previousCallback = cursorPositionCallback;
		if (callback == null)
		{
			callback = (window, x, y) ->
			{
			};
		}
		this.cursorPositionCallback = callback;
		return previousCallback;
	}

	public DropCallback getDropCallback()
	{
		return dropCallback;
	}

	public DropCallback setDropCallback(DropCallback callback)
	{
		DropCallback previousCallback = dropCallback;
		if (callback == null)
		{
			callback = (window, paths) ->
			{
			};
		}
		this.dropCallback = callback;
		return previousCallback;
	}

	public FramebufferSizeCallback getFramebufferSizeCallback()
	{
		return framebufferSizeCallback;
	}

	public FramebufferSizeCallback setFramebufferSizeCallback(FramebufferSizeCallback callback)
	{
		FramebufferSizeCallback previousCallback = framebufferSizeCallback;
		if (callback == null)
		{
			callback = (window, width, height) ->
			{
			};
		}
		this.framebufferSizeCallback = callback;
		return previousCallback;
	}

	public KeyCallback getKeyCallback()
	{
		return keyCallback;
	}

	public KeyCallback setKeyCallback(KeyCallback callback)
	{
		KeyCallback previousCallback = keyCallback;
		if (callback == null)
		{
			callback = (window, key, scanCode, action, mods) ->
			{
			};
		}
		this.keyCallback = callback;
		return previousCallback;
	}

	public MouseButtonCallback getMouseButtonCallback()
	{
		return mouseButtonCallback;
	}

	public MouseButtonCallback setMouseButtonCallback(MouseButtonCallback callback)
	{
		MouseButtonCallback previousCallback = mouseButtonCallback;
		if (callback == null)
		{
			callback = (window, button, action, mods) ->
			{
			};
		}
		this.mouseButtonCallback = callback;
		return previousCallback;
	}

	public ScrollCallback getScrollCallback()
	{
		return scrollCallback;
	}

	public ScrollCallback setScrollCallback(ScrollCallback callback)
	{
		ScrollCallback previousCallback = scrollCallback;
		if (callback == null)
		{
			callback = (window, x, y) ->
			{
			};
		}
		this.scrollCallback = callback;
		return previousCallback;
	}

	public WindowCloseCallback getCloseCallback()
	{
		return windowCloseCallback;
	}

	public WindowCloseCallback setCloseCallback(WindowCloseCallback callback)
	{
		WindowCloseCallback previousCallback = windowCloseCallback;
		if (callback == null)
		{
			callback = (window) ->
			{
			};
		}
		this.windowCloseCallback = callback;
		return previousCallback;
	}

	public WindowFocusCallback getFocusCallback()
	{
		return windowFocusCallback;
	}

	public WindowFocusCallback setFocusCallback(WindowFocusCallback callback)
	{
		WindowFocusCallback previousCallback = windowFocusCallback;
		if (callback == null)
		{
			callback = (window, focused) ->
			{
			};
		}
		this.windowFocusCallback = callback;
		return previousCallback;
	}

	public WindowIconifyCallback getIconifyCallback()
	{
		return windowIconifyCallback;
	}

	public WindowIconifyCallback setIconifyCallback(WindowIconifyCallback callback)
	{
		WindowIconifyCallback previousCallback = windowIconifyCallback;
		if (callback == null)
		{
			callback = (window, iconified) ->
			{
			};
		}
		this.windowIconifyCallback = callback;
		return previousCallback;
	}

	public WindowPositionCallback getPositionCallback()
	{
		return windowPositionCallback;
	}

	public WindowPositionCallback setPositionCallback(WindowPositionCallback callback)
	{
		WindowPositionCallback previousCallback = windowPositionCallback;
		if (callback == null)
		{
			callback = (window, x, y) ->
			{
			};
		}
		this.windowPositionCallback = callback;
		return previousCallback;
	}

	public WindowRefreshCallback getRefreshCallback()
	{
		return windowRefreshCallback;
	}

	public WindowRefreshCallback setRefreshCallback(WindowRefreshCallback callback)
	{
		WindowRefreshCallback previousCallback = windowRefreshCallback;
		if (callback == null)
		{
			callback = (window) ->
			{
			};
		}
		this.windowRefreshCallback = callback;
		return previousCallback;
	}

	public WindowSizeCallback getSizeCallback()
	{
		return windowSizeCallback;
	}

	public WindowSizeCallback setSizeCallback(WindowSizeCallback callback)
	{
		WindowSizeCallback previousCallback = windowSizeCallback;
		if (callback == null)
		{
			callback = (window, width, height) ->
			{
			};
		}
		this.windowSizeCallback = callback;
		return previousCallback;
	}

	public Monitor getCurrentMonitor()
	{
		int bestoverlap = 0;
		Monitor currentMonitor = null;

		int wx = (int) getPosition().x;
		int wy = (int) getPosition().y;
		int ww = (int) getSize().x;
		int wh = (int) getSize().y;

		for (Monitor monitor : Monitor.getMonitors())
		{
			Vector2 vPos = monitor.getVirtualPosition();
			VideoMode vMode = monitor.getVideoMode();
			int mx = (int) vPos.x;
			int my = (int) vPos.y;
			int mw = vMode.getWidth();
			int mh = vMode.getHeight();
			int overlap = MathUtils.max(0, MathUtils.min(wx + ww, wx + mw) - MathUtils.max(wx, mx)) * MathUtils.max(0, MathUtils.min(wy + wh, my + mh) - MathUtils.max(wy, my));
			if (bestoverlap < overlap)
			{
				bestoverlap = overlap;
				currentMonitor = monitor;
			}
		}
		return currentMonitor;
	}

	public GLCapabilities getCapabilities()
	{
		return windowCapabilities;
	}

	public static final class Constants
	{
		public static final int TRUE = 1,
				FALSE = 0;
		public static final int FOCUSED = 0x20001,
				ICONIFIED = 0x20002,
				RESIZABLE = 0x20003,
				VISIBLE = 0x20004,
				DECORATED = 0x20005,
				AUTO_ICONIFY = 0x20006,
				FLOATING = 0x20007,
				MAXIMIZED = 0x20008;
	}
}
