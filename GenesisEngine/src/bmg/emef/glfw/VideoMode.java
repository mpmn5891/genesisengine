package bmg.emef.glfw;

public class VideoMode
{
	private int width;
	private int height;
	private int redBits;
	private int greenBits;
	private int blueBits;
	private int refreshRate;

	VideoMode(int width, int height, int redBits, int greenBits, int blueBits, int refreshRate)
	{
		this.width = width;
		this.height = height;
		this.redBits = redBits;
		this.greenBits = greenBits;
		this.blueBits = blueBits;
		this.refreshRate = refreshRate;
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public int getRedBits()
	{
		return redBits;
	}

	public int getGreenBits()
	{
		return greenBits;
	}

	public int getBlueBits()
	{
		return blueBits;
	}

	public int getRefreshRate()
	{
		return refreshRate;
	}

	@Override
	public int hashCode()
	{
		int result = width;
		result = 31 * result + height;
		result = 31 * result + redBits;
		result = 31 * result + greenBits;
		result = 31 * result + blueBits;
		result = 31 * result + refreshRate;
		return result;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		VideoMode videoMode = (VideoMode) o;

		return blueBits == videoMode.blueBits &&
				greenBits == videoMode.greenBits &&
				height == videoMode.height &&
				redBits == videoMode.redBits &&
				refreshRate == videoMode.refreshRate &&
				width == videoMode.width;
	}

	@Override
	public String toString()
	{
		return "VideoMode{" +
				"width=" + width +
				", height=" + height +
				", redBits=" + redBits +
				", greenBits=" + greenBits +
				", blueBits=" + blueBits +
				", refreshRate=" + refreshRate +
				'}';
	}
}
