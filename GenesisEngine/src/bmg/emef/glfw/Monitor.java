package bmg.emef.glfw;

import static org.lwjgl.glfw.GLFW.glfwGetGammaRamp;
import static org.lwjgl.glfw.GLFW.glfwGetMonitorName;
import static org.lwjgl.glfw.GLFW.glfwGetMonitors;
import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;
import static org.lwjgl.glfw.GLFW.glfwGetVideoModes;
import static org.lwjgl.glfw.GLFW.glfwSetGamma;
import static org.lwjgl.glfw.GLFW.glfwSetGammaRamp;
import static org.lwjgl.glfw.GLFW.glfwSetMonitorCallback;
import static org.lwjgl.glfw.GLFW.nglfwGetMonitorPhysicalSize;
import static org.lwjgl.glfw.GLFW.nglfwGetMonitorPos;

import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.lwjgl.BufferUtils;
import org.lwjgl.PointerBuffer;
import org.lwjgl.glfw.GLFWGammaRamp;
import org.lwjgl.glfw.GLFWMonitorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.system.MemoryUtil;

import bmg.emef.glfw.functional.MonitorCallback;
import bmg.emef.math.Vector2;

public class Monitor
{
	private static Map<Long, Monitor> registeredMonitors = new HashMap<>();
	private static Monitor primary;
	private static List<Monitor> monitors;
	private static GLFWMonitorCallback glfwMonitorCallback;
	private static MonitorCallback monitorCallback;

	public static Monitor getPrimaryMonitor()
	{
		if (primary == null)
		{
			primary = new Monitor(glfwGetPrimaryMonitor());
		}
		return primary;
	}

	public static MonitorCallback setCallback(MonitorCallback callback)
	{
		MonitorCallback previousCallback = monitorCallback;
		if (callback == null)
		{
			callback = (monitor, event) ->
			{
			};
		}
		monitorCallback = callback;
		if (glfwMonitorCallback != null) glfwMonitorCallback.free();
		glfwMonitorCallback = GLFWMonitorCallback.create((monitor, event) -> monitorCallback.invoke(registeredMonitors.get(monitor), event));
		glfwSetMonitorCallback(glfwMonitorCallback);
		return previousCallback;
	}

	public static List<Monitor> getMonitors()
	{
		if (monitors == null)
		{
			monitors = new ArrayList<>();
			PointerBuffer buffer = glfwGetMonitors();
			while (buffer.hasRemaining())
			{
				monitors.add(new Monitor(buffer.get()));
			}
			monitors = Collections.unmodifiableList(monitors);
		}
		return monitors;
	}

	private long handle;
	private List<VideoMode> videoModes;

	private Monitor(long handle)
	{
		this.handle = handle;
		registeredMonitors.put(handle, this);
	}

	public List<VideoMode> getVideoModes()
	{
		if (videoModes == null)
		{
			videoModes = new ArrayList<>();
			GLFWVidMode.Buffer modes = glfwGetVideoModes(handle);
			for (int i = 0; i < modes.capacity(); i++)
			{
				modes.position(i);
				int width = modes.width();
				int height = modes.height();
				int redBits = modes.redBits();
				int greenBits = modes.greenBits();
				int blueBits = modes.blueBits();
				int refreshRate = modes.refreshRate();
				videoModes.add(new VideoMode(width, height, redBits, greenBits, blueBits, refreshRate));
			}
			videoModes = Collections.unmodifiableList(videoModes);
		}
		return videoModes;
	}

	public VideoMode getVideoMode()
	{
		GLFWVidMode mode = glfwGetVideoMode(handle);
		int width = mode.width();
		int height = mode.height();
		int redBits = mode.redBits();
		int greenBits = mode.greenBits();
		int blueBits = mode.blueBits();
		int refreshRate = mode.refreshRate();
		return new VideoMode(width, height, redBits, greenBits, blueBits, refreshRate);
	}

	public long getHandle()
	{
		return handle;
	}

	public void setGamma(float gamma)
	{
		glfwSetGamma(handle, gamma);
	}

	public GammaRamp getGammaRamp()
	{
		GLFWGammaRamp gammaRamp = glfwGetGammaRamp(handle);
		if (gammaRamp.address() == 0) return null;
		ShortBuffer rBuffer = gammaRamp.red();
		ShortBuffer gBuffer = gammaRamp.green();
		ShortBuffer bBuffer = gammaRamp.blue();
		short[] red = new short[gammaRamp.size()];
		short[] green = new short[gammaRamp.size()];
		short[] blue = new short[gammaRamp.size()];
		int i = 0;
		while (rBuffer.hasRemaining())
		{
			red[i++] = rBuffer.get();
		}
		i = 0;
		while (gBuffer.hasRemaining())
		{
			green[i++] = gBuffer.get();
		}
		i = 0;
		while (bBuffer.hasRemaining())
		{
			blue[i++] = bBuffer.get();
		}
		return new GammaRamp(red, green, blue);
	}

	public void setGammaRamp(GammaRamp gammaRamp)
	{
		GLFWGammaRamp ramp = GLFWGammaRamp.malloc();
		ShortBuffer rBuffer = BufferUtils.createShortBuffer(gammaRamp.getSize());
		ShortBuffer gBuffer = BufferUtils.createShortBuffer(gammaRamp.getSize());
		ShortBuffer bBuffer = BufferUtils.createShortBuffer(gammaRamp.getSize());
		rBuffer.put(gammaRamp.getRed()).flip();
		gBuffer.put(gammaRamp.getGreen()).flip();
		bBuffer.put(gammaRamp.getBlue()).flip();
		ramp.red(rBuffer);
		ramp.green(gBuffer);
		ramp.blue(bBuffer);
		ramp.size(gammaRamp.getSize());
		glfwSetGammaRamp(handle, ramp);
		ramp.free();
	}

	public Vector2 getPhysicalSize()
	{
		IntBuffer sizeBuffer = BufferUtils.createIntBuffer(2);
		long addressWidth = MemoryUtil.memAddress(sizeBuffer);
		long addressHeight = addressWidth + Integer.BYTES;
		nglfwGetMonitorPhysicalSize(handle, addressWidth, addressHeight);
		return new Vector2(sizeBuffer.get(0), sizeBuffer.get(1));
	}

	public Vector2 getVirtualPosition()
	{
		IntBuffer posBuffer = BufferUtils.createIntBuffer(2);
		long posX = MemoryUtil.memAddress(posBuffer);
		long posY = posX + Integer.BYTES;
		nglfwGetMonitorPos(handle, posX, posY);
		return new Vector2(posBuffer.get(0), posBuffer.get(1));
	}

	public String getName()
	{
		return glfwGetMonitorName(handle);
	}

	public boolean isPrimary()
	{
		return getMonitors().get(0).equals(this);
	}

	@Override
	public int hashCode()
	{
		return (int) (handle ^ (handle >>> 32));
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Monitor monitor = (Monitor) o;

		return handle == monitor.handle;
	}

	@Override
	public String toString()
	{
		return "Monitor{" +
				"handle=" + handle + ", " +
				"name=\"" + getName() + "\", " +
				"primary=" + isPrimary() +
				'}';
	}
}
