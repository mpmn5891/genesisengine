package bmg.emef.glfw;

import java.nio.IntBuffer;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWJoystickCallback;
import org.lwjgl.system.MemoryUtil;

import bmg.emef.glfw.functional.ErrorCallback;
import bmg.emef.glfw.functional.JoystickCallback;
import bmg.emef.math.Vector3;
import bmg.emef.util.BufferUtils;

public class GLFW3
{
	private static GLFWErrorCallback glfwErrorCallback;
	private static ErrorCallback errorCallback;
	private static GLFWJoystickCallback glfwJoystickCallback;
	private static JoystickCallback joystickCallback;
	private static boolean initialized = false;

	private GLFW3()
	{
		// nothing to see here, prevents instantiation
	}

	public static boolean init()
	{
		if (isInitialized())
		{
			return true;
		}
		else
		{
			boolean state = GLFW.glfwInit();
			if (!state)
			{
				return initialized = false;
			}
			else
			{
				setErrorCallback(null);
				glfwErrorCallback = GLFWErrorCallback.create((error, description) -> errorCallback.invoke(error, MemoryUtil.memUTF8(description)));
				setJoystickCallback(null);
				glfwJoystickCallback = GLFWJoystickCallback.create((joy, event) -> joystickCallback.invoke(joy, event == GLFW.GLFW_CONNECTED));
				GLFW.glfwSetErrorCallback(glfwErrorCallback);
				GLFW.glfwSetJoystickCallback(glfwJoystickCallback);
				return initialized = true;
			}
		}
	}

	public static void setErrorCallback(ErrorCallback errorCallback)
	{
		if (errorCallback == null)
		{
			errorCallback = (error, description) ->
			{
			};
		}
		GLFW3.errorCallback = errorCallback;
	}

	public static void setJoystickCallback(JoystickCallback joystickCallback)
	{
		if (joystickCallback == null)
		{
			joystickCallback = (joystick, connected) ->
			{
			};
		}
		GLFW3.joystickCallback = joystickCallback;
	}

	public static void pollEvents()
	{
		GLFW.glfwPollEvents();
	}

	public static void waitEvents()
	{
		GLFW.glfwWaitEvents();
	}

	public static void postEmptyEvent()
	{
		GLFW.glfwPostEmptyEvent();
	}

	public static void setSwapInterval(int interval)
	{
		GLFW.glfwSwapInterval(interval);
	}

	public static Vector3 getVersion()
	{
		IntBuffer version = BufferUtils.createIntBuffer(3);
		GLFW.nglfwGetVersion(MemoryUtil.memAddress(version), MemoryUtil.memAddress(version) + Integer.BYTES, MemoryUtil.memAddress(version) + 2 * Integer.BYTES);
		return new Vector3(version.get(1), version.get(2), version.get(3));
	}

	public static String getVersionString()
	{
		return GLFW.glfwGetVersionString();
	}

	public static boolean isInitialized()
	{
		return initialized;
	}

	public static void terminate()
	{
		if (!isInitialized())
		{
			return;
		}
		if (glfwErrorCallback != null)
		{
			glfwErrorCallback.free();
		}
		if (glfwJoystickCallback != null)
		{
			glfwJoystickCallback.free();
		}
		GLFW.glfwTerminate();
		initialized = false;
	}
}
