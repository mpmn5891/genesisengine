package bmg.emef.glfw;

import java.util.Arrays;

import bmg.emef.util.MathUtils;

public class GammaRamp
{
	private short[] red;
	private short[] green;
	private short[] blue;
	private int size;

	public GammaRamp(short[] red, short[] green, short[] blue)
	{
		setRed(red);
		setGreen(green);
		setBlue(blue);
	}

	public void setRed(short... red)
	{
		size = MathUtils.max(size, red.length);
		this.red = Arrays.copyOf(red, size);
	}

	public void setGreen(short... green)
	{
		size = MathUtils.max(size, green.length);
		this.green = Arrays.copyOf(green, size);
	}

	public void setBlue(short... blue)
	{
		size = MathUtils.max(size, blue.length);
		this.blue = Arrays.copyOf(blue, size);
	}

	public short[] getRed()
	{
		return red;
	}

	public short[] getGreen()
	{
		return green;
	}

	public short[] getBlue()
	{
		return blue;
	}

	public int getSize()
	{
		return size;
	}

	@Override
	public int hashCode()
	{
		int result = Arrays.hashCode(red);
		result = 31 * result + Arrays.hashCode(green);
		result = 31 * result + Arrays.hashCode(blue);
		result = 31 * result + size;
		return result;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		GammaRamp gammaRamp = (GammaRamp) o;

		return size == gammaRamp.size &&
				Arrays.equals(red, gammaRamp.red) &&
				Arrays.equals(green, gammaRamp.green) &&
				Arrays.equals(blue, gammaRamp.blue);
	}

	@Override
	public String toString()
	{
		return "GammaRamp{" +
				"red=" + Arrays.toString(red) +
				",\ngreen=" + Arrays.toString(green) +
				",\nblue=" + Arrays.toString(blue) +
				",\nsize=" + size +
				'}';
	}
}
