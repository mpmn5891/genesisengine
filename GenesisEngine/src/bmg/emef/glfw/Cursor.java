package bmg.emef.glfw;

import static org.lwjgl.glfw.GLFW.GLFW_ARROW_CURSOR;
import static org.lwjgl.glfw.GLFW.GLFW_CROSSHAIR_CURSOR;
import static org.lwjgl.glfw.GLFW.GLFW_HAND_CURSOR;
import static org.lwjgl.glfw.GLFW.GLFW_HRESIZE_CURSOR;
import static org.lwjgl.glfw.GLFW.GLFW_IBEAM_CURSOR;
import static org.lwjgl.glfw.GLFW.GLFW_VRESIZE_CURSOR;

import java.nio.ByteBuffer;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWImage;
import org.lwjgl.system.MemoryUtil;

import bmg.emef.environment.Exceptions;
import bmg.emef.graphics.Color;
import bmg.emef.graphics.Image;
import bmg.emef.util.BufferUtils;

public class Cursor
{
	private long handle;

	public Cursor(Image image)
	{
		this(image, 0, 0);
	}

	public Cursor(Type type)
	{
		handle = GLFW.glfwCreateStandardCursor(type.getType());
		if (handle == MemoryUtil.NULL)
		{
			throw new Exceptions.Exception("Incountered Error while creating GLFW cursor handle: Unable to laod cursor from standard type " + type.getType());
		}
	}

	public Cursor(Image image, int xHot, int yHot)
	{
		int width = image.getWidth();
		int height = image.getHeight();
		GLFWImage glfwImage = GLFWImage.calloc();
		glfwImage.width(width).height(height);
		ByteBuffer data = BufferUtils.createByteBuffer(width * height * 4);
		Color color = Color.REUSABLE_STACK.pop();
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				image.getPixel(x, y, color);
				float r = (color.r * 255f);
				float g = (color.g * 255f);
				float b = (color.b * 255f);
				float a = ((1 - color.a) * 255f);
				data.put((byte) r).put((byte) g).put((byte) b).put((byte) a);
			}
		}
		Color.REUSABLE_STACK.push(color);
		data.flip();
		glfwImage.pixels(data);
		handle = GLFW.glfwCreateCursor(glfwImage, xHot, yHot);
		if (handle == MemoryUtil.NULL)
		{
			throw new Exceptions.Exception("Incountered Error while creating GLFW Cursor handle: Unable to load cursor from texture");
		}
		glfwImage.free();
	}

	public long getHandle()
	{
		return handle;
	}

	public void destroy()
	{
		GLFW.glfwDestroyCursor(handle);
	}

	public enum Type
	{
		ARROW(GLFW_ARROW_CURSOR),
		IBEAM(GLFW_IBEAM_CURSOR),
		CROSSHAIR(GLFW_CROSSHAIR_CURSOR),
		HAND(GLFW_HAND_CURSOR),
		HRESIZE(GLFW_HRESIZE_CURSOR),
		VRESIZE(GLFW_VRESIZE_CURSOR);

		private int type;

		Type(int type)
		{
			this.type = type;
		}

		public int getType()
		{
			return type;
		}
	}
}
