package bmg.emef.Input;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.glfw.GLFW;

import bmg.emef.Input.Keyboard.QWERTY;
import bmg.emef.Input.Mouse.ButtonCode;
import bmg.emef.environment.Environment;
import bmg.emef.events.callbacks.BiCallback;
import bmg.emef.events.callbacks.UniCallback;
import bmg.emef.glfw.GLFW3;
import bmg.emef.glfw.Window;

public class Input
{
	private List<BiCallback<QWERTY, InputState>> keyboardListeners = new ArrayList<>();
	private List<BiCallback<ButtonCode, InputState>> mouseListeners = new ArrayList<>();
	private List<UniCallback<Controller>> controllerConnectionListeners = new ArrayList<>();

	public Input()
	{
		for (Keyboard.QWERTY key : Keyboard.QWERTY.values())
		{
			Keyboard.keyStates.put(key, InputState.NO_INPUT);
		}
		for (Mouse.ButtonCode button : Mouse.ButtonCode.values())
		{
			Mouse.ButtonStates.put(button, InputState.NO_INPUT);
		}
		Window window = Environment.display.window;
		window.setKeyCallback((windowID, key, scanCode, action, mods) ->
		{
			if (action != GLFW.GLFW_REPEAT)
			{
				Keyboard.eventStates.put(key, action != GLFW.GLFW_RELEASE);
				for (BiCallback<QWERTY, InputState> listener : keyboardListeners)
				{
					listener.invoke(QWERTY.getKeyCode(key), Keyboard.getKeyState(QWERTY.getKeyCode(key)));
				}
			}
		});
		window.setMouseButtonCallback((windowID, button, action, mods) ->
		{
			if (action != GLFW.GLFW_REPEAT)
			{
				Mouse.eventStates.put(button, action != GLFW.GLFW_RELEASE);
				for (BiCallback<ButtonCode, InputState> listener : mouseListeners)
				{
					listener.invoke(ButtonCode.getCode(button), Mouse.getButtonState(ButtonCode.getCode(button)));
				}
			}
		});
		window.setCursorPositionCallback((windowID, xpos, ypos) ->
		{
			Mouse.delta_x = (int) xpos - Mouse.x;
			Mouse.delta_y = (int) ypos - Mouse.y;
			Mouse.x = (int) xpos;
			Mouse.y = (int) ypos;
		});
		window.setScrollCallback((windowID, xoff, yoff) ->
		{
			Mouse.scroll_x = xoff > 0 ? 1 : xoff == 0 ? 0 : -1;
			Mouse.scroll_y = yoff > 0 ? 1 : yoff == 0 ? 0 : -1;
		});
		GLFW3.setJoystickCallback((joystickID, connected) ->
		{
			Controller c = Controllers.connectedControllers.get(joystickID);
			if (c == null)
			{
				c = new Controller(joystickID, GLFW.glfwGetJoystickName(joystickID), GLFW.glfwGetJoystickButtons(joystickID).capacity(),
						GLFW.glfwGetJoystickAxes(joystickID).capacity());
				Controllers.connectedControllers.put(joystickID, c);
			}
			c.connected = connected;
			for (UniCallback<Controller> listener : controllerConnectionListeners)
			{
				listener.invoke(c);
			}
		});
		Environment.eventManager.registerUpdateListener((delta) ->
		{
			GLFW3.pollEvents();
			Controllers.update();
			Keyboard.update();
			Mouse.update();
		});
	}

	public void registerKeyboardListener(BiCallback<QWERTY, InputState> listener)
	{
		keyboardListeners.add(listener);
	}

	public void unRegisterKeyboardListener(BiCallback<QWERTY, InputState> listener)
	{
		keyboardListeners.remove(listener);
	}

	public void registerMouseListener(BiCallback<ButtonCode, InputState> listener)
	{
		mouseListeners.add(listener);
	}

	public void unRegisterMouseListener(BiCallback<ButtonCode, InputState> listener)
	{
		mouseListeners.remove(listener);
	}

	public void registerControllerConnectionListener(UniCallback<Controller> listener)
	{
		controllerConnectionListeners.add(listener);
	}

	public void unRegisterControllerConnectionListener(UniCallback<Controller> listener)
	{
		controllerConnectionListeners.remove(listener);
	}

	public enum InputState
	{
		NO_INPUT,
		PRESSED,
		HELD,
		RELEASED;
	}
}
