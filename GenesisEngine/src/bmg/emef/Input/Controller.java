package bmg.emef.Input;

import java.util.HashMap;
import java.util.Map;

import bmg.emef.Input.Input.InputState;

public class Controller
{
	public static final int BUTTON_0 = 0;
	public static final int BUTTON_1 = 1;
	public static final int BUTTON_2 = 2;
	public static final int BUTTON_3 = 3;
	public static final int BUTTON_4 = 4;
	public static final int BUTTON_5 = 5;
	public static final int BUTTON_6 = 6;
	public static final int BUTTON_7 = 7;
	public static final int BUTTON_8 = 8;
	public static final int BUTTON_9 = 9;
	public static final int BUTTON_10 = 10;
	public static final int BUTTON_11 = 11;
	public static final int BUTTON_12 = 12;
	public static final int BUTTON_13 = 13;
	public static final int BUTTON_14 = 14;
	public static final int BUTTON_15 = 15;
	public static final int BUTTON_16 = 16;
	public static final int BUTTON_17 = 17;
	public static final int BUTTON_18 = 18;
	public static final int BUTTON_19 = 19;
	public static final int BUTTON_20 = 20;
	public static final int BUTTON_21 = 21;
	public static final int BUTTON_22 = 22;
	public static final int BUTTON_23 = 23;
	public static final int BUTTON_24 = 24;
	public static final int BUTTON_25 = 25;
	public static final int BUTTON_26 = 26;
	public static final int BUTTON_27 = 27;
	public static final int BUTTON_28 = 28;
	public static final int BUTTON_29 = 29;
	public static final int BUTTON_30 = 30;

	public static final int BUTTON_A = BUTTON_0;
	public static final int BUTTON_B = BUTTON_1;
	public static final int BUTTON_X = BUTTON_2;
	public static final int BUTTON_Y = BUTTON_3;
	public static final int BUTTON_L1 = BUTTON_4;
	public static final int BUTTON_L2 = BUTTON_5;
	public static final int BUTTON_R1 = BUTTON_6;
	public static final int BUTTON_R2 = BUTTON_7;
	public static final int BUTTON_SELECT = BUTTON_8;
	public static final int BUTTON_START = BUTTON_9;
	public static final int BUTTON_DPAD_UP = BUTTON_10;
	public static final int BUTTON_DPAD_RIGHT = BUTTON_11;
	public static final int BUTTON_DPAD_DOWN = BUTTON_12;
	public static final int BUTTON_DPAD_LEFT = BUTTON_13;
	public static final int BUTTON_LEFT_STICK = BUTTON_14;
	public static final int BUTTON_RIGHT_STICK = BUTTON_15;

	public static final int AXIS_0 = 0;
	public static final int AXIS_1 = 1;
	public static final int AXIS_2 = 2;
	public static final int AXIS_3 = 3;
	public static final int AXIS_4 = 4;
	public static final int AXIS_5 = 5;
	public static final int AXIS_6 = 6;
	public static final int AXIS_7 = 7;
	public static final int AXIS_8 = 8;
	public static final int AXIS_9 = 9;
	public static final int AXIS_10 = 10;

	public static final int AXIS_LEFT_X = AXIS_0;
	public static final int AXIS_LEFT_Y = AXIS_1;
	public static final int AXIS_RIGHT_X = AXIS_2;
	public static final int AXIS_RIGHT_Y = AXIS_3;

	Map<Integer, InputState> buttonStates = new HashMap<>();
	Map<Integer, Axe> axeStates = new HashMap<>();

	int id;
	String name;
	boolean connected;

	private int numButtons;
	private int numAxes;

	Controller(int id, String name, int buttons, int axes)
	{
		this.id = id;
		this.name = name;
		this.numButtons = buttons;
		this.numAxes = axes;
	}

	void update()
	{
		for (java.util.Map.Entry<Integer, InputState> entry : buttonStates.entrySet())
		{
			switch (entry.getValue())
			{
				case PRESSED:
					entry.setValue(InputState.HELD);
					break;
				case RELEASED:
					entry.setValue(InputState.NO_INPUT);
					break;
				default:
					break;
			}
		}

		for (java.util.Map.Entry<Integer, Axe> entry : axeStates.entrySet())
		{
			switch (entry.getValue().state)
			{
				case HELD:
					entry.getValue().state = entry.getValue().amount == 0 ? InputState.RELEASED : entry.getValue().state;
					break;
				case NO_INPUT:
					entry.getValue().state = entry.getValue().amount != 0 ? InputState.PRESSED : entry.getValue().state;
					break;
				case PRESSED:
					entry.getValue().state = InputState.HELD;
					break;
				case RELEASED:
					entry.getValue().state = InputState.NO_INPUT;
					break;
				default:
					break;
			}
		}
	}

	public InputState getButtonState(int button)
	{
		return buttonStates.containsKey(button) ? buttonStates.get(button) : InputState.NO_INPUT;
	}

	public boolean isButtonDown(int button)
	{
		return buttonStates.containsKey(button) ? buttonStates.get(button).equals(InputState.PRESSED) || buttonStates.get(button).equals(InputState.HELD) : false;
	}

	public boolean isButtonUp(int button)
	{
		return buttonStates.containsKey(button) ? buttonStates.get(button).equals(InputState.RELEASED) || buttonStates.get(button).equals(InputState.NO_INPUT) : false;
	}

	public InputState getAxeState(int axe)
	{
		return axeStates.containsKey(axe) ? axeStates.get(axe).state : InputState.NO_INPUT;
	}

	public double getAxeAmount(int axe)
	{
		return axeStates.containsKey(axe) ? axeStates.get(axe).amount : 0.0d;
	}

	public boolean isAxeDown(int axe)
	{
		return axeStates.containsKey(axe) ? axeStates.get(axe).state.equals(InputState.PRESSED) || axeStates.get(axe).state.equals(InputState.HELD) : false;
	}

	public boolean isAxeUp(int axe)
	{
		return axeStates.containsKey(axe) ? axeStates.get(axe).state.equals(InputState.RELEASED) || axeStates.get(axe).state.equals(InputState.NO_INPUT) : false;
	}

	public int getID()
	{
		return id;
	}

	public String getName()
	{
		return name;
	}

	public boolean isConnected()
	{
		return connected;
	}

	public int getNumberOfButtons()
	{
		return numButtons;
	}

	public int getNumberOfAxes()
	{
		return numAxes;
	}

	public static class Axe
	{
		InputState state;
		double amount;
	}
}
