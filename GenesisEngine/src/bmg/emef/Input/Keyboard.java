package bmg.emef.Input;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.lwjgl.glfw.GLFW;

import bmg.emef.Input.Input.InputState;

public class Keyboard
{
	static Map<Integer, Boolean> eventStates = new HashMap<>();
	static Map<QWERTY, InputState> keyStates = new HashMap<>();

	static void update()
	{
		for (java.util.Map.Entry<QWERTY, InputState> entry : keyStates.entrySet())
		{
			switch (entry.getValue())
			{
				case NO_INPUT:
					entry.setValue(eventStates.containsKey(entry.getKey().code) ? eventStates.get(entry.getKey().code) == true ? InputState.PRESSED : InputState.NO_INPUT
							: InputState.NO_INPUT);
					break;
				case PRESSED:
					entry.setValue(InputState.HELD);
					break;
				case HELD:
					entry.setValue(
							eventStates.containsKey(entry.getKey().code) ? eventStates.get(entry.getKey().code) == false ? InputState.RELEASED : InputState.HELD : InputState.HELD);
					break;
				case RELEASED:
					entry.setValue(InputState.NO_INPUT);
					break;
				default:
					break;
			}
		}
	}

	public static InputState getKeyState(QWERTY key)
	{
		return keyStates.containsKey(key) ? keyStates.get(key) : InputState.NO_INPUT;
	}

	public static boolean isKeyDown(QWERTY key)
	{
		return keyStates.containsKey(key) ? (keyStates.get(key).equals(InputState.PRESSED) || keyStates.get(key).equals(InputState.HELD)) : false;
	}

	public static boolean isKeyUp(QWERTY key)
	{
		return keyStates.containsKey(key) ? (keyStates.get(key).equals(InputState.NO_INPUT) || keyStates.get(key).equals(InputState.RELEASED)) : false;
	}

	public static List<QWERTY> getTypedKeys()
	{
		return getTypedKeys(new ArrayList<>());
	}

	public static List<QWERTY> getTypedKeys(List<QWERTY> dest)
	{
		if (dest == null)
		{
			dest = new ArrayList<>();
		}
		for (java.util.Map.Entry<QWERTY, InputState> entry : keyStates.entrySet())
		{
			if (entry.getValue().equals(InputState.RELEASED))
			{
				dest.add(entry.getKey());
			}
		}
		return dest;
	}

	public static void consume(QWERTY key)
	{
		keyStates.put(key, InputState.NO_INPUT);
		eventStates.put(key.code, false);
	}

	public enum QWERTY
	{
		KEY_NONE(GLFW.GLFW_KEY_UNKNOWN, (char) 0, (char) 0),
		KEY_ESCAPE(GLFW.GLFW_KEY_ESCAPE, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		// function keys
		KEY_F1(GLFW.GLFW_KEY_F1, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_F2(GLFW.GLFW_KEY_F2, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_F3(GLFW.GLFW_KEY_F3, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_F4(GLFW.GLFW_KEY_F4, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_F5(GLFW.GLFW_KEY_F5, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_F6(GLFW.GLFW_KEY_F6, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_F7(GLFW.GLFW_KEY_F7, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_F8(GLFW.GLFW_KEY_F8, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_F9(GLFW.GLFW_KEY_F9, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_F10(GLFW.GLFW_KEY_F10, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_F11(GLFW.GLFW_KEY_F11, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_F12(GLFW.GLFW_KEY_F12, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		// numbers (top)
		KEY_1(GLFW.GLFW_KEY_1, '1', '!'),
		KEY_2(GLFW.GLFW_KEY_2, '2', '@'),
		KEY_3(GLFW.GLFW_KEY_3, '3', '#'),
		KEY_4(GLFW.GLFW_KEY_4, '4', '$'),
		KEY_5(GLFW.GLFW_KEY_5, '5', '%'),
		KEY_6(GLFW.GLFW_KEY_6, '6', '^'),
		KEY_7(GLFW.GLFW_KEY_7, '7', '&'),
		KEY_8(GLFW.GLFW_KEY_8, '8', '*'),
		KEY_9(GLFW.GLFW_KEY_9, '9', '('),
		KEY_0(GLFW.GLFW_KEY_0, '0', ')'),
		// numbers (number pad)
		KEY_ONE(GLFW.GLFW_KEY_KP_1, '1', '1'),
		KEY_TWO(GLFW.GLFW_KEY_KP_1, '2', '2'),
		KEY_THREE(GLFW.GLFW_KEY_KP_1, '3', '3'),
		KEY_FOUR(GLFW.GLFW_KEY_KP_1, '4', '4'),
		KEY_FIVE(GLFW.GLFW_KEY_KP_1, '5', '5'),
		KEY_SIX(GLFW.GLFW_KEY_KP_1, '6', '6'),
		KEY_SEVEN(GLFW.GLFW_KEY_KP_1, '7', '7'),
		KEY_EIGHT(GLFW.GLFW_KEY_KP_1, '8', '8'),
		KEY_NINE(GLFW.GLFW_KEY_KP_1, '9', '9'),
		KEY_ZERO(GLFW.GLFW_KEY_KP_1, '0', '0'),
		// symbol keys
		KEY_BACKTICK(GLFW.GLFW_KEY_GRAVE_ACCENT, '`', '~'),
		KEY_DASH(GLFW.GLFW_KEY_MINUS, '-', '_'),
		KEY_EQUALS(GLFW.GLFW_KEY_EQUAL, '=', '+'),
		KEY_OPEN_BRACKET(GLFW.GLFW_KEY_LEFT_BRACKET, '[', '{'),
		KEY_CLOSE_BRACKET(GLFW.GLFW_KEY_RIGHT_BRACKET, ']', '}'),
		KEY_BACKSLASH(GLFW.GLFW_KEY_BACKSLASH, '\\', '|'),
		KEY_SEMICOLON(GLFW.GLFW_KEY_SEMICOLON, ';', ':'),
		KEY_APOSTROPHE(GLFW.GLFW_KEY_APOSTROPHE, '\'', '"'),
		KEY_COMMA(GLFW.GLFW_KEY_COMMA, ',', '<'),
		KEY_PERIOD(GLFW.GLFW_KEY_PERIOD, '.', '>'),
		KEY_FORWARDSLASH(GLFW.GLFW_KEY_SLASH, '/', '?'),
		// lock keys
		KEY_NUM_LOCK(GLFW.GLFW_KEY_NUM_LOCK, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_CAPS_LOCK(GLFW.GLFW_KEY_CAPS_LOCK, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_SCROLL_LOCK(GLFW.GLFW_KEY_SCROLL_LOCK, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		// special keys
		KEY_TAB(GLFW.GLFW_KEY_TAB, '\t', KEY_NONE.secondaryCharacter()),
		KEY_BACKSPACE(GLFW.GLFW_KEY_BACKSPACE, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_ENTER(GLFW.GLFW_KEY_ENTER, '\n', KEY_NONE.secondaryCharacter()),
		KEY_LEFT_SHIFT(GLFW.GLFW_KEY_LEFT_SHIFT, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_RIGHT_SHIFT(GLFW.GLFW_KEY_RIGHT_SHIFT, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_LEFT_CONTROL(GLFW.GLFW_KEY_LEFT_CONTROL, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_RIGHT_CONTROL(GLFW.GLFW_KEY_RIGHT_CONTROL, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_LEFT_ALT(GLFW.GLFW_KEY_LEFT_ALT, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_RIGHT_ALT(GLFW.GLFW_KEY_RIGHT_ALT, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_LEFT_SUPER(GLFW.GLFW_KEY_LEFT_SUPER, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_RIGHT_SUPER(GLFW.GLFW_KEY_RIGHT_SUPER, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_INSERT(GLFW.GLFW_KEY_INSERT, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_DELETE(GLFW.GLFW_KEY_DELETE, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_HOME(GLFW.GLFW_KEY_HOME, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_PAGEDOWN(GLFW.GLFW_KEY_PAGE_DOWN, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_PAGEUP(GLFW.GLFW_KEY_PAGE_UP, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_PRINTSCREEN(GLFW.GLFW_KEY_PRINT_SCREEN, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_PAUSE_BREAK(GLFW.GLFW_KEY_PAUSE, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		// number pad special keys
		KEY_DIVIDE(GLFW.GLFW_KEY_KP_DIVIDE, '/', '/'),
		KEY_MULTIPLY(GLFW.GLFW_KEY_KP_MULTIPLY, '*', '*'),
		KEY_MINUS(GLFW.GLFW_KEY_KP_SUBTRACT, '-', '-'),
		KEY_ADD(GLFW.GLFW_KEY_KP_ADD, '+', '+'),
		KEY_NUM_ENTER(GLFW.GLFW_KEY_KP_ENTER, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_DECIMAL(GLFW.GLFW_KEY_KP_DECIMAL, '.', '.'),
		// arrow keys
		KEY_LEFT(GLFW.GLFW_KEY_LEFT, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_UP(GLFW.GLFW_KEY_UP, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_DOWN(GLFW.GLFW_KEY_DOWN, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		KEY_RIGHT(GLFW.GLFW_KEY_RIGHT, KEY_NONE.primaryCharacter(), KEY_NONE.secondaryCharacter()),
		// alphabet
		KEY_A(GLFW.GLFW_KEY_A, 'a', 'A'),
		KEY_B(GLFW.GLFW_KEY_B, 'b', 'B'),
		KEY_C(GLFW.GLFW_KEY_C, 'c', 'C'),
		KEY_D(GLFW.GLFW_KEY_D, 'd', 'D'),
		KEY_E(GLFW.GLFW_KEY_E, 'e', 'E'),
		KEY_F(GLFW.GLFW_KEY_F, 'f', 'F'),
		KEY_G(GLFW.GLFW_KEY_G, 'g', 'G'),
		KEY_H(GLFW.GLFW_KEY_H, 'h', 'H'),
		KEY_I(GLFW.GLFW_KEY_I, 'i', 'I'),
		KEY_J(GLFW.GLFW_KEY_J, 'j', 'J'),
		KEY_K(GLFW.GLFW_KEY_K, 'k', 'K'),
		KEY_L(GLFW.GLFW_KEY_L, 'l', 'L'),
		KEY_M(GLFW.GLFW_KEY_M, 'm', 'M'),
		KEY_N(GLFW.GLFW_KEY_N, 'n', 'N'),
		KEY_O(GLFW.GLFW_KEY_O, 'o', 'O'),
		KEY_P(GLFW.GLFW_KEY_P, 'p', 'P'),
		KEY_Q(GLFW.GLFW_KEY_Q, 'q', 'Q'),
		KEY_R(GLFW.GLFW_KEY_R, 'r', 'R'),
		KEY_S(GLFW.GLFW_KEY_S, 's', 'S'),
		KEY_T(GLFW.GLFW_KEY_T, 't', 'T'),
		KEY_U(GLFW.GLFW_KEY_U, 'u', 'U'),
		KEY_V(GLFW.GLFW_KEY_V, 'v', 'V'),
		KEY_W(GLFW.GLFW_KEY_W, 'w', 'W'),
		KEY_X(GLFW.GLFW_KEY_X, 'x', 'X'),
		KEY_Y(GLFW.GLFW_KEY_Y, 'y', 'Y'),
		KEY_Z(GLFW.GLFW_KEY_Z, 'z', 'Z'),
		KEY_SPACE(GLFW.GLFW_KEY_SPACE, ' ', ' ');

		private final int code;
		private final char primary, secondary;

		private QWERTY(int code, char primary, char secondary)
		{
			this.code = code;
			this.primary = primary;
			this.secondary = secondary;
		}

		public static QWERTY getKeyCode(int code)
		{
			for (QWERTY k : QWERTY.values())
			{
				if (k.code() == code)
				{
					return k;
				}
			}
			return QWERTY.KEY_NONE;
		}

		public int code()
		{
			return code;
		}

		public char primaryCharacter()
		{
			return primary;
		}

		public char secondaryCharacter()
		{
			return secondary;
		}
	}
}
