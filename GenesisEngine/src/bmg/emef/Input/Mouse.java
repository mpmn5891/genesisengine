package bmg.emef.Input;

import java.util.HashMap;
import java.util.Map;

import org.lwjgl.glfw.GLFW;

import bmg.emef.Input.Input.InputState;
import bmg.emef.math.Vector2;

public class Mouse
{
	static Map<Integer, Boolean> eventStates = new HashMap<>();
	static Map<ButtonCode, InputState> ButtonStates = new HashMap<>();

	private static Vector2 mousePosition;
	private static Vector2 deltaVector;
	private static Vector2 scrollVector;
	static int x;
	static int y;
	static int delta_x;
	static int delta_y;
	static float scroll_x;
	static float scroll_y;

	static void update()
	{
		for (java.util.Map.Entry<ButtonCode, InputState> entry : ButtonStates.entrySet())
		{
			switch (entry.getValue())
			{
				case NO_INPUT:
					entry.setValue(eventStates.containsKey(entry.getKey().code) ? eventStates.get(entry.getKey().code) == true ? InputState.PRESSED : InputState.NO_INPUT
							: InputState.NO_INPUT);
					break;
				case PRESSED:
					entry.setValue(InputState.HELD);
					break;
				case HELD:
					entry.setValue(
							eventStates.containsKey(entry.getKey().code) ? eventStates.get(entry.getKey().code) == false ? InputState.RELEASED : InputState.HELD : InputState.HELD);
					break;
				case RELEASED:
					entry.setValue(InputState.NO_INPUT);
					break;
				default:
					break;
			}
		}
	}

	public static boolean isButtonDown(ButtonCode button)
	{
		return ButtonStates.containsKey(button) ? (ButtonStates.get(button).equals(InputState.PRESSED) || ButtonStates.get(button).equals(InputState.HELD)) : false;
	}

	public static boolean isButtonUp(ButtonCode button)
	{
		return ButtonStates.containsKey(button) ? (ButtonStates.get(button).equals(InputState.NO_INPUT) || ButtonStates.get(button).equals(InputState.RELEASED)) : false;
	}

	public static boolean isButtonTapped(ButtonCode button)
	{
		return ButtonStates.containsKey(button) ? ButtonStates.get(button).equals(InputState.RELEASED) : false;
	}

	public static InputState getButtonState(ButtonCode button)
	{
		return ButtonStates.containsKey(button) ? ButtonStates.get(button) : InputState.NO_INPUT;
	}

	public static Vector2 getMousePosition()
	{
		if (mousePosition == null)
		{
			mousePosition = new Vector2();
		}
		return mousePosition.set(x, y);
	}

	public static Vector2 getDeltaVector()
	{
		if (deltaVector == null)
		{
			deltaVector = new Vector2();
		}
		return deltaVector.set(delta_x, delta_y);
	}

	public static Vector2 getScrollVector()
	{
		if (scrollVector == null)
		{
			scrollVector = new Vector2();
		}
		return scrollVector.set(scroll_x, scroll_y);
	}

	public static int getPositionX()
	{
		return x;
	}

	public static int getPositionY()
	{
		return y;
	}

	public static int getDeltaX()
	{
		return delta_x;
	}

	public static int getDeltaY()
	{
		return delta_y;
	}

	public static float getScrollX()
	{
		return scroll_x;
	}

	public static float getScrollY()
	{
		return scroll_y;
	}

	public static void consume(ButtonCode button)
	{
		ButtonStates.put(button, InputState.NO_INPUT);
		eventStates.put(button.code, false);
	}

	public static void resetDelta()
	{
		delta_x = 0;
		delta_y = 0;
	}

	public enum ButtonCode
	{
		BUTTON_NONE(-1),
		BUTTON_1(GLFW.GLFW_MOUSE_BUTTON_1),
		BUTTON_2(GLFW.GLFW_MOUSE_BUTTON_2),
		BUTTON_3(GLFW.GLFW_MOUSE_BUTTON_3),
		BUTTON_4(GLFW.GLFW_MOUSE_BUTTON_4),
		BUTTON_5(GLFW.GLFW_MOUSE_BUTTON_5),
		BUTTON_6(GLFW.GLFW_MOUSE_BUTTON_6),
		BUTTON_7(GLFW.GLFW_MOUSE_BUTTON_7),
		BUTTON_8(GLFW.GLFW_MOUSE_BUTTON_8),
		BUTTON_LEFT(BUTTON_1.getCode()),
		BUTTON_RIGHT(BUTTON_2.getCode()),
		BUTTON_MIDDLE(BUTTON_3.getCode());

		private final int code;

		private ButtonCode(int code)
		{
			this.code = code;
		}

		public static ButtonCode getCode(int code)
		{
			for (ButtonCode b : ButtonCode.values())
			{
				if (b.getCode() == code)
				{
					return b;
				}
			}
			return BUTTON_NONE;
		}

		public int getCode()
		{
			return code;
		}
	}
}
