package bmg.emef.Input;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.lwjgl.glfw.GLFW;

import bmg.emef.Input.Input.InputState;

public class Controllers
{
	public static final int CONTROLLER_0 = 0;
	public static final int CONTROLLER_1 = 1;
	public static final int CONTROLLER_2 = 2;
	public static final int CONTROLLER_3 = 3;
	public static final int CONTROLLER_4 = 4;
	public static final int CONTROLLER_5 = 5;
	public static final int CONTROLLER_6 = 6;
	public static final int CONTROLLER_7 = 7;
	public static final int CONTROLLER_8 = 8;
	public static final int CONTROLLER_9 = 9;
	public static final int CONTROLLER_10 = 10;
	public static final int CONTROLLER_11 = 11;
	public static final int CONTROLLER_12 = 12;
	public static final int CONTROLLER_13 = 13;
	public static final int CONTROLLER_14 = 14;
	public static final int CONTROLLER_15 = 15;

	static Map<Integer, Controller> connectedControllers = new HashMap<>();

	private static ByteBuffer buttonBuffer;
	private static FloatBuffer axeBuffer;

	static void update()
	{
		for (Controller c : connectedControllers.values())
		{
			c.update();
			buttonBuffer = GLFW.glfwGetJoystickButtons(c.id);
			while (buttonBuffer.hasRemaining())
			{
				final boolean down = buttonBuffer.get() == 1;
				if (c.isButtonDown(buttonBuffer.position() - 1) != down)
				{
					c.buttonStates.put(buttonBuffer.position() - 1, down ? InputState.PRESSED : InputState.RELEASED);
				}
			}
			axeBuffer = GLFW.glfwGetJoystickAxes(c.id);
			while (axeBuffer.hasRemaining())
			{
				Controller.Axe a = c.axeStates.get(axeBuffer.position());
				if (a == null)
				{
					a = new Controller.Axe();
					a.state = InputState.NO_INPUT;
					a.amount = 0.0d;
					c.axeStates.put(axeBuffer.position(), a);
				}
				a.amount = axeBuffer.get();
			}
		}
	}

	/**
	 * 
	 * @param controllerID
	 * @return {@link Controller} with given ID, or {@link null} if no Controller exists
	 */
	public static Controller getControllerByID(int controllerID)
	{
		return connectedControllers.get(controllerID);
	}

	/**
	 * Remove the {@link Controller} object from the map of connected controllers
	 * 
	 * @param controller
	 */
	public static void removeController(Controller controller)
	{
		if (controller != null)
		{
			connectedControllers.remove(controller);
		}
	}

	public static int getNumberOfConnectedControllers()
	{
		int count = 0;
		for (Controller c : connectedControllers.values())
		{
			if (c.connected)
			{
				count++;
			}
		}
		return count;
	}

	public static List<Controller> getListOfConnectedControllers(List<Controller> dest)
	{
		if (dest == null)
		{
			dest = new ArrayList<>();
		}
		for (Controller c : connectedControllers.values())
		{
			if (c.connected)
			{
				dest.add(c);
			}
		}
		return dest;
	}
}
