package bmg.emef.buffer;

import java.nio.ByteBuffer;

import org.lwjgl.system.MemoryUtil;

import bmg.emef.environment.Environment;

public class DirectBuffer
{
	// static functions
	public static DirectBuffer wrap(int... data)
	{
		DirectBuffer buffer = create(data.length * PrimitiveSize.INT);
		for (int i = 0; i < data.length; i++)
		{
			buffer.writeInt(i * PrimitiveSize.INT, data[i]);
		}
		return buffer;
	}

	public static DirectBuffer wrap(float... data)
	{
		DirectBuffer buffer = create(data.length * PrimitiveSize.FLOAT);
		for (int i = 0; i < data.length; i++)
		{
			buffer.writeFloat(i * PrimitiveSize.FLOAT, data[i]);
		}
		return buffer;
	}

	public static DirectBuffer wrap(long... data)
	{
		DirectBuffer buffer = create(data.length * PrimitiveSize.LONG);
		for (int i = 0; i < data.length; i++)
		{
			buffer.writeLong(i * PrimitiveSize.LONG, data[i]);
		}
		return buffer;
	}

	public static DirectBuffer wrap(double... data)
	{
		DirectBuffer buffer = create(data.length * PrimitiveSize.DOUBLE);
		for (int i = 0; i < data.length; i++)
		{
			buffer.writeDouble(i * PrimitiveSize.DOUBLE, data[i]);
		}
		return buffer;
	}

	public static DirectBuffer wrap(short... data)
	{
		DirectBuffer buffer = create(data.length * PrimitiveSize.SHORT);
		for (int i = 0; i < data.length; i++)
		{
			buffer.writeShort(i * PrimitiveSize.SHORT, data[i]);
		}
		return buffer;
	}

	public static DirectBuffer wrap(byte... data)
	{
		DirectBuffer buffer = create(data.length);
		for (int i = 0; i < data.length; i++)
		{
			buffer.writeByte(i, data[i]);
		}
		return buffer;
	}

	public static DirectBuffer create(int sizeInBytes)
	{
		return new DirectBuffer(sizeInBytes);
	}

	public static void free(DirectBuffer directBuffer)
	{
		if (directBuffer != null)
		{
			directBuffer.free();
		}
	}

	private ByteBuffer nativeBuffer;
	private boolean disposed;

	public DirectBuffer(int sizeInBytes)
	{
		nativeBuffer = MemoryUtil.memAlloc(sizeInBytes);
		disposed = false;
		Environment.eventManager.registerDisposeListener(this::free);
	}

	public DirectBuffer(ByteBuffer buffer)
	{
		nativeBuffer = buffer;
		disposed = false;
		Environment.eventManager.registerDisposeListener(this::free);
	}

	public DirectBuffer writeInt(int index, int value)
	{
		nativeBuffer.putInt(index, value);
		return this;
	}

	public DirectBuffer writeFloat(int index, float value)
	{
		nativeBuffer.putFloat(index, value);
		return this;
	}

	public DirectBuffer writeLong(int index, long value)
	{
		nativeBuffer.putLong(index, value);
		return this;
	}

	public DirectBuffer writeDouble(int index, double value)
	{
		nativeBuffer.putDouble(index, value);
		return this;
	}

	public DirectBuffer writeShort(int index, short value)
	{
		nativeBuffer.putShort(index, value);
		return this;
	}

	public DirectBuffer writeByte(int index, byte value)
	{
		nativeBuffer.put(index, value);
		return this;
	}

	public int readInt(int index)
	{
		return nativeBuffer.getInt(index);
	}

	public float readFloat(int index)
	{
		return nativeBuffer.getFloat(index);
	}

	public long readLong(int index)
	{
		return nativeBuffer.getLong(index);
	}

	public double readDouble(int index)
	{
		return nativeBuffer.getDouble(index);
	}

	public short readShort(int index)
	{
		return nativeBuffer.getShort(index);
	}

	public byte readByte(int index)
	{
		return nativeBuffer.get(index);
	}

	public int sizeInBytes()
	{
		return nativeBuffer.capacity();
	}

	public ByteBuffer nativeBuffer()
	{
		return nativeBuffer;
	}

	public DirectBuffer clear()
	{
		nativeBuffer.clear();
		return this;
	}

	public void free()
	{
		if (!disposed)
		{
			disposed = true;
			MemoryUtil.memFree(nativeBuffer);
		}
	}
}
