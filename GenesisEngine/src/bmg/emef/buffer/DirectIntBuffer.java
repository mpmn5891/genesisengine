package bmg.emef.buffer;

public class DirectIntBuffer
{
	public static void free(DirectIntBuffer buffer)
	{
		if (buffer != null)
		{
			DirectBuffer.free(buffer.getDirectBuffer());
		}
	}

	protected DirectBuffer directBuffer;

	public DirectIntBuffer(DirectBuffer buffer)
	{
		this.directBuffer = buffer;
	}

	public DirectIntBuffer(int sizeInInts)
	{
		directBuffer = DirectBuffer.create(sizeInInts * PrimitiveSize.INT);
	}

	public DirectIntBuffer write(int index, int value)
	{
		directBuffer.writeInt(index * PrimitiveSize.INT, value);
		return this;
	}

	public int read(int index)
	{
		return directBuffer.readInt(index * PrimitiveSize.INT);
	}

	public DirectBuffer getDirectBuffer()
	{
		return directBuffer;
	}
}
