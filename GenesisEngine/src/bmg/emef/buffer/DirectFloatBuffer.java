package bmg.emef.buffer;

public class DirectFloatBuffer
{
	public static void free(DirectFloatBuffer buffer)
	{
		if (buffer != null)
		{
			DirectBuffer.free(buffer.getDirectBuffer());
		}
	}

	protected DirectBuffer directBuffer;

	public DirectFloatBuffer(DirectBuffer buffer)
	{
		this.directBuffer = buffer;
	}

	public DirectFloatBuffer(int sizeInFloats)
	{
		directBuffer = DirectBuffer.create(sizeInFloats * PrimitiveSize.FLOAT);
	}

	public DirectFloatBuffer write(int index, float value)
	{
		directBuffer.writeFloat(index * PrimitiveSize.FLOAT, value);
		return this;
	}

	public float read(int index)
	{
		return directBuffer.readFloat(index * PrimitiveSize.FLOAT);
	}

	public DirectBuffer getDirectBuffer()
	{
		return directBuffer;
	}
}
