package bmg.emef.buffer;

public final class PrimitiveSize
{
	public static final int BYTE = 1;
	public static final int INT = Integer.SIZE / Byte.SIZE;
	public static final int CHAR = Character.SIZE / Byte.SIZE;
	public static final int FLOAT = Float.SIZE / Byte.SIZE;
	public static final int DOUBLE = Double.SIZE / Byte.SIZE;
	public static final int LONG = Long.SIZE / Byte.SIZE;
	public static final int SHORT = Short.SIZE / Byte.SIZE;

	private PrimitiveSize()
	{
		// nothing to see here, prevents instantiation
	}
}
