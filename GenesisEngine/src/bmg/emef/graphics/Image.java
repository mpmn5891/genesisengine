package bmg.emef.graphics;

import bmg.emef.buffer.DirectBuffer;
import bmg.emef.buffer.DirectFloatBuffer;
import bmg.emef.buffer.PrimitiveSize;
import bmg.emef.environment.Environment;
import bmg.emef.environment.Exceptions;

public class Image
{
	private boolean disposed;
	private int width;
	private int height;
	private DirectFloatBuffer imageData;

	public Image(int width, int height)
	{
		this.width = width;
		this.height = height;
		imageData = new DirectFloatBuffer(width * height * PrimitiveSize.FLOAT);
		disposed = false;
		Environment.eventManager.registerDisposeListener(this::dispose);
	}

	public Image setPixel(int x, int y, Color pixel)
	{
		if (pixel == null)
		{
			throw new Exceptions.GLException("Pixel can not be NULL");
		}
		int start = 4 * (width * y + x);
		imageData.write(start, pixel.r).write(start + 1, pixel.g).write(start + 2, pixel.b).write(start + 3, pixel.a);
		return this;
	}

	public Color getPixel(int x, int y, Color dest)
	{
		if (dest == null)
		{
			throw new Exceptions.GLException("Dest(Color) can not be NULL");
		}
		int start = 4 * (width * y + x);
		dest.r = imageData.read(start);
		dest.g = imageData.read(start + 1);
		dest.b = imageData.read(start + 2);
		dest.a = imageData.read(start + 3);
		return dest;
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public DirectBuffer getImageData()
	{
		return imageData.getDirectBuffer();
	}

	public void dispose()
	{
		if (disposed)
		{
			Environment.log.warn("Tried to dispose an already disposed Image");
		}
		else
		{
			imageData.getDirectBuffer().free();
			disposed = true;
		}
	}
}
