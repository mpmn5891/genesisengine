package bmg.emef.graphics.gl;

import bmg.emef.environment.Environment;
import bmg.emef.environment.Exceptions;

public class ArrayObject
{
	// static functions
	public static ArrayObject CURRENT;

	public static boolean isValid(int id)
	{
		return Environment.graphics.glIsVertexArray(id);
	}

	private int id;
	private boolean disposed;

	public ArrayObject()
	{
		id = Environment.graphics.glGenVertexArrays();
		GLError.check();
		disposed = false;
		Environment.eventManager.registerDisposeListener(this::dispose);
	}

	public void bind()
	{
		bind(false);
	}

	public void bind(boolean force)
	{
		if (disposed)
		{
			throw new Exceptions.GLException("Can not bind a disposed ArrayObject");
		}
		if (force || CURRENT != this)
		{
			Environment.graphics.glBindVertexArray(id);
			GLError.check();
			CURRENT = this;
		}
	}

	public void enableAttributeArray(int index)
	{
		bind();
		Environment.graphics.glEnableVertexAttribArray(index);
		GLError.check();
	}

	public void disableAttributeArray(int index)
	{
		bind();
		Environment.graphics.glDisableVertexAttribArray(index);
		GLError.check();
	}

	public void pointAttribute(int index, int count, int type, BufferObject buffer)
	{
		pointAttribute(index, count, type, false, buffer);
	}

	public void pointAttribute(int index, int count, int type, boolean normalized, BufferObject buffer)
	{
		pointAttribute(index, count, type, normalized, 0, 0, buffer);
	}

	public void pointAttribute(int index, int count, int type, boolean normalized, int stride, long offset, BufferObject buffer)
	{
		bind();
		buffer.bind();
		Environment.graphics.glVertexAttribPointer(index, count, type, normalized, stride, offset);
		GLError.check();
	}

	public int getID()
	{
		return this.id;
	}

	public boolean isValid()
	{
		return isValid(id);
	}

	public boolean isDisposed()
	{
		return disposed;
	}

	public void dispose()
	{
		if (disposed)
		{
			Environment.log.warn("Tried to dispose an already disposed OpenGL ArrayObject");
		}
		else
		{
			Environment.graphics.glBindVertexArray(0);
			GLError.check();
			Environment.graphics.glDeleteVertexArrays(id);
			GLError.check();
			disposed = true;
		}
	}
}
