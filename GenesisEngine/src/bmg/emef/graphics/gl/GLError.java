package bmg.emef.graphics.gl;

import static bmg.emef.graphics.Graphics.Constants.GL_INVALID_ENUM;
import static bmg.emef.graphics.Graphics.Constants.GL_INVALID_FRAMEBUFFER_OPERATION;
import static bmg.emef.graphics.Graphics.Constants.GL_INVALID_OPERATION;
import static bmg.emef.graphics.Graphics.Constants.GL_INVALID_VALUE;
import static bmg.emef.graphics.Graphics.Constants.GL_NO_ERROR;
import static bmg.emef.graphics.Graphics.Constants.GL_OUT_OF_MEMORY;

import bmg.emef.environment.Environment;
import bmg.emef.environment.Exceptions;

public final class GLError
{
	private GLError()
	{
		// nothing to see here, prevents instantiation
	}

	public static void check()
	{
		check(false);
	}

	public static void check(boolean force)
	{
		if (!force && !Environment.Analytics.development)
		{
			return;
		}

		switch (Environment.graphics.glGetError())
		{
			case GL_NO_ERROR:
			{
				break;
			}
			case GL_INVALID_ENUM:
			{
				throw new Exceptions.GLException.InvalidEnum();
			}
			case GL_INVALID_VALUE:
			{
				throw new Exceptions.GLException.InvalidValue();
			}
			case GL_INVALID_OPERATION:
			{
				throw new Exceptions.GLException.InvalidOperation();
			}
			case GL_INVALID_FRAMEBUFFER_OPERATION:
			{
				throw new Exceptions.GLException.InvalidFramebufferOperation();
			}
			case GL_OUT_OF_MEMORY:
			{
				throw new Exceptions.GLException.OutOfMemory();
			}
		}
	}

	public static Value get()
	{
		switch (Environment.graphics.glGetError())
		{
			case GL_INVALID_ENUM:
			{
				return Value.INVALID_ENUM;
			}
			case GL_INVALID_VALUE:
			{
				return Value.INVALID_VALUE;
			}
			case GL_INVALID_OPERATION:
			{
				return Value.INVALID_OPERATION;
			}
			case GL_INVALID_FRAMEBUFFER_OPERATION:
			{
				return Value.INVALID_FRAMEBUFFER_OPERATION;
			}
			case GL_OUT_OF_MEMORY:
			{
				return Value.OUT_OF_MEMORY;
			}
			default:
			{
				return Value.NO_ERROR;
			}
		}
	}

	public enum Value
	{
		NO_ERROR,
		INVALID_ENUM,
		INVALID_VALUE,
		INVALID_OPERATION,
		INVALID_FRAMEBUFFER_OPERATION,
		OUT_OF_MEMORY
	}
}
