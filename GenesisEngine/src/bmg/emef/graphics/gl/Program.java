package bmg.emef.graphics.gl;

import static bmg.emef.graphics.Graphics.Constants.GL_FALSE;
import static bmg.emef.graphics.Graphics.Constants.GL_LINK_STATUS;
import static bmg.emef.graphics.Graphics.Constants.GL_TRUE;

import java.util.HashMap;
import java.util.Map;

import bmg.emef.buffer.DirectFloatBuffer;
import bmg.emef.environment.Environment;
import bmg.emef.environment.Exceptions;
import bmg.emef.graphics.Color;
import bmg.emef.math.Matrix3;
import bmg.emef.math.Matrix4;
import bmg.emef.math.Transform;
import bmg.emef.math.Vector2;
import bmg.emef.math.Vector3;
import bmg.emef.math.Vector4;
import bmg.emef.util.BufferUtils;

public class Program
{
	// static functions
	private static DirectFloatBuffer m3buffer;
	private static DirectFloatBuffer m4buffer;
	public static Program CURRENT;

	private int id;
	private boolean disposed;
	private Map<String, Integer> uniformLocations;
	private Map<String, Integer> attributeLocations;

	public Program()
	{
		id = Environment.graphics.glCreateProgram();
		GLError.check();
		uniformLocations = new HashMap<>();
		attributeLocations = new HashMap<>();
		if (m3buffer == null)
		{
			m3buffer = new DirectFloatBuffer(9);
		}
		if (m4buffer == null)
		{
			m4buffer = new DirectFloatBuffer(16);
		}
		disposed = false;
		Environment.eventManager.registerDisposeListener(this::dispose);
	}

	public void use()
	{
		if (CURRENT != this)
		{
			if (disposed)
			{
				throw new Exceptions.GLException("Can not use a disposed Program");
			}
			else
			{
				Environment.graphics.glUseProgram(id);
				GLError.check();
				Program.CURRENT = this;
				prepareFrame();
			}
		}
	}

	public void prepareFrame()
	{

	}

	public void attach(Shader shader)
	{
		if (shader == null)
		{
			throw new Exceptions.GLException("Shader can not be NULL");
		}
		else
		{
			if (disposed)
			{
				throw new Exceptions.GLException("Can not attach Shader to a disposed Program");
			}
			else
			{
				Environment.graphics.glAttachShader(id, shader.getID());
				GLError.check();
			}
		}
	}

	public void link()
	{
		if (disposed)
		{
			throw new Exceptions.GLException("Can not link a disposed Program");
		}
		else
		{
			Environment.graphics.glLinkProgram(id);
			GLError.check();
			if (Environment.graphics.glGetProgrami(id, GL_LINK_STATUS) == GL_FALSE)
			{
				throw new Exceptions.GLException("Unable to link Program:\n" + getInfoLog());
			}
		}
	}

	public String getInfoLog()
	{
		if (disposed)
		{
			throw new Exceptions.GLException("Can not get info log from disposed Program");
		}
		else
		{
			return Environment.graphics.glGetProgramInfoLog(id);
		}
	}

	public int getAttribute(String name)
	{
		if (attributeLocations.containsKey(name))
		{
			return attributeLocations.get(name);
		}
		else
		{
			use();
			int location = Environment.graphics.glGetAttribLocation(id, name);
			attributeLocations.put(name, location);
			GLError.check();
			return location;
		}
	}

	public int getUniform(String name)
	{
		if (uniformLocations.containsKey(name))
		{
			return uniformLocations.get(name);
		}
		else
		{
			use();
			int location = Environment.graphics.glGetUniformLocation(id, name);
			uniformLocations.put(name, location);
			GLError.check();
			return location;
		}
	}

	public void setUniform(int location, int... values)
	{
		if (values.length > 4)
		{
			throw new Exceptions.GLException("Uniform component can not have more than 4 components");
		}
		else
		{
			use();
			switch (values.length)
			{
				case 1:
				{
					Environment.graphics.glUniform1i(location, values[0]);
					break;
				}
				case 2:
				{
					Environment.graphics.glUniform2i(location, values[0], values[1]);
					break;
				}
				case 3:
				{
					Environment.graphics.glUniform3i(location, values[0], values[1], values[2]);
					break;
				}
				case 4:
				{
					Environment.graphics.glUniform4i(location, values[0], values[1], values[2], values[3]);
					break;
				}
			}
			GLError.check();
		}
	}

	public void setUniform(int location, float... values)
	{
		if (values.length > 4)
		{
			throw new Exceptions.GLException("Uniform can not have more than 4 components");
		}
		else
		{
			use();
			switch (values.length)
			{
				case 1:
				{
					Environment.graphics.glUniform1f(location, values[0]);
					break;
				}
				case 2:
				{
					Environment.graphics.glUniform2f(location, values[0], values[1]);
					break;
				}
				case 3:
				{
					Environment.graphics.glUniform3f(location, values[0], values[1], values[2]);
					break;
				}
				case 4:
				{
					Environment.graphics.glUniform4f(location, values[0], values[1], values[2], values[3]);
					break;
				}
			}
			GLError.check();
		}
	}

	public void setUniform(int location, boolean transpose, Matrix3 mat)
	{
		use();
		Environment.graphics.glUniformMatrix3fv(location, transpose, BufferUtils.storeInto(mat, m3buffer));
		GLError.check();
	}

	public void setUniform(int location, boolean transpose, Matrix4 mat)
	{
		use();
		Environment.graphics.glUniformMatrix4fv(location, transpose, BufferUtils.storeInto(mat, m4buffer));
		GLError.check();
	}

	public void setUniform(int location, boolean value)
	{
		use();
		Environment.graphics.glUniform1i(location, value ? GL_TRUE : GL_FALSE);
		GLError.check();
	}

	public void setUniform(String name, boolean value)
	{
		setUniform(getUniform(name), value);
	}

	public void setUniform(String name, int... values)
	{
		setUniform(getUniform(name), values);
	}

	public void setUniform(String name, float... values)
	{
		setUniform(getUniform(name), values);
	}

	public void setUniform(int location, Vector2 value)
	{
		setUniform(location, value.x, value.y);
	}

	public void setUniform(int location, Vector3 value)
	{
		setUniform(location, value.x, value.y, value.z);
	}

	public void setUniform(int location, Color value)
	{
		setUniform(location, value.r, value.g, value.b, value.a);
	}

	public void setUniform(int location, Vector4 value)
	{
		setUniform(location, value.x, value.y, value.z, value.w);
	}

	public void setUniform(String name, Vector2 value)
	{
		setUniform(name, value.x, value.y);
	}

	public void setUniform(String name, Vector3 value)
	{
		setUniform(name, value.x, value.y, value.z);
	}

	public void setUniform(String name, Vector4 value)
	{
		setUniform(name, value.x, value.y, value.z, value.w);
	}

	public void setUniform(String name, Color value)
	{
		setUniform(name, value.r, value.g, value.b, value.a);
	}

	public void setUniform(int location, Matrix3 value)
	{
		setUniform(location, false, value);
	}

	public void setUniform(int location, Matrix4 value)
	{
		setUniform(location, false, value);
	}

	public void setUniform(int location, Transform value)
	{
		setUniform(location, false, value.matrix);
	}

	public void setUniform(String name, boolean transpose, Matrix3 value)
	{
		setUniform(getUniform(name), transpose, value);
	}

	public void setUniform(String name, Matrix3 value)
	{
		setUniform(name, false, value);
	}

	public void setUniform(String name, boolean transpose, Matrix4 value)
	{
		setUniform(getUniform(name), transpose, value);
	}

	public void setUniform(String name, Matrix4 value)
	{
		setUniform(name, false, value);
	}

	public void setUniform(String name, Transform value)
	{
		setUniform(name, false, value.matrix);
	}

	public int getID()
	{
		return id;
	}

	public boolean isDisposed()
	{
		return disposed;
	}

	public void dispose()
	{
		if (disposed)
		{
			Environment.log.warn("Tried to dispose an already disposed OpenGL Program");
		}
		else
		{
			Environment.graphics.glDeleteProgram(id);
			GLError.check();
			disposed = true;
		}
	}
}
