package bmg.emef.graphics.gl;

import static bmg.emef.graphics.Graphics.Constants.GL_COMPILE_STATUS;
import static bmg.emef.graphics.Graphics.Constants.GL_FALSE;
import static bmg.emef.graphics.Graphics.Constants.GL_FRAGMENT_SHADER;
import static bmg.emef.graphics.Graphics.Constants.GL_VERTEX_SHADER;

import bmg.emef.environment.Environment;
import bmg.emef.environment.Exceptions;

public class Shader
{
	private int id;
	private Type type;
	private boolean disposed;

	public Shader(int type)
	{
		this(type == Type.VERTEX.getValue() ? Type.VERTEX : Type.FRAGMENT);
	}

	public Shader(Type type)
	{
		this(Environment.graphics.glCreateShader(type.getValue()), type);
	}

	public Shader(int id, Type type)
	{
		this.id = id;
		this.type = type;
		GLError.check();
		disposed = false;
		Environment.eventManager.registerDisposeListener(this::dispose);
	}

	public void source(String... source)
	{
		if (disposed)
		{
			throw new Exceptions.GLException("Can not add source to a disposed Shader");
		}
		else
		{
			Environment.graphics.glShaderSource(id, source);
			GLError.check();
		}
	}

	public void compile()
	{
		if (disposed)
		{
			throw new Exceptions.GLException("Can not compile a disposed Shader");
		}
		else
		{
			Environment.graphics.glCompileShader(id);
			GLError.check();
			if (Environment.graphics.glGetShaderi(id, GL_COMPILE_STATUS) == GL_FALSE)
			{
				throw new Exceptions.GLException("Unable to compile shader:\n" + getInfoLog());
			}
		}
	}

	public String getInfoLog()
	{
		if (disposed)
		{
			throw new Exceptions.GLException("Can not get info log from disposed Shader");
		}
		else
		{
			return Environment.graphics.glGetShaderInfoLog(id);
		}
	}

	public boolean isDisposed()
	{
		return disposed;
	}

	public int getID()
	{
		return id;
	}

	public Type getType()
	{
		return type;
	}

	public void dispose()
	{
		if (disposed)
		{
			Environment.log.warn("Tried to disposed an already disposed OpenGL Shader");
		}
		else
		{
			Environment.graphics.glDeleteShader(id);
			GLError.check();
			disposed = true;
		}
	}

	public enum Type
	{
		VERTEX(GL_VERTEX_SHADER),
		FRAGMENT(GL_FRAGMENT_SHADER);

		private int value;

		Type(int value)
		{
			this.value = value;
		}

		public int getValue()
		{
			return value;
		}
	}
}
