package bmg.emef.graphics.gl;

import bmg.emef.environment.Environment;
import bmg.emef.math.Vector2;

public class SubTexture extends Texture
{
	private Texture parent;
	private float minU, maxU, minV, maxV;
	private float width, height;

	public SubTexture(Texture parent, Vector2 min, Vector2 max)
	{
		this(parent, min.x, min.y, max.x, max.y);
	}

	public SubTexture(Texture parent, float minU, float minV, float maxU, float maxV)
	{
		super(parent.getID());
		this.parent = parent;
		this.minU = minU;
		this.maxU = maxU;
		this.minV = minV;
		this.maxV = maxV;
		this.width = (maxU - minU) * parent.getWidth();
		this.height = (maxV - minV) * parent.getHeight();
	}

	public SubTexture(Texture parent, float minU, float minV, float maxU, float maxV, float width, float height)
	{
		this(parent, minU, minV, maxU, maxV);
		this.width = width;
		this.height = height;
	}

	public Texture getParent()
	{
		return parent;
	}

	public SubTexture getSubTexture(float minU, float minV, float maxU, float maxV)
	{
		minU = (minU * getWidth()) / getParent().getWidth();
		minV = (minV * getHeight()) / getParent().getHeight();
		maxU = (maxU * getWidth()) / getParent().getWidth();
		maxV = (maxV * getHeight()) / getParent().getHeight();
		minU += this.minU;
		minV += this.minV;
		maxU += minU;
		maxV += minV;
		return new SubTexture(parent, minU, minV, maxU, maxV);
	}

	public SubTexture getSubTexture(Vector2 min, Vector2 max)
	{
		return getSubTexture(min.x, min.y, max.x, max.y);
	}

	@Override
	public void dispose()
	{
		Environment.log.warn("No need to dispose a SubTexture");
	}

	@Override
	public float getWidth()
	{
		return width;
	}

	@Override
	public float getHeight()
	{
		return height;
	}

	@Override
	public float getMinU()
	{
		return minU;
	}

	@Override
	public float getMaxU()
	{
		return maxU;
	}

	@Override
	public float getMinV()
	{
		return minV;
	}

	@Override
	public float getMaxV()
	{
		return maxV;
	}
}
