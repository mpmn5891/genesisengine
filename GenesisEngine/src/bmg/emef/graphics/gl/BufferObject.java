package bmg.emef.graphics.gl;

import static bmg.emef.graphics.Graphics.Constants.GL_ARRAY_BUFFER;
import static bmg.emef.graphics.Graphics.Constants.GL_DYNAMIC_DRAW;
import static bmg.emef.graphics.Graphics.Constants.GL_ELEMENT_ARRAY_BUFFER;
import static bmg.emef.graphics.Graphics.Constants.GL_STATIC_DRAW;
import static bmg.emef.graphics.Graphics.Constants.GL_STREAM_DRAW;

import java.util.HashMap;
import java.util.Map;

import bmg.emef.buffer.DirectBuffer;
import bmg.emef.environment.Environment;
import bmg.emef.environment.Exceptions;

public class BufferObject
{
	// static functions
	static Map<Integer, BufferObject> CURRENT = new HashMap<>();

	public static boolean isValid(int id)
	{
		boolean value = Environment.graphics.glIsBuffer(id);
		GLError.check();
		return value;
	}

	private int id;
	private int capacity;
	private Target target;
	private boolean disposed;

	public BufferObject(Target target)
	{
		id = Environment.graphics.glGenBuffers();
		this.target = target;
		disposed = false;
		GLError.check();
		Environment.eventManager.registerDisposeListener(this::dispose);
	}

	public void uploadData(DirectBuffer data, Usage usage)
	{
		bind();
		capacity = data.sizeInBytes();
		Environment.graphics.glBufferData(target.getValue(), data, usage.getValue());
		GLError.check();
	}

	public void uploadData(int capacity, Usage usage)
	{
		bind();
		this.capacity = capacity;
		Environment.graphics.glBufferData(target.getValue(), capacity, usage.getValue());
		GLError.check();
	}

	public void uploadSubData(DirectBuffer data)
	{
		uploadSubData(data, 0, data.sizeInBytes());
	}

	public void uploadSubData(DirectBuffer data, int offset)
	{
		uploadSubData(data, offset, data.sizeInBytes());
	}

	public void uploadSubData(DirectBuffer data, int offset, int size)
	{
		if (capacity < data.sizeInBytes() - offset)
		{
			throw new Exceptions.GLException("Not enough capacity");
		}
		else
		{
			bind();
			Environment.graphics.glBufferSubData(target.getValue(), offset, size, data);
			GLError.check();
		}
	}

	public void bind()
	{
		if (disposed)
		{
			throw new Exceptions.GLException("BufferObject is diposed");
		}

		if (CURRENT.containsKey(target.getValue()) && BufferObject.CURRENT.get(target.getValue()) == this)
		{
			// prevent unneeded bind calls
			return;
		}

		Environment.graphics.glBindBuffer(target.getValue(), id);
		BufferObject.CURRENT.put(target.getValue(), this);
		GLError.check();
	}

	public void dispose()
	{
		if (disposed)
		{
			Environment.log.warn("Tried to dispose already disposed OpenGL BufferObject");
		}
		else
		{
			Environment.graphics.glBindBuffer(target.getValue(), 0);
			GLError.check();
			Environment.graphics.glDeleteBuffers(id);
			GLError.check();
			disposed = true;
		}
	}

	public boolean isValid()
	{
		return BufferObject.isValid(id);
	}

	public int getID()
	{
		return id;
	}

	public int getCapacity()
	{
		return capacity;
	}

	public Target getTarget()
	{
		return target;
	}

	public boolean isDisposed()
	{
		return disposed;
	}

	public enum Target
	{
		ARRAY_BUFFER(GL_ARRAY_BUFFER),
		ELEMENT_ARRAY_BUFFER(GL_ELEMENT_ARRAY_BUFFER);

		int value;

		Target(int value)
		{
			this.value = value;
		}

		public int getValue()
		{
			return value;
		}
	}

	public enum Usage
	{
		STREAM_DRAW(GL_STREAM_DRAW),
		STATIC_DRAW(GL_STATIC_DRAW),
		DYNAMIC_DRAW(GL_DYNAMIC_DRAW);

		int value;

		Usage(int value)
		{
			this.value = value;
		}

		public int getValue()
		{
			return value;
		}
	}
}
