package bmg.emef.graphics.gl;

import static bmg.emef.graphics.Graphics.Constants.GL_LINEAR;
import static bmg.emef.graphics.Graphics.Constants.GL_LINEAR_MIPMAP_LINEAR;
import static bmg.emef.graphics.Graphics.Constants.GL_RGB;
import static bmg.emef.graphics.Graphics.Constants.GL_RGBA;
import static bmg.emef.graphics.Graphics.Constants.GL_TEXTURE0;
import static bmg.emef.graphics.Graphics.Constants.GL_TEXTURE_2D;
import static bmg.emef.graphics.Graphics.Constants.GL_TEXTURE_MAG_FILTER;
import static bmg.emef.graphics.Graphics.Constants.GL_TEXTURE_MIN_FILTER;
import static bmg.emef.graphics.Graphics.Constants.GL_TEXTURE_WRAP_S;
import static bmg.emef.graphics.Graphics.Constants.GL_TEXTURE_WRAP_T;
import static bmg.emef.graphics.Graphics.Constants.GL_UNSIGNED_BYTE;

import bmg.emef.buffer.DirectBuffer;
import bmg.emef.environment.Environment;
import bmg.emef.environment.Exceptions;
import bmg.emef.graphics.Color;
import bmg.emef.graphics.Image;
import bmg.emef.math.Vector2;

public class Texture
{
	// static functions
	private static int activeUnit;
	public static Texture CURRENT;
	public static Texture EMPTY;

	public static int getActiveUnit()
	{
		return activeUnit;
	}

	public static void setActiveUnit(int unit)
	{
		if (unit != activeUnit)
		{
			GLError.check();
			Environment.graphics.glActiveTexture(GL_TEXTURE0 + unit);
			GLError.check();
			activeUnit = unit;
		}
	}

	public static Texture fromColor(Color c, int width, int height)
	{
		Image image = new Image(width, height);
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				image.setPixel(x, y, c);
			}
		}
		Texture texture = fromImage(image);
		image.dispose();
		return texture;
	}

	public static Texture fromImage(Image image)
	{
		int width = image.getWidth();
		int height = image.getHeight();
		DirectBuffer data = DirectBuffer.create(width * height * 4);
		Color color = Color.REUSABLE_STACK.pop();
		int index = 0;
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				image.getPixel(x, y, color);
				int r = (int) (color.r * 255f);
				int g = (int) (color.g * 255f);
				int b = (int) (color.b * 255f);
				int a = (int) (color.a * 255f);

				data.writeByte(index++, (byte) r)
						.writeByte(index++, (byte) g)
						.writeByte(index++, (byte) b)
						.writeByte(index++, (byte) a);

			}
		}
		Color.REUSABLE_STACK.push(color);
		Texture texture = fromDirectBuffer(data, width, height, 4);
		data.free();
		texture.width = image.getWidth();
		texture.height = image.getHeight();
		return texture;
	}

	public static Texture fromDirectBuffer(DirectBuffer buffer, int width, int height, int components)
	{
		Texture texture = new Texture();
		texture.bind();
		texture.setFilter(GL_LINEAR, GL_LINEAR);
		texture.image2d(buffer, GL_UNSIGNED_BYTE, components == 4 ? GL_RGBA : GL_RGB, width, height, GL_RGBA);
		if (texture.width >= 128 && texture.height >= 128)
		{
			texture.generateMipMaps();
			texture.setFilter(GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR);
		}
		return texture;
	}

	private int id;
	private float width;
	private float height;
	private boolean disposed;

	public Texture()
	{
		this(Environment.graphics.glGenTextures());
	}

	public Texture(int id)
	{
		this.id = id;
		GLError.check();
		disposed = false;
		Environment.eventManager.registerDisposeListener(this::dispose);
	}

	public void bind()
	{
		if (CURRENT == this)
		{
			return;
		}
		if (disposed)
		{
			throw new Exceptions.GLException("Can not bind a disposed texture");
		}
		Environment.graphics.glBindTexture(GL_TEXTURE_2D, id);
		GLError.check();
		CURRENT = this;
	}

	public void bind(int unit)
	{
		Texture.setActiveUnit(unit);
		bind();
	}

	public void setFilter(int min, int mag)
	{
		bind();
		Environment.graphics.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min);
		GLError.check();
		Environment.graphics.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mag);
		GLError.check();
	}

	public void image2d(DirectBuffer data, int type, int format, int width, int height, int internalFormat)
	{
		image2d(data, 0, type, format, width, height, internalFormat);
	}

	public void image2d(DirectBuffer data, int level, int type, int format, int width, int height, int internalFormat)
	{
		bind();
		Environment.graphics.glTexImage2D(GL_TEXTURE_2D, level, internalFormat, width, height, 0, format, type, data);
		GLError.check();
		this.width = width;
		this.height = height;
	}

	public void generateMipMaps()
	{
		bind();
		Environment.graphics.glGenerateMipmap(GL_TEXTURE_2D);
		GLError.check();
	}

	public void setWrapping(int s)
	{
		bind();
		Environment.graphics.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, s);
		GLError.check();
	}

	public void setWrapping(int s, int t)
	{
		bind();
		Environment.graphics.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, s);
		GLError.check();
		Environment.graphics.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, t);
		GLError.check();
	}

	public SubTexture getSubTexture(float minU, float minV, float maxU, float maxV)
	{
		return new SubTexture(this, minU, minV, maxU, maxV);
	}

	public SubTexture getSubTexture(Vector2 min, Vector2 max)
	{
		return new SubTexture(this, min, max);
	}

	public SubTexture getSubTexture(float minU, float minV, float maxU, float maxV, float width, float height)
	{
		return new SubTexture(this, minU, minV, maxU, maxV, width, height);
	}

	public int getID()
	{
		return id;
	}

	public float getWidth()
	{
		return width;
	}

	public float getHeight()
	{
		return height;
	}

	public float getMinU()
	{
		return 0;
	}

	public float getMaxU()
	{
		return 1;
	}

	public float getMinV()
	{
		return 0;
	}

	public float getMaxV()
	{
		return 1;
	}

	public boolean isDisposed()
	{
		return disposed;
	}

	public void dispose()
	{
		if (disposed)
		{
			Environment.log.warn("Tried to dispose an already disposed Texture");
		}
		else
		{
			Environment.graphics.glDeleteTextures(id);
			GLError.check();
			EMPTY.bind(activeUnit);
			disposed = true;
		}
	}

	@Override
	public int hashCode()
	{
		int result = getID();
		result = 31 * result + (getWidth() != +0.0f ? Float.floatToIntBits(getWidth()) : 0);
		result = 31 * result + (getHeight() != +0.0f ? Float.floatToIntBits(getHeight()) : 0);
		result = 31 * result + (disposed ? 1 : 0);
		return result;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Texture texture = (Texture) o;

		return getID() == texture.getID() &&
				Float.compare(texture.getWidth(), getWidth()) == 0 &&
				Float.compare(texture.getHeight(), getHeight()) == 0 &&
				disposed == texture.disposed;
	}
}
