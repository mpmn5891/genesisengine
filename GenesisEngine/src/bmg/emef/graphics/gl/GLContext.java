package bmg.emef.graphics.gl;

import static bmg.emef.graphics.Graphics.Constants.GL_ARRAY_BUFFER;

import bmg.emef.environment.Environment;
import bmg.emef.graphics.Color;
import bmg.emef.graphics.Graphics.Primitive;

public final class GLContext
{
	private GLContext()
	{
		// nothing to see here, prevents instantiation
	}

	public static void drawArrays(ArrayObject vao, Primitive mode, int offset, int vertexCount)
	{
		vao.bind();
		Environment.graphics.glDrawArrays(mode.getGLPrimitive(), offset, vertexCount);
		GLError.check();
	}

	public static void drawElements(ArrayObject vao, Primitive mode, int offset, int vertexCount, int type)
	{
		vao.bind();
		Environment.graphics.glDrawElements(mode.getGLPrimitive(), vertexCount, type, offset);
		GLError.check();
	}

	public static void bindVertexArrayObject(ArrayObject vao)
	{
		if (vao == null)
		{
			Environment.graphics.glBindVertexArray(0);
			GLError.check();
			ArrayObject.CURRENT = null;
		}
		else
		{
			vao.bind();
		}
	}

	public static void bindVertexBufferObject(BufferObject vbo)
	{
		if (vbo == null)
		{
			Environment.graphics.glBindBuffer(GL_ARRAY_BUFFER, 0);
			GLError.check();
			BufferObject.CURRENT.clear();
		}
		else
		{
			vbo.bind();
		}
	}

	public static void viewport(int x, int y, int width, int height)
	{
		Environment.graphics.glViewport(x, y, width, height);
		GLError.check();
	}

	public static void enable(int capability)
	{
		Environment.graphics.glEnable(capability);
		GLError.check();
	}

	public static void disable(int capability)
	{
		Environment.graphics.glDisable(capability);
		GLError.check();
	}

	public static void blendFunc(int src, int dest)
	{
		Environment.graphics.glBlendFunc(src, dest);
		GLError.check();
	}

	public static void depthFunc(int func)
	{
		Environment.graphics.glDepthFunc(func);
		GLError.check();
	}

	public static void depthMask(boolean value)
	{
		Environment.graphics.glDepthMask(value);
		GLError.check();
	}

	public static void clearColor(Color c)
	{
		GLContext.clearColor(c.r, c.g, c.b, c.a);
	}

	public static void clearColor(float r, float g, float b, float a)
	{
		Environment.graphics.glClearColor(r, g, b, a);
		GLError.check();
	}

	public static void clear(int buffer)
	{
		Environment.graphics.glClear(buffer);
		GLError.check();
	}

	public static void cullFace(int mode)
	{
		Environment.graphics.glCullFace(mode);
		GLError.check();
	}
}
