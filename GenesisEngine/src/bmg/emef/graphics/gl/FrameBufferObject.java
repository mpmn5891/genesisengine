package bmg.emef.graphics.gl;

import static bmg.emef.graphics.Graphics.Constants.GL_COLOR_ATTACHMENT0;
import static bmg.emef.graphics.Graphics.Constants.GL_COLOR_ATTACHMENT1;
import static bmg.emef.graphics.Graphics.Constants.GL_COLOR_ATTACHMENT2;
import static bmg.emef.graphics.Graphics.Constants.GL_COLOR_ATTACHMENT3;
import static bmg.emef.graphics.Graphics.Constants.GL_COLOR_ATTACHMENT4;
import static bmg.emef.graphics.Graphics.Constants.GL_COLOR_ATTACHMENT5;
import static bmg.emef.graphics.Graphics.Constants.GL_COLOR_ATTACHMENT6;
import static bmg.emef.graphics.Graphics.Constants.GL_COLOR_ATTACHMENT7;
import static bmg.emef.graphics.Graphics.Constants.GL_COLOR_BUFFER_BIT;
import static bmg.emef.graphics.Graphics.Constants.GL_DEPTH_ATTACHMENT;
import static bmg.emef.graphics.Graphics.Constants.GL_DEPTH_BUFFER_BIT;
import static bmg.emef.graphics.Graphics.Constants.GL_DEPTH_STENCIL_ATTACHMENT;
import static bmg.emef.graphics.Graphics.Constants.GL_DRAW_FRAMEBUFFER;
import static bmg.emef.graphics.Graphics.Constants.GL_FRAMEBUFFER;
import static bmg.emef.graphics.Graphics.Constants.GL_FRAMEBUFFER_COMPLETE;
import static bmg.emef.graphics.Graphics.Constants.GL_READ_FRAMEBUFFER;
import static bmg.emef.graphics.Graphics.Constants.GL_STENCIL_ATTACHMENT;
import static bmg.emef.graphics.Graphics.Constants.GL_TEXTURE_2D;

import bmg.emef.environment.Environment;
import bmg.emef.environment.Exceptions;

public class FrameBufferObject
{
	// static functions
	public static final FrameBufferObject SCREEN = new FrameBufferObject(0);
	public static FrameBufferObject CURRENT = SCREEN;

	public static boolean isValid(int id)
	{
		if (id == 0)
		{
			return true;
		}
		boolean value = Environment.graphics.glIsFramebuffer(id);
		GLError.check();
		return value;
	}

	private boolean disposed;
	private Target target;
	private int id;

	public FrameBufferObject()
	{
		this(Target.READ_WRITE);
	}

	public FrameBufferObject(int id)
	{
		this(id, Target.READ_WRITE);
	}

	public FrameBufferObject(Target target)
	{
		this(Environment.graphics.glGenFramebuffers(), target);
	}

	public FrameBufferObject(int id, Target target)
	{
		this.id = id;
		this.target = target;
		disposed = false;
		GLError.check();
		Environment.eventManager.registerDisposeListener(this::dispose);
	}

	public void texture2d(Texture texture)
	{
		texture2d(texture, Attachment.COLOR0);
	}

	public void texture2d(Texture texture, Attachment attachment)
	{
		texture2d(texture, attachment, 0);
	}

	public void texture2d(Texture texture, Attachment attachment, int level)
	{
		bind();
		Environment.graphics.glFramebufferTexture2D(target.getValue(), attachment.getValue(), GL_TEXTURE_2D, texture.getID(), level);
		GLError.check();
	}

	public void bind()
	{
		bind(false);
	}

	public void bind(boolean force)
	{
		if (disposed)
		{
			throw new Exceptions.GLException("Can not bind a disposed FrameBufferObject");
		}
		if (!force && CURRENT == this)
		{
			// prevent needless bind call
			return;
		}
		Environment.graphics.glBindFramebuffer(target.getValue(), id);
		GLError.check();
		Environment.graphics.glViewport(0, 0, Environment.display.getWidth(), Environment.display.getHeight());
		GLError.check();
		Environment.graphics.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		GLError.check();
		CURRENT = this;
	}

	public boolean isComplete()
	{
		bind();
		int status = Environment.graphics.glCheckFramebufferStatus(target.getValue());
		GLError.check();
		return status == GL_FRAMEBUFFER_COMPLETE;
	}

	public boolean isValid()
	{
		return FrameBufferObject.isValid(id);
	}

	public void release()
	{
		SCREEN.bind();
	}

	public int getID()
	{
		return id;
	}

	public Target getTarget()
	{
		return target;
	}

	public void dispose()
	{
		if (disposed)
		{
			Environment.log.warn("Tried to dispose an already disposed OpenGL FrameBufferObject");
		}
		else
		{
			release();
			Environment.graphics.glDeleteFramebuffers(id);
			GLError.check();
			disposed = true;
		}
	}

	public enum Target
	{
		READ_WRITE(GL_FRAMEBUFFER),
		READ(GL_READ_FRAMEBUFFER),
		DRAW(GL_DRAW_FRAMEBUFFER);

		int value;

		Target(int value)
		{
			this.value = value;
		}

		public int getValue()
		{
			return value;
		}
	}

	public enum Attachment
	{
		COLOR0(GL_COLOR_ATTACHMENT0),
		COLOR1(GL_COLOR_ATTACHMENT1),
		COLOR2(GL_COLOR_ATTACHMENT2),
		COLOR3(GL_COLOR_ATTACHMENT3),
		COLOR4(GL_COLOR_ATTACHMENT4),
		COLOR5(GL_COLOR_ATTACHMENT5),
		COLOR6(GL_COLOR_ATTACHMENT6),
		COLOR7(GL_COLOR_ATTACHMENT7),
		DEPTH(GL_DEPTH_ATTACHMENT),
		STENCIL(GL_STENCIL_ATTACHMENT),
		DEPTH_STANCIL(GL_DEPTH_STENCIL_ATTACHMENT);

		int value;

		Attachment(int value)
		{
			this.value = value;
		}

		public int getValue()
		{
			return value;
		}
	}
}
