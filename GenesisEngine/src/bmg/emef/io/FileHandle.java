package bmg.emef.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import bmg.emef.environment.Environment;
import bmg.emef.environment.Exceptions;
import bmg.emef.environment.Exceptions.FileIOException;

public abstract class FileHandle
{
	// static functions
	public static final char SEPARATOR = '/';

	public static FileHandle getExternalFile(String path)
	{
		return Environment.io.createExternalFileHandle(path);
	}

	public static FileHandle getInternalFile(String path)
	{
		return Environment.io.createInternalFileHandle(path);
	}

	protected String path;
	protected Type type;

	protected FileHandle(String path, Type type)
	{
		this.type = type;
		if (path.startsWith("http") || path.startsWith("ftp"))
		{
			this.path = path;
		}
		else
		{
			this.path = path.replaceAll("\\\\", "" + FileHandle.SEPARATOR).replaceAll("/+", "" + FileHandle.SEPARATOR).trim();
		}
		if (type == Type.INTERNAL && this.path.startsWith("/"))
		{
			this.path = this.path.replaceFirst("/", "");
		}
	}

	public String getPath()
	{
		if (path.trim().endsWith("" + FileHandle.SEPARATOR))
		{
			return path.trim().substring(0, path.lastIndexOf(FileHandle.SEPARATOR));
		}
		else
		{
			return path;
		}
	}

	public String getAbsolutePath()
	{
		return path;
	}

	public String getName()
	{
		String path = this.path;
		if (path.endsWith("" + FileHandle.SEPARATOR))
		{
			path = path.substring(0, path.length() - 1);
		}
		return path.substring(path.lastIndexOf(FileHandle.SEPARATOR) + 1);
	}

	public String getNameWithoutExtension()
	{
		return getName().replaceAll("\\." + getExtension(), "");
	}

	public String getExtension()
	{
		if (isDirectory())
		{
			return "";
		}
		else
		{
			String[] parts = getPath().split("\\.(?=[^\\.]+$)");
			return parts.length > 1 ? parts[1] : "";
		}
	}

	public FileHandle getParent()
	{
		String[] parts = path.split("" + FileHandle.SEPARATOR);
		String path = parts[0];
		for (int i = 1; i < parts.length - 1; i++)
		{
			path += FileHandle.SEPARATOR + parts[i] + FileHandle.SEPARATOR;
		}
		return type == Type.INTERNAL ? Environment.io.createInternalFileHandle(path + FileHandle.SEPARATOR) : Environment.io.createExternalFileHandle(path + FileHandle.SEPARATOR);
	}

	public FileHandle getChild(String path)
	{
		if (!isDirectory())
		{
			throw new Exceptions.FileIOException("Files do not have Children");
		}
		if (!exists())
		{
			throw new Exceptions.FileIOException("Can not get Children from non existing directory");
		}
		return type == Type.INTERNAL ? Environment.io.createInternalFileHandle(this.path + FileHandle.SEPARATOR + path)
				: Environment.io.createExternalFileHandle(this.path + FileHandle.SEPARATOR + path);
	}

	public void copyTo(FileHandle path) throws FileIOException
	{
		boolean thisIsDir = this.isDirectory();
		boolean pathIsDir = path.isDirectory();
		if (thisIsDir && !pathIsDir)
		{
			throw new Exceptions.FileIOException("Can not copy a directory into a file");
		}
		if (!thisIsDir && pathIsDir)
		{
			throw new Exceptions.FileIOException("Can not copy a file into a directory");
		}
		if (!exists())
		{
			throw new Exceptions.FileIOException("Can not copy a file that does not exist");
		}
		byte[] buffer = new byte[1024];
		int length;
		try (InputStream is = getInputStream(); OutputStream os = path.getOutputStream())
		{
			while ((length = is.read(buffer)) > 0)
			{
				os.write(buffer, 0, length);
			}
		}
		catch (IOException ioe)
		{
			throw new FileIOException(ioe.getMessage());
		}
	}

	public abstract InputStream getInputStream() throws FileIOException;

	public abstract OutputStream getOutputStream() throws FileIOException;

	public abstract boolean exists();

	public abstract boolean isDirectory();

	public abstract boolean isFile();

	public abstract void moveTo(FileHandle path) throws FileIOException;

	public abstract void mkdirs() throws FileIOException;

	public abstract void createFile() throws FileIOException;

	public abstract long sizeInBytes();

	public abstract List<FileHandle> listFiles() throws FileIOException;

	public abstract boolean delete() throws FileIOException;

	public abstract void deleteOnExit();

	@Override
	public int hashCode()
	{
		int result = getPath().hashCode();
		result = 31 * result + getType().hashCode();
		return result;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		FileHandle FileHandle = (FileHandle) o;

		return getPath().equals(FileHandle.getPath()) && getType() == FileHandle.getType();
	}

	@Override
	public String toString()
	{
		return "FileHandle{" +
				"path='" + getPath() + '\'' +
				", name='" + getName() + "'" +
				", extension='" + getExtension() + "'" +
				", type=" + type +
				", isDirectory=" + isDirectory() +
				", exists=" + exists() +
				", size=" + sizeInBytes() +
				'}';
	}

	public Type getType()
	{
		return type;
	}

	public enum Type
	{
		INTERNAL,
		EXTERNAL;
	}
}
