package bmg.emef.io;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import bmg.emef.buffer.DirectBuffer;
import bmg.emef.environment.Exceptions;

public class FileReader
{
	public DirectBuffer readBinaryFile(FileHandle file)
	{
		try
		{
			InputStream is = file.getInputStream();
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			byte[] buffer = new byte[4096];
			int n = -1;
			while ((n = is.read(buffer)) > -1)
			{
				os.write(buffer, 0, n);
			}
			is.close();
			byte[] bytes = os.toByteArray();
			DirectBuffer directBuffer = new DirectBuffer(bytes.length);
			for (int i = 0; i < bytes.length; i++)
			{
				directBuffer.writeByte(i, bytes[i]);
			}
			os.close();
			return directBuffer;
		}
		catch (Throwable thrown)
		{
			Exceptions.FileIOException.reThrow(thrown);
		}
		return null;
	}

	public String readTextFile(FileHandle file)
	{
		try (InputStream is = file.getInputStream(); BufferedReader br = new BufferedReader(new InputStreamReader(is)))
		{
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = br.readLine()) != null)
			{
				sb.append(line).append("\n");
			}
			return sb.toString();
		}
		catch (Throwable thrown)
		{
			Exceptions.FileIOException.reThrow(thrown);
		}
		return null;
	}
}
