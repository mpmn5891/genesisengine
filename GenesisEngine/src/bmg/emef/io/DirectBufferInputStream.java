package bmg.emef.io;

import java.io.InputStream;

import bmg.emef.buffer.DirectBuffer;
import bmg.emef.environment.Exceptions.FileIOException;

public class DirectBufferInputStream extends InputStream
{
	private DirectBuffer directBuffer;
	private int index;

	public DirectBufferInputStream(DirectBuffer directBuffer)
	{
		this.directBuffer = directBuffer;
	}

	@Override
	public int read() throws FileIOException
	{
		if (index >= directBuffer.sizeInBytes())
		{
			return -1;
		}
		else
		{
			return directBuffer.readByte(index++) & 0xff;
		}
	}
}
