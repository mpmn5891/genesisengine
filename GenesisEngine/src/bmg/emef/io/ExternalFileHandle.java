package bmg.emef.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import bmg.emef.environment.Exceptions.FileIOException;

public class ExternalFileHandle extends FileHandle
{
	public ExternalFileHandle(String path)
	{
		super(path, Type.EXTERNAL);
	}

	@Override
	public InputStream getInputStream() throws FileIOException
	{
		try
		{
			return Files.newInputStream(Paths.get(getPath()));
		}
		catch (IOException ioe)
		{
			throw new FileIOException(ioe.getMessage());
		}
	}

	@Override
	public OutputStream getOutputStream() throws FileIOException
	{
		try
		{
			return Files.newOutputStream(Paths.get(getPath()));
		}
		catch (IOException ioe)
		{
			throw new FileIOException(ioe.getMessage());
		}
	}

	@Override
	public boolean exists()
	{
		return Files.exists(Paths.get(getPath()));
	}

	@Override
	public boolean isDirectory()
	{
		return Files.isDirectory(Paths.get(getPath()));
	}

	@Override
	public boolean isFile()
	{
		return !isDirectory();
	}

	@Override
	public void moveTo(FileHandle path) throws FileIOException
	{
		try
		{
			Files.move(Paths.get(this.path), Paths.get(path.getPath()));
		}
		catch (IOException ioe)
		{
			throw new FileIOException(ioe.getMessage());
		}
	}

	@Override
	public void mkdirs() throws FileIOException
	{
		try
		{
			if (isFile() && !exists())
			{
				getParent().mkdirs();
			}
			else
			{
				Files.createDirectories(Paths.get(path));
			}
		}
		catch (IOException ioe)
		{
			throw new FileIOException(ioe.getMessage());
		}
	}

	@Override
	public void createFile() throws FileIOException
	{
		if (isDirectory())
		{
			throw new FileIOException("Can not convert a directory into a file");
		}
		else
		{
			try
			{
				Files.createFile(Paths.get(path));
			}
			catch (IOException ioe)
			{
				throw new FileIOException(ioe.getMessage());
			}
		}
	}

	@Override
	public long sizeInBytes()
	{
		try
		{
			if (exists())
			{
				return Files.size(Paths.get(path));
			}
		}
		catch (IOException ioe)
		{
			throw new FileIOException(ioe.getMessage());
		}
		return -1L;
	}

	@Override
	public List<FileHandle> listFiles() throws FileIOException
	{
		if (!isDirectory())
		{
			throw new FileIOException("Can not list Files in a path that is not a directory");
		}
		if (!exists())
		{
			throw new FileIOException("Can not list Files in a directory that does not exist");
		}
		List<FileHandle> list = new ArrayList<>();
		File file = new File(path);
		File[] children = file.listFiles();
		if (children != null)
		{
			for (File child : children)
			{
				list.add(new ExternalFileHandle(path + FileHandle.SEPARATOR + child.getPath().replace(file.getPath(), "")));
			}
		}
		return Collections.unmodifiableList(list);
	}

	@Override
	public boolean delete() throws FileIOException
	{
		if (!exists())
		{
			throw new FileIOException("Can not delete a non existing file");
		}
		if (isDirectory())
		{
			for (FileHandle child : listFiles())
			{
				child.delete();
			}
		}
		try
		{
			return Files.deleteIfExists(Paths.get(path));
		}
		catch (IOException ioe)
		{
			throw new FileIOException(ioe.getMessage());
		}
	}

	@Override
	public void deleteOnExit()
	{
		new File(getPath()).deleteOnExit();
	}
}
