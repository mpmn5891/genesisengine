package bmg.emef.io;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import org.lwjgl.stb.STBImage;

import bmg.emef.buffer.DirectBuffer;
import bmg.emef.environment.Environment;
import bmg.emef.environment.Exceptions;
import bmg.emef.graphics.Color;
import bmg.emef.graphics.Image;
import bmg.emef.util.BufferUtils;

public class ImageReader
{
	public Image readImage(FileHandle file)
	{
		return readImage(Environment.io.getFileReader().readBinaryFile(file));
	}

	public Image readImage(DirectBuffer data)
	{
		Image image = null;
		try
		{
			IntBuffer width = BufferUtils.createIntBuffer(1);
			IntBuffer height = BufferUtils.createIntBuffer(1);
			IntBuffer components = BufferUtils.createIntBuffer(1);
			ByteBuffer imageBuffer = STBImage.stbi_load_from_memory(data.nativeBuffer(), width, height, components, 4);
			if (imageBuffer == null)
			{
				throw new Exceptions.FileIOException("Faled to load image: " + STBImage.stbi_failure_reason());
			}
			else
			{
				image = new Image(width.get(0), height.get(0));
				for (int y = 0; y < image.getHeight(); y++)
				{
					for (int x = 0; x < image.getWidth(); x++)
					{
						int start = 4 * (y * image.getWidth() + x);
						float r = (imageBuffer.get(start) & 0xff) / 255f;
						float g = (imageBuffer.get(start + 1) & 0xff) / 255f;
						float b = (imageBuffer.get(start + 2) & 0xff) / 255f;
						float a = (imageBuffer.get(start + 3) & 0xff) / 255f;
						image.setPixel(x, y, new Color(r, g, b, a));
					}
				}
			}
			STBImage.stbi_image_free(imageBuffer);
		}
		catch (Throwable thrown)
		{
			Exceptions.FileIOException.reThrow(thrown);
		}
		return image;
	}
}
