package bmg.emef.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

import bmg.emef.environment.Environment;
import bmg.emef.environment.Exceptions;
import bmg.emef.environment.Exceptions.FileIOException;

public class InternalFileHandle extends FileHandle
{
	public InternalFileHandle(String path)
	{
		super(path, Type.INTERNAL);
	}

	public ResourceType getResourceType()
	{
		URL url = InternalFileHandle.class.getClassLoader().getResource(getPath());
		if (url != null && url.toString().contains("jar"))
		{
			return ResourceType.JAR;
		}
		else
		{
			return ResourceType.FILE;
		}
	}

	@Override
	public InputStream getInputStream() throws FileIOException
	{
		return InternalFileHandle.class.getClassLoader().getResourceAsStream(getPath());
	}

	@Override
	public OutputStream getOutputStream() throws FileIOException
	{
		throw new Exceptions.FileIOException("Can not open an OutputStream for an in-jar resource, which by definition is read only");
	}

	@Override
	public boolean exists()
	{
		return InternalFileHandle.class.getClassLoader().getResource(path) != null;
	}

	@Override
	public boolean isDirectory()
	{
		if (!exists())
		{
			return false;
		}
		try
		{
			if (getResourceType() == ResourceType.FILE)
			{
				return Files.isDirectory(Paths.get(getIDEPath()));
			}

			List<JarEntry> entries = getJarEntries();
			return entries.size() > 1 || entries.get(0).isDirectory();
		}
		catch (Exception thrown)
		{
			Exceptions.FileIOException.reThrow(thrown);
		}
		return false;
	}

	@Override
	public boolean isFile()
	{
		return !isDirectory();
	}

	@Override
	public void moveTo(FileHandle path) throws FileIOException
	{
		throw new FileIOException("Can not move an in-jar resource");
	}

	@Override
	public void mkdirs() throws FileIOException
	{
		throw new FileIOException("Can not create directory inside a jar");
	}

	@Override
	public void createFile() throws FileIOException
	{
		throw new FileIOException("Can not create a file inside a jar");
	}

	@Override
	public long sizeInBytes()
	{
		if (!exists())
		{
			return -1;
		}

		try
		{
			if (getResourceType() == ResourceType.FILE)
			{
				return Files.size(Paths.get(getIDEPath()));
			}

			List<JarEntry> entries = getJarEntries();
			long size = 0;
			for (JarEntry entry : entries)
			{
				size += entry.getSize();
			}
			return size;
		}
		catch (Exception thrown)
		{
			Exceptions.FileIOException.reThrow(thrown);
		}
		return -1;
	}

	@Override
	public List<FileHandle> listFiles() throws FileIOException
	{
		if (!isDirectory())
		{
			throw new Exceptions.FileIOException("Can not list Files in a path that is not a directory");
		}
		if (!exists())
		{
			throw new Exceptions.FileIOException("Can not list Files in a non existing directory");
		}
		List<FileHandle> list = new ArrayList<>();
		if (getResourceType() == ResourceType.FILE)
		{
			File file = new File(getIDEPath());
			File[] children = file.listFiles();
			if (children != null)
			{
				for (File child : children)
				{
					list.add(new InternalFileHandle(path + FileHandle.SEPARATOR + child.getPath().replace(file.getPath(), "")));
				}
			}
		}
		else
		{
			List<JarEntry> entries = getJarEntries();
			for (JarEntry entry : entries)
			{
				list.add(FileHandle.getInternalFile(entry.getName()));
			}
		}
		return Collections.unmodifiableList(list);
	}

	@Override
	public boolean delete() throws FileIOException
	{
		throw new FileIOException("Can not delete an in-jar resource");
	}

	@Override
	public void deleteOnExit()
	{
		throw new Exceptions.FileIOException("Can not delete an in-jar resource on exit");
	}

	private String getIDEPath() throws FileIOException
	{
		URL url = InternalFileHandle.class.getClassLoader().getResource(getPath());
		if (url == null)
		{
			throw new FileIOException("Incountered Error getting path relative to IDE: Path not found");
		}
		String urlString = url.toString();
		try
		{
			if (Environment.getPlatform() == Environment.Platform.WINDOWS_32 || Environment.getPlatform() == Environment.Platform.WINDOWS_64)
			{
				return URLDecoder.decode(urlString.substring(urlString.indexOf('/') + 1), "UTF-8");
			}
			else
			{
				return "/" + URLDecoder.decode(urlString.substring(urlString.indexOf('/') + 1), "UTF-8");
			}
		}
		catch (IOException ioe)
		{
			throw new FileIOException(ioe.getMessage());
		}
	}

	private List<JarEntry> getJarEntries() throws FileIOException
	{
		URL url = InternalFileHandle.class.getClassLoader().getResource(getPath());
		if (url == null)
		{
			throw new FileIOException("Incountered Error getting resource entries in jar: Resource does not exist");
		}
		try
		{
			String jarUrl = url.toString();
			String jarPath = URLDecoder.decode(jarUrl.substring(jarUrl.indexOf('/'), jarUrl.indexOf('!')), "UTF-8");
			JarFile jarFile = new JarFile(new File(jarPath));
			List<JarEntry> entries = jarFile.stream().filter(e -> e.getName().startsWith(path)).collect(Collectors.toList());
			jarFile.close();
			if (entries != null)
			{
				return entries;
			}
			else
			{
				throw new FileIOException("Incountered Error getting resource entries in jar:\n"
						+ "Can not find the JAR entry for: " + getPath() + " in JAR file " + jarPath);
			}
		}
		catch (IOException ioe)
		{
			throw new FileIOException(ioe.getMessage());
		}
	}

	public enum ResourceType
	{
		JAR,
		FILE;
	}
}
