package bmg.emef.io;

public class IO
{
	private FileReader fileReader = new FileReader();
	private ImageReader imageReader = new ImageReader();

	public FileHandle createInternalFileHandle(String path)
	{
		return new InternalFileHandle(path);
	}

	public FileHandle createExternalFileHandle(String path)
	{
		return new ExternalFileHandle(path);
	}

	public FileReader getFileReader()
	{
		return fileReader;
	}

	public ImageReader getImageReader()
	{
		return imageReader;
	}
}
