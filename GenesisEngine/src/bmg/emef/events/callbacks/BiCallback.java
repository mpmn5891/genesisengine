package bmg.emef.events.callbacks;

public interface BiCallback<T1, T2>
{
	void invoke(T1 arg1, T2 arg2);
}
