package bmg.emef.events.callbacks;

@FunctionalInterface
public interface SimpleCallback
{
	void invoke();
}
