package bmg.emef.events.callbacks;

@FunctionalInterface
public interface UniCallback<T>
{
	void invoke(T arg1);
}
