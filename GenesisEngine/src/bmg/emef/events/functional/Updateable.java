package bmg.emef.events.functional;

public interface Updateable
{
	public void update(float delta);
}
