package bmg.emef.events.functional;

public interface Initializable
{
	public void initialize();
}
