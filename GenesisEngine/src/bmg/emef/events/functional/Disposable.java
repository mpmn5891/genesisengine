package bmg.emef.events.functional;

public interface Disposable
{
	public void dispose();
}
