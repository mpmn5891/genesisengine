package bmg.emef.events.functional;

public interface Focusable
{
	public void focusChanged(boolean hasFocus);
}
