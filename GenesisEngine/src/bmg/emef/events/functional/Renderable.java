package bmg.emef.events.functional;

public interface Renderable
{
	public void render(float delta);
}
