package bmg.emef.events.functional;

public interface Resizable
{
	public void resized();
}
