package bmg.emef.events.functional;

@FunctionalInterface
public interface OnError
{
	public void invoke(Throwable thrown);
}
