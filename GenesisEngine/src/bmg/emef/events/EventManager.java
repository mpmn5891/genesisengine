package bmg.emef.events;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import bmg.emef.events.functional.Disposable;
import bmg.emef.events.functional.Focusable;
import bmg.emef.events.functional.Initializable;
import bmg.emef.events.functional.OnError;
import bmg.emef.events.functional.Renderable;
import bmg.emef.events.functional.Resizable;
import bmg.emef.events.functional.Updateable;

public class EventManager
{
	@SuppressWarnings("rawtypes")
	private Queue<ListenerQueueObject> listenerQueue = new LinkedList<>();
	private List<Initializable> initializeListeners = new ArrayList<>();
	private List<Updateable> updateListeners = new ArrayList<>();
	private List<Renderable> renderListeners = new ArrayList<>();
	private List<Resizable> resizeListeners = new ArrayList<>();
	private List<Focusable> focusListeners = new ArrayList<>();
	private List<Disposable> disposeListeners = new ArrayList<>();
	private List<OnError> errorListeners = new ArrayList<>();
	private static final Object QueueOperationLock = new Object();
	private static final Object initializeOperationLock = new Object();
	private static final Object updateOperationLock = new Object();
	private static final Object renderOperationLock = new Object();
	private static final Object resizeOperationLock = new Object();
	private static final Object focusOperationLock = new Object();
	private static final Object disposeOperationLock = new Object();
	private static final Object errorOperationLock = new Object();
	private static EventManager instance = null;

	public static EventManager getInstance()
	{
		if (instance == null)
		{
			EventManager.instance = new EventManager();
		}
		return EventManager.instance;
	}

	private EventManager()
	{
		// nothing to see here, prevents instantiation
	}

	public void registerInitializeListener(Initializable listener)
	{
		ListenerQueueObject<Initializable> lqo = new ListenerQueueObject<>();
		lqo.add = true;
		lqo.listener = listener;
		synchronized (QueueOperationLock)
		{
			listenerQueue.add(lqo);
		}
	}

	public void registerUpdateListener(Updateable listener)
	{
		ListenerQueueObject<Updateable> lqo = new ListenerQueueObject<>();
		lqo.add = true;
		lqo.listener = listener;
		synchronized (QueueOperationLock)
		{
			listenerQueue.add(lqo);
		}
	}

	public void registerRenderListener(Renderable listener)
	{
		ListenerQueueObject<Renderable> lqo = new ListenerQueueObject<>();
		lqo.add = true;
		lqo.listener = listener;
		synchronized (QueueOperationLock)
		{
			listenerQueue.add(lqo);
		}
	}

	public void registerResizeListener(Resizable listener)
	{
		ListenerQueueObject<Resizable> lqo = new ListenerQueueObject<>();
		lqo.add = true;
		lqo.listener = listener;
		synchronized (QueueOperationLock)
		{
			listenerQueue.add(lqo);
		}
	}

	public void registerFocusListener(Focusable listener)
	{
		ListenerQueueObject<Focusable> lqo = new ListenerQueueObject<>();
		lqo.add = true;
		lqo.listener = listener;
		synchronized (QueueOperationLock)
		{
			listenerQueue.add(lqo);
		}
	}

	public void registerDisposeListener(Disposable listener)
	{
		ListenerQueueObject<Disposable> lqo = new ListenerQueueObject<>();
		lqo.add = true;
		lqo.listener = listener;
		synchronized (QueueOperationLock)
		{
			listenerQueue.add(lqo);
		}
	}

	public void registerErrorListener(OnError listener)
	{
		ListenerQueueObject<OnError> lqo = new ListenerQueueObject<>();
		lqo.add = true;
		lqo.listener = listener;
		synchronized (QueueOperationLock)
		{
			listenerQueue.add(lqo);
		}
	}

	public void unregisterInitializeListener(Initializable listener)
	{
		ListenerQueueObject<Initializable> lqo = new ListenerQueueObject<>();
		lqo.add = false;
		lqo.listener = listener;
		synchronized (QueueOperationLock)
		{
			listenerQueue.add(lqo);
		}
	}

	public void unregisterUpdateListener(Updateable listener)
	{
		ListenerQueueObject<Updateable> lqo = new ListenerQueueObject<>();
		lqo.add = false;
		lqo.listener = listener;
		synchronized (QueueOperationLock)
		{
			listenerQueue.add(lqo);
		}
	}

	public void unregisterRenderListener(Renderable listener)
	{
		ListenerQueueObject<Renderable> lqo = new ListenerQueueObject<>();
		lqo.add = false;
		lqo.listener = listener;
		synchronized (QueueOperationLock)
		{
			listenerQueue.add(lqo);
		}
	}

	public void unregisterResizeListener(Resizable listener)
	{
		ListenerQueueObject<Resizable> lqo = new ListenerQueueObject<>();
		lqo.add = false;
		lqo.listener = listener;
		synchronized (QueueOperationLock)
		{
			listenerQueue.add(lqo);
		}
	}

	public void unregisterFocusListener(Focusable listener)
	{
		ListenerQueueObject<Focusable> lqo = new ListenerQueueObject<>();
		lqo.add = false;
		lqo.listener = listener;
		synchronized (QueueOperationLock)
		{
			listenerQueue.add(lqo);
		}
	}

	public void unregisterDisposeListener(Disposable listener)
	{
		ListenerQueueObject<Disposable> lqo = new ListenerQueueObject<>();
		lqo.add = false;
		lqo.listener = listener;
		synchronized (QueueOperationLock)
		{
			listenerQueue.add(lqo);
		}
	}

	public void unregisterErrorListener(OnError listener)
	{
		ListenerQueueObject<OnError> lqo = new ListenerQueueObject<>();
		lqo.add = false;
		lqo.listener = listener;
		synchronized (QueueOperationLock)
		{
			listenerQueue.add(lqo);
		}
	}

	public void propagateInitializeEvent()
	{
		processListenerQueue();
		synchronized (initializeOperationLock)
		{
			for (Initializable listener : initializeListeners)
			{
				listener.initialize();
			}
		}
	}

	public void propagateUpdateEvent(float delta)
	{
		processListenerQueue();
		synchronized (updateOperationLock)
		{
			for (Updateable listener : updateListeners)
			{
				listener.update(delta);
			}
		}
	}

	public void propagateRenderEvent(float delta)
	{
		processListenerQueue();
		synchronized (renderOperationLock)
		{
			for (Renderable listener : renderListeners)
			{
				listener.render(delta);
			}
		}
	}

	public void propagateResizeEvent()
	{
		processListenerQueue();
		synchronized (resizeOperationLock)
		{
			for (Resizable listener : resizeListeners)
			{
				listener.resized();
			}
		}
	}

	public void propagateFocusEvent(boolean hasFocus)
	{
		processListenerQueue();
		synchronized (focusOperationLock)
		{
			for (Focusable listener : focusListeners)
			{
				listener.focusChanged(hasFocus);
			}
		}
	}

	public void propagateDisposeEvent()
	{
		processListenerQueue();
		synchronized (disposeOperationLock)
		{
			for (Disposable listener : disposeListeners)
			{
				listener.dispose();
			}
		}
	}

	public void propagateError(Throwable thrown)
	{
		processListenerQueue();
		synchronized (errorOperationLock)
		{
			for (OnError listener : errorListeners)
			{
				listener.invoke(thrown);
			}
		}
	}

	private void processListenerQueue()
	{
		synchronized (QueueOperationLock)
		{
			@SuppressWarnings("rawtypes")
			ListenerQueueObject l = null;
			while ((l = listenerQueue.poll()) != null)
			{
				if (l.listener instanceof Initializable)
				{
					synchronized (initializeOperationLock)
					{
						if (l.add)
						{
							initializeListeners.add((Initializable) l.listener);
						}
						else
						{
							initializeListeners.remove((Initializable) l.listener);
						}
					}
				}
				if (l.listener instanceof Updateable)
				{
					synchronized (updateOperationLock)
					{
						if (l.add)
						{
							updateListeners.add((Updateable) l.listener);
						}
						else
						{
							updateListeners.remove((Updateable) l.listener);
						}
					}
				}
				if (l.listener instanceof Renderable)
				{
					synchronized (renderOperationLock)
					{
						if (l.add)
						{
							renderListeners.add((Renderable) l.listener);
						}
						else
						{
							renderListeners.remove((Renderable) l.listener);
						}
					}
				}
				if (l.listener instanceof Resizable)
				{
					synchronized (resizeOperationLock)
					{
						if (l.add)
						{
							resizeListeners.add((Resizable) l.listener);
						}
						else
						{
							resizeListeners.remove((Resizable) l.listener);
						}
					}
				}
				if (l.listener instanceof Focusable)
				{
					synchronized (focusOperationLock)
					{
						if (l.add)
						{
							focusListeners.add((Focusable) l.listener);
						}
						else
						{
							focusListeners.remove((Focusable) l.listener);
						}
					}
				}
				if (l.listener instanceof Disposable)
				{
					synchronized (disposeOperationLock)
					{
						if (l.add)
						{
							disposeListeners.add((Disposable) l.listener);
						}
						else
						{
							disposeListeners.remove((Disposable) l.listener);
						}
					}
				}
				if (l.listener instanceof OnError)
				{
					synchronized (errorOperationLock)
					{
						if (l.add)
						{
							errorListeners.add((OnError) l.listener);
						}
						else
						{
							errorListeners.remove((OnError) l.listener);
						}
					}
				}
			}
		}
	}

	private class ListenerQueueObject<T>
	{
		public boolean add;
		public T listener;
	}
}
