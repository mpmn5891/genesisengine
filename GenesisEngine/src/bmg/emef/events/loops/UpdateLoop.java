package bmg.emef.events.loops;

public interface UpdateLoop
{
	public void preformLoop();

	public int getFPS();

	public int getUPS();

	public float getDelta();

	public void onFocusChange(boolean hasFocus);
}
