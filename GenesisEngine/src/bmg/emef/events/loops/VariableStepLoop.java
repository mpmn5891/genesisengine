package bmg.emef.events.loops;

import bmg.emef.environment.Environment;
import bmg.emef.graphics.Graphics;
import bmg.emef.graphics.gl.GLContext;
import bmg.emef.util.TimeUtils;

public class VariableStepLoop implements UpdateLoop
{
	private double previous;
	private int frames;
	private int fps;
	private double lastFPSTime;
	private float delta;

	private double aut = 0;
	private double art = 0;

	public VariableStepLoop()
	{
		previous = TimeUtils.currentTime();
		Environment.eventManager.registerFocusListener(this::onFocusChange);
	}

	@Override
	public void preformLoop()
	{
		double current = TimeUtils.currentTime();
		delta = (float) (current - previous);
		previous = current;

		double updateStart = TimeUtils.currentTime();
		Environment.eventManager.propagateUpdateEvent(delta);
		Environment.Analytics.lastUpdateTime = TimeUtils.currentTime() - updateStart;
		if (aut == 0)
		{
			aut = Environment.Analytics.lastUpdateTime;
		}
		else
		{
			aut = (aut + Environment.Analytics.lastUpdateTime) / 2;
		}

		double renderStart = TimeUtils.currentTime();
		GLContext.clear(Graphics.Constants.GL_COLOR_BUFFER_BIT | Graphics.Constants.GL_DEPTH_BUFFER_BIT);
		Environment.eventManager.propagateRenderEvent(delta);
		Environment.Analytics.lastRenderTime = TimeUtils.currentTime() - renderStart;
		if (art == 0)
		{
			art = Environment.Analytics.lastRenderTime;
		}
		else
		{
			art = (art + Environment.Analytics.lastRenderTime) / 2;
		}

		frames++;
		if (current - lastFPSTime >= TimeUtils.convert(1000, TimeUtils.Unit.MILLIS))
		{
			fps = frames;
			frames = 0;
			lastFPSTime = current;
			Environment.Analytics.averageUpdateTime = aut;
			Environment.Analytics.averageRenderTime = art;
		}
	}

	@Override
	public int getFPS()
	{
		return fps;
	}

	@Override
	public int getUPS()
	{
		return fps;
	}

	@Override
	public float getDelta()
	{
		return delta;
	}

	@Override
	public void onFocusChange(boolean hasFocus)
	{
		if (hasFocus)
		{
			previous = TimeUtils.currentTime();
		}
	}
}
